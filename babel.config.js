module.exports = {
  presets: [
    [
      '@babel/env',
      {
        targets: { node: 'current' },
      },
    ],
    '@babel/preset-react',
  ],

  plugins: [
    '@babel/plugin-proposal-object-rest-spread',
    '@babel/transform-runtime',
    'import-graphql',
    [
      'wrap-in-js',
      {
        extensions: ['css$', 'scss$'],
      },
    ],
    ['@babel/plugin-proposal-decorators', { legacy: true }],
    '@babel/plugin-proposal-class-properties',
  ],
}
