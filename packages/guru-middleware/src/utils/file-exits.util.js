import fs from 'fs'
import util from 'util'

const writeFile = util.promisify(fs.writeFile)
const exists = util.promisify(fs.exists)
const mkdir = util.promisify(fs.mkdir)

export async function fileExists(filePath, write) {
  try {
    const doesExist = exists(filePath)

    if (!doesExist && write) {
      const index = filePath.lastIndexOf('/')
      const dir = filePath.substr(0, index)

      if (dir !== '.') {
        return await mkdir(dir)
      }

      await writeFile(filePath)
    }
  } catch (error) {
    console.error(error)
    return error
  }
}
