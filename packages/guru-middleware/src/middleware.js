import { logger } from './logger.middleware'
import { security } from './security.middleware'
import { session } from './session.middleware'

export function middleware({ app }) {
  logger(app)
  session(app)
  security(app)
}
