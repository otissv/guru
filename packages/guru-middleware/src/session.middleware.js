import cookieParser from 'cookie-parser'
import expressSession from 'express-session'

export function session(app) {
  app.use(
    expressSession({
      saveUninitialized: true, // saved new sessions
      resave: false, // do not automatically write to the session store
      // store: sessionStore,
      secret: process.env.SECRET,
      cookie: { httpOnly: true, maxAge: 2419200000 }, // configure when sessions expires
    })
  )

  if (app.get('env') === 'production') {
    app.set('trust proxy', 1) // trust first proxy
    expressSession.cookie.secure = true // serve secure cookies
  }

  app.use(cookieParser(process.env.SECRET))
}
