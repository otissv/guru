import { fileExists } from './utils'
import fs from 'fs'
import morgan from 'morgan'
import path from 'path'
import winston from 'winston'

const dest = `${process.cwd()}/logs`

export function logger(app) {
  if (process.env.ACCESS_LOG) {
    fileExists(process.env.ACCESS_LOG, true).then(() => {
      let accessLogStream = fs.createWriteStream(process.env.ACCESS_LOG, {
        flags: 'a',
      })
      app.use(morgan('combined', { stream: accessLogStream }))

      if (app.get('env') === 'development') {
        app.use(morgan('dev'))
      }
    })
  } else {
    app.use(morgan('tiny'))
  }

  if (process.env.ERROR_LOG) {
    fileExists(process.env.ERROR_LOG, true).then(() => {
      const tsFormat = () => new Date()

      const logger = winston.createLogger({
        transports: [
          new winston.transports.Console({
            timestamp: tsFormat,
            colorize: true,
          }),
          new winston.transports.File({
            filename: path.join(__dirname, process.env.ERROR_LOG),
            timestamp: tsFormat,
          }),
        ],
      })
    })
  }

  //TODO: Add event emitter
}
