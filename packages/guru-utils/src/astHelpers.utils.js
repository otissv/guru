import { Kind, parse } from 'graphql'

export function buildTypeDefs(typeDefs, nonInputTypes, callback) {
  const types = parse(Array.isArray(typeDefs) ? typeDefs.join(' ') : typeDefs)

  const typeNames = getTypeNames(types)

  return types.definitions.reduce(
    (previous, current, index) => {
      // if (current.kind === 'UnionTypeDefinition') {
      //   console.log('buildTypeDefs', JSON.stringify(current.types, null, 2))
      // }

      if (notObjectTypeDef(current)) return previous

      const inputFields = makeInputFields(current, nonInputTypes, typeNames)
      const cb = callback({
        previous,
        current,
        input: inputFields,
        index,
        previous,
        types,
      })

      return {
        inputs: `${previous.inputs}
            ${cb.inputs}
          `,
        types: `${previous.types}
          ${cb.types}
          `,
      }
    },
    {
      inputs: '',
      types: '',
    }
  )
}

export function getObjectDirectiveArguments(field) {
  return field.directives.reduce((previous, current) => {
    return {
      ...previous,
      [current.name.value]: current.arguments.reduce((prev, curr) => {
        return {
          ...prev,
          [curr.name.value]: curr.value.value,
        }
      }, {}),
    }
  }, {})
}

export function getTypeNames(types) {
  return types.definitions.reduce((previous, current) => {
    if (notObjectTypeDef(current)) return previous

    return [...previous, current.name.value]
  }, [])
}

export function hasDefinitionWithName(nodes, name) {
  return nodes.findIndex(node => node.name.value === name) !== -1
}

export function isObjectTypeDefinition(def) {
  return (
    def.kind === Kind.OBJECT_TYPE_DEFINITION ||
    def.kind === Kind.INPUT_OBJECT_TYPE_DEFINITION
  )
}

export function isObjectSchemaDefinition(def) {
  return def.kind === Kind.SCHEMA_DEFINITION
}

export function makeInputFields(current, nonInputTypes = [], typeNames) {
  return current.fields.reduce(
    (previous, field) => {
      if (
        nonInputTypes.includes(
          (field.type.type && field.type.type.name.value) ||
            field.type.name.value
        )
      ) {
        return previous
      }

      let fieldName
      if (field.type.kind === 'ListType') {
        fieldName = typeNames.includes(field.type.type.name.value)
          ? `Input${field.type.type.name.value}`
          : field.type.type.name.value
      } else {
        fieldName = typeNames.includes(field.type.name.value)
          ? `Input${field.type.name.value}`
          : field.type.name.value
      }

      return {
        fields: `${previous.fields}
        ${field.name.value}: ${fieldName}
      `,
        projection: `${previous.projection}
        ${field.name.value}: Int
      `,
      }
    },
    {
      fields: '',
      projection: '',
    }
  )
}

export function notObjectTypeDef(type) {
  return (
    type.kind !== Kind.OBJECT_TYPE_DEFINITION ||
    !type.kind ||
    type.name.value === 'Query' ||
    type.name.value === 'Mutation' ||
    type.name.value === 'Subscription' ||
    type.name.value === 'schema'
  )
}
