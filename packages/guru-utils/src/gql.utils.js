export const gql = (arr, ...args) =>
  [arr[0], ...args, ...arr.slice(1, arr.length)].join('')
