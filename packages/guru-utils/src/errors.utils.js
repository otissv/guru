export function log(message) {
  console.error(`Error: ${message}`)
}

export function errorLogger(error) {
  if (process.env.DEBUG === 'true') {
    console.error(error)
  }
}

export function accessDeniedError({ resolverName }) {
  const message = `Unauthorized access to ${resolverName}. You do not have permission to access this resource.`

  log(message)

  return [message, 'ACCESS_DENIED']
}

export function collectionNameError({ resolverName }) {
  const message = `${resolverName} must have a database table or a collection name.`

  log(message)

  return [message, 'COLLECTION_NAME_ERROR']
}

export function databaseConfigurationError({ namespace }) {
  const message = `${namespace} is not not setup correctly. Check it is configured correctly.`

  log(message)

  return [message, 'DATABASE_CONFIGURATION_ERROR']
}

export function invalidAuthenticationError() {
  const message = `Invalid authentication details.`

  log(message)

  return [message, 'AUTHENTICATION_FAILURE']
}

export function loginError() {
  const message = 'Incorrect login details.'
  return [message, 'LOGIN_FAILURE']
}

export function logoutError() {
  const message = 'Logout failed.'
  return [message, 'LOGOUT_FAILURE']
}

export function noDataError({ resolverName }) {
  const message = `${resolverName}, no data supplied.`

  log(message)

  return [message, 'RESOLVER_MUTATION_DATA_ERROR']
}

export function noInputError({ resolverName }) {
  const message = `${resolverName}, invalid input argument.`

  log(message)

  return [message, 'RESOLVER_INPUT_ERROR']
}

export function resolverMutationError({ error, resolverName }) {
  const message = `${resolverName} mutation resolver error.`

  log(message, error)

  return [message, 'RESOLVER_MUTATION_ERROR']
}

export function resolverQueryError({ error, resolverName }) {
  const message = `${resolverName} query resolver error.`

  log(message, error)

  return [message, 'RESOLVER_QUERY_ERROR']
}
