const initialValue = {
  resolvers: {},
  typeDefs: {},
  middlewareModules: [],
  resolverModules: [],
  routeModules: [],
  typeDefModules: [],
  typeDefJsModules: [],
  engines: [],
}

function resolvers(previous, current) {
  return current.resolvers
    ? {
        ...previous,
        [current.namespace]: current.resolvers,
      }
    : previous
}

function typeDefs(previous, current) {
  return current.typeDefs
    ? {
        ...previous,
        [current.namespace]: current.typeDefs,
      }
    : previous
}

function middlewareModules(previous, current, plugins) {
  return current.middlewareModules
    ? [...previous, current.middlewareModules(plugins)]
    : previous
}

function resolverModules(previous, current, plugins) {
  return current.resolverModules
    ? [...previous, current.resolverModules(plugins)]
    : previous
}

function routeModules(previous, current, plugins) {
  return current.routeModules
    ? [...previous, current.routeModules(plugins)]
    : previous
}

function typeDefModules(previous, current, plugins) {
  return current.typeDefModules
    ? [...previous, current.typeDefModules(plugins)]
    : previous
}

function typeDefJsModules(previous, current, plugins) {
  return current.typeDefModules
    ? [...previous, current.typeDefJsModules(plugins)]
    : previous
}

export function getMappedPlugins(plugins) {
  return plugins
    ? plugins.reduce((previous, current) => {
        return {
          ...previous,
          middlewareModules: middlewareModules(
            previous.middlewareModules,
            current,
            [...plugins]
          ),
          resolverModules: resolverModules(previous.resolverModules, current, [
            ...plugins,
          ]),
          routeModules: routeModules(previous.routeModules, current, [
            ...plugins,
          ]),
          typeDefModules: typeDefModules(previous.typeDefModules, current, [
            ...plugins,
          ]),
          typeDefJsModules: typeDefJsModules(
            previous.typeDefJsModules,
            current,
            [...plugins]
          ),

          resolvers: resolvers(previous.resolvers, current),
          typeDefs: typeDefs(previous.typeDefs, current),
          engines: [
            ...previous.engines,
            ...(current.client ? [current.namespace] : []),
          ],
          [current.namespace]: current,
        }
      }, initialValue)
    : initialValue
}
