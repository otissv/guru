import chalk from 'chalk'

export function resolverLogger(resolverName) {
  if (process.env.DEBUG) {
    console.log(`${chalk.green('Resolver operation:')} ${resolverName}`)
  }
}
