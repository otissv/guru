import isEmpty from 'lodash/isEmpty'

export function getResolverArgs(args, obj) {
  switch (true) {
    case !isEmpty(args):
      return args
    case !isEmpty(obj):
      return obj
    default:
      return {}
  }
}
