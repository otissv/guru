import isEmpty from 'lodash/fp/isEmpty'

export function cleanArray(array) {
  if (!Array.isArray) return array
  if (isEmpty(array)) return array

  return array.filter(item => !isEmpty(item))
}
