export function meta({ userId, create }) {
  let data = {}
  const now = new Date().toISOString()

  if (create) {
    data.createdAt = now
    data.createdBy = userId
  }

  data.updatedAt = now
  data.updatedBy = userId || null

  return data
}
