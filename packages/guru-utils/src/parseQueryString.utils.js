import queryString from 'querystring'

export async function parseQueryString(str) {
  if (str.indexOf('?') >= 0) return queryString.parse(str.replace(/^.*\?/, ''))
}
