import get from 'lodash/fp/get'
import isEmpty from 'lodash/fp/isEmpty'

export function maybe(emptyType = null) {
  return value => (value == null ? emptyType : value)
}

export function maybeNotEmpty(emptyType = null) {
  return value => (isEmpty(value) ? emptyType : value)
}

export function maybeProp(obj) {
  return path => get(path)(obj) || null
}

export function maybeArray(array, emptyType = null) {
  const isArray = Array.isArray(array)
  return function(cb) {
    return cb(isArray, isArray ? array : emptyType)
  }
}
