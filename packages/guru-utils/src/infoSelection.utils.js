export function getInfoSelection(info) {
  const stringify = obj => JSON.stringify(obj, null, 2)

  const reduceArguments = (acc, field) => {
    return {
      ...acc,
      [field.name.value]:
        field.value.kind === 'ListValue'
          ? field.value.values.map(v => v.value)
          : field.value.fields
          ? field.value.fields.reduce(reduceArguments, {})
          : field.value.kind === 'Variable'
          ? info.variableValues[field.name.value]
          : field.value.value,
    }
  }
  const reduceSelectionSetFields = (acc, field) => {
    return {
      ...acc,
      [field.name.value]: {
        arguments: field.arguments.reduce(reduceArguments, {}),
      },
    }
  }

  return {
    arguments: info.fieldNodes[0].arguments.reduce(reduceArguments, {}),
    fields: info.fieldNodes[0].selectionSet.selections.reduce(
      reduceSelectionSetFields,
      {}
    ),
  }
}

export function getProjectionFromInfoSelection(selection) {
  const projection =
    (selection.fields &&
      selection.fields.data &&
      selection.fields.data.arguments.projection &&
      selection.fields.data.arguments.projection) ||
    {}

  const makeIdKey = (acc, key) =>
    key === '_id'
      ? {
          ...acc,
          _id: parseInt(projection._id, 10),
        }
      : acc

  return Object.keys(projection).reduce(makeIdKey, {})
}

export function getValueFromInfoSelection(selection, prop) {
  return (
    (selection.fields &&
      selection.fields.data &&
      selection.fields.data.arguments[prop]) ||
    {}
  )
}
