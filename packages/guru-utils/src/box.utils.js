export const Box = x => ({
  map: f => Box(f(x)),
  fold: f => f(x),
  inspect: () => `Box(${x}`,
  debugger: v => {
    debugger
  },
  log: v => {
    console.log(v)
    return v
  },
})
