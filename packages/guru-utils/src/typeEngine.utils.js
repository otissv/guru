export function typeEngine({ engine, namespace, type, name }) {
  if (engine) {
    if (engine[type] === false) {
      return null
    }

    if (engine.namespace === namespace || engine.namespace === name) {
      return {
        ...engine,
        value: {
          collection: engine.collection || engine.table,
          table: engine.table || engine.collection,
        },
      }
    }
  }
  return null
}
