import camelCase from 'lodash/fp/camelCase'
import { queryObjectId } from './utils'

function makeObjToMongodbQueryObj(value) {
  if (Array.isArray(value)) {
    return value.reduce(
      (prev, curr) => [
        ...prev,
        typeof value === 'object' ? makeObjToMongodbQueryObj(curr) : value,
      ],
      []
    )
  }

  if (typeof value === 'object') {
    return Object.keys(value).reduce((previous, current) => {
      const key =
        current === current.toUpperCase() ? `$${camelCase(current)}` : current
      return {
        ...previous,
        [key]:
          typeof value[current] === 'object'
            ? makeObjToMongodbQueryObj(value[current])
            : value[current],
      }
    }, {})
  }

  return value
}

export function execute({
  args,
  col,
  cursor,
  db,
  method,
  projection,
  data,
  options,
}) {
  const objToMongodbQueryObj = makeObjToMongodbQueryObj(args)
  const queryArgs = queryObjectId(objToMongodbQueryObj)
  const parameters = [queryArgs, projection, data].filter(item => item)
  const query = db.collection(col)[method](...parameters, options)

  let _cursor
  if (!cursor) {
    return query
  } else {
    _cursor = makeObjToMongodbQueryObj(cursor)
  }

  return Object.keys(_cursor).reduce((previous, key) => {
    const value = _cursor[key]

    // close: () => previous[key](),
    // count: () => previous[key](),
    // explain: () => previous[key](),
    // hasNext: () => previous[key](),
    // isClosed
    // next
    // nextObject
    // read: value => previous[key](value),

    // alias methods
    const type = {
      last: value => {
        if (isNaN(value)) {
          const k = Object.keys(value)
          return previous.limit(3).sort({ [k]: -1 })
        } else {
          return previous.limit(value).sort({ $natural: -1 })
        }
      },
      first: value => previous.limit(value),
    }

    return (
      (previous[key] && previous[key](value)) ||
      (type[key] && type[key](value)) ||
      previous
    )
  }, query)
}
