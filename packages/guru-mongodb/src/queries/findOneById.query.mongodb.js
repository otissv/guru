import {
  collectionNameError,
  databaseConfigurationError,
  errorLogger,
  noInputError,
  resolverQueryError,
} from 'guru-utils'

import { ApolloError } from 'apollo-server'
import { execute } from '../execute.mongodb'

export function findOneByIdQuery(params) {
  const namespace = params.namespace
  const moduleName = params.moduleName
  const collection = params.collection
  const paramsContext = params.context
  const resolverName = `${moduleName}FindOneByIdQuery`

  return async function findOneByIdQueryResolver(
    obj,
    args,
    context = {},
    info
  ) {
    try {
      const dbInstance = paramsContext[namespace]
        ? await paramsContext[namespace]
        : context[namespace]

      // Error Checking
      if (!dbInstance) {
        throw new ApolloError(
          ...databaseConfigurationError({ namespace, resolverName })
        )
      }
      if (!collection) {
        throw new ApolloError(...collectionNameError({ resolverName }))
      }
      if (
        Object.keys(args).length > 0 &&
        !args.options &&
        !args.query &&
        !args.cursor
      ) {
        throw new ApolloError(...noInputError({ resolverName, args: args }))
      }

      if (args.query == null || args.query.id == null) return null

      // Database execute
      const mongodb = dbInstance.client()
      return mongodb
        .then(db =>
          execute({
            db,
            col: collection,
            cursor: args.cursor,
            method: 'findOne',
            projection: args.projection,
            args: args.query,
          })
        )
        .then(doc => {
          if (doc == null) return doc
          return doc
        })
    } catch (error) {
      errorLogger(error)

      return error.extensions
        ? error
        : new ApolloError(
            ...resolverQueryError({
              error,
              namespace,
              resolverName,
            })
          )
    }
  }
}
