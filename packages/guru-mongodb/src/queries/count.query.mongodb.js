import {
  collectionNameError,
  databaseConfigurationError,
  errorLogger,
  noInputError,
  resolverQueryError,
} from 'guru-utils'
import { execute, logger } from '../execute.mongodb'

import { ApolloError } from 'apollo-server'

export function countQuery(params) {
  const namespace = params.namespace
  const moduleName = params.moduleName
  const collection = params.collection
  const paramsContext = params.context
  const resolverName = `${moduleName}Count`

  return async function countQueryResolver(obj, args, context = {}, info) {
    try {
      const dbInstance = paramsContext[namespace]
        ? await paramsContext[namespace]
        : context[namespace]

      // Error Checking
      if (!dbInstance) {
        throw new ApolloError(
          ...databaseConfigurationError({ namespace, resolverName })
        )
      }
      if (!collection) {
        throw new ApolloError(...collectionNameError({ resolverName }))
      }
      if (
        Object.keys(args).length > 0 &&
        !args.options &&
        !args.query &&
        !args.cursor
      ) {
        throw new ApolloError(...noInputError({ resolverName }))
      }

      // Arguments
      const tmpArgs = { ...args.query }
      delete tmpArgs.options

      // Database execute
      const mongodb = dbInstance.client()
      const n = await mongodb.then(db =>
        execute({
          args: tmpArgs,
          col: collection,
          cursor: args.cursor,
          db,
          method: 'find',
          projection: args.projection,
        }).count()
      )

      return {
        COUNT: { n },
      }
    } catch (error) {
      errorLogger(error)

      return error.extensions
        ? error
        : new ApolloError(
            ...resolverQueryError({
              error,
              namespace,
              resolverName,
            })
          )
    }
  }
}
