import {
  collectionNameError,
  databaseConfigurationError,
  errorLogger,
  noInputError,
  resolverQueryError,
} from 'guru-utils'

import { ApolloError } from 'apollo-server'
import { execute } from '../execute.mongodb'

export function findOneQuery(params) {
  const namespace = params.namespace
  const moduleName = params.moduleName
  const collection = params.collection
  const paramsContext = params.context
  const resolverName = `${moduleName}FindOne`

  return async function findOneQueryResolver(obj, args, context = {}, info) {
    try {
      const dbInstance = paramsContext[namespace]
        ? await paramsContext[namespace]
        : context[namespace]

      // Error Checking
      if (!dbInstance) {
        throw new ApolloError(
          ...databaseConfigurationError({ namespace, resolverName })
        )
      }
      if (!collection) {
        throw new ApolloError(...collectionNameError({ resolverName }))
      }
      if (
        Object.keys(args).length > 0 &&
        !args.options &&
        !args.query &&
        !args.cursor
      ) {
        throw new ApolloError(
          findOneQueryResolver,
          ...noInputError({ resolverName, args: args })
        )
      }

      // Database execute
      const mongodb = dbInstance.client()
      return mongodb.then(async db => {
        try {
          const doc = await execute({
            args: args.query,
            col: collection,
            cursor: args.cursor,
            db,
            method: 'findOne',
            projection: args.projection,
          })

          if (!doc) return doc

          return doc
        } catch (error) {
          throw error
        }
      })
    } catch (error) {
      errorLogger(error)

      return error.extensions
        ? error
        : new ApolloError(
            ...resolverQueryError({
              error,
              namespace,
              resolverName,
            })
          )
    }
  }
}
