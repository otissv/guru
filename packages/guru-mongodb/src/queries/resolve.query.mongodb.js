import { ApolloError } from 'apollo-server'
import { findByIdQuery } from './findById.query.mongodb'
import { resolverQueryError } from 'guru-utils'

export function resolveQuery({ namespace, moduleName, collection }) {
  return function resolveQueryResolver(obj, args, context, info) {
    const result = findByIdQuery({ namespace, moduleName, collection })(
      obj,
      args,
      context,
      info
    )

    return Promise.resolve(result).catch(error => {
      return new ApolloError(
        resolverQueryError({ error, namespace, resolverName: moduleName })
      )
    })
  }
}
