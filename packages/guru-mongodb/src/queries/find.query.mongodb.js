import {
  collectionNameError,
  databaseConfigurationError,
  errorLogger,
  noInputError,
  resolverQueryError,
} from 'guru-utils'

import { ApolloError } from 'apollo-server'
import { execute } from '../execute.mongodb'

export function findQuery(params) {
  const namespace = params.namespace
  const moduleName = params.moduleName
  const collection = params.collection
  const paramsContext = params.context
  const resolverName = `${moduleName}Find`

  return async function findQueryResolver(obj, args, context = {}, info) {
    try {
      const dbInstance = paramsContext[namespace]
        ? await paramsContext[namespace]
        : context[namespace]

      // Error Checking
      if (!context || !context[namespace]) {
        throw new ApolloError(
          ...databaseConfigurationError({ namespace, resolverName })
        )
      }
      if (!collection) {
        throw new ApolloError(...collectionNameError({ resolverName }))
      }
      if (
        Object.keys(args).length > 0 &&
        !args.options &&
        !args.query &&
        !args.cursor
      ) {
        throw new ApolloError(...noInputError({ resolverName, args: args }))
      }

      // Arguments
      // Database execute
      const mongodb = context[namespace].client()
      return mongodb.then(db =>
        new Promise((resolve, reject) => {
          return execute({
            args: args.query,
            col: collection,
            cursor: args.cursor,
            db,
            method: 'find',
            projection: args.projection,
          }).toArray((error, docs) => {
            if (error) {
              throw error
            }

            resolve(docs)
          })
        }).catch(error => {
          throw error
        })
      )
    } catch (error) {
      errorLogger(error)

      return error.extensions
        ? error
        : new ApolloError(
            ...resolverQueryError({
              error,
              namespace,
              resolverName,
            })
          )
    }
  }
}
