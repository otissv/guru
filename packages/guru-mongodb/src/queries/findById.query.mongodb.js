import {
  collectionNameError,
  databaseConfigurationError,
  errorLogger,
  noInputError,
  resolverQueryError,
} from 'guru-utils'

import { ApolloError } from 'apollo-server'
import { execute } from '../execute.mongodb'

export function findByIdQuery(params) {
  const namespace = params.namespace
  const moduleName = params.moduleName
  const collection = params.collection
  const paramsContext = params.context
  const resolverName = `${moduleName}FindManyById`

  return async function findByIdQueryResolver(obj, args, context = {}, info) {
    try {
      const dbInstance = paramsContext[namespace]
        ? await paramsContext[namespace]
        : context[namespace]

      // Error Checking
      if (!dbInstance) {
        throw new ApolloError(
          ...databaseConfigurationError({ namespace, resolverName })
        )
      }
      if (!collection) {
        throw new ApolloError(...collectionNameError({ resolverName }))
      }
      if (
        Object.keys(args).length > 0 &&
        !args.options &&
        !args.query &&
        !args.cursor
      ) {
        throw new ApolloError(...noInputError({ resolverName, args: args }))
      }

      // Arguments
      if (args.query == null) return null

      let tmpArgs = Array.isArray(args.query)
        ? { id: { $in: args.query } }
        : args.query

      // Database execute
      const mongodb = dbInstance.client()
      return mongodb.then(
        db =>
          new Promise((resolve, reject) =>
            execute({
              args: tmpArgs,
              col: collection,
              cursor: args.cursor,
              db,
              method: 'find',
              projection: args.projection,
            }).toArray((error, docs) => {
              if (error) {
                throw error
              }

              resolve(docs)
            })
          )
      )
    } catch (error) {
      console.log(error)
      errorLogger(error)

      return error.extensions
        ? error
        : new ApolloError(
            ...resolverQueryError({
              error,
              namespace,
              resolverName,
            })
          )
    }
  }
}
