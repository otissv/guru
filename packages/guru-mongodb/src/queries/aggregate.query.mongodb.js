import {
  collectionNameError,
  databaseConfigurationError,
  errorLogger,
  noInputError,
  resolverQueryError,
} from 'guru-utils'

import { ApolloError } from 'apollo-server'
import { execute } from '../execute.mongodb'

export function aggregateQuery({ namespace, moduleName, collection }) {
  const resolverName = `${moduleName}Aggregate`

  return async function aggregateQueryResolver(obj, args, context = {}, info) {
    try {
      // Error Checking
      if (!context || !context[namespace]) {
        throw new ApolloError(
          ...databaseConfigurationError({ namespace, resolverName })
        )
      }
      if (!collection) {
        throw new ApolloError(...collectionNameError({ resolverName }))
      }
      if (
        Object.keys(args).length > 0 &&
        !args.options &&
        !args.query &&
        !args.cursor
      ) {
        throw new ApolloError(...noInputError({ resolverName, args: args }))
      }

      // Arguments
      const { options, ...tmpArgs } = args.query

      // Database execute
      // const mongodb = context[namespace].client()
      // const n = await mongodb.then(db =>
      //   execute(({
      //     args: tmpArgs,
      //     col: collection,
      //     cursor: args.cursor,
      //     db,
      //     method: 'find',
      //     projection: args.projection,
      //   }).count()
      // )

      return {
        COUNT: { n },
      }
    } catch (error) {
      errorLogger(error)

      return error.extensions
        ? error
        : new ApolloError(
            ...resolverQueryError({
              error,
              namespace,
              resolverName,
            })
          )
    }
  }
}
