import chalk from 'chalk'
import { mongoDbResolvers } from './resolvers.mongodb'
import mongodb from 'mongodb'

export class MongodbClient {
  constructor({ context = {}, namespace, options, typeDefs }) {
    this._db
    this.namespace = namespace
    this.options = options
    this.context = context
    this.typeDefs = typeDefs
  }

  connection = async options => {
    try {
      const mongodbUri = await options.uri
      return await mongodb.MongoClient.connect(mongodbUri, {
        native_parser: true,
        useNewUrlParser: true,
      })
    } catch (error) {
      console.error(error)
      return error
    }
  }

  client = async options => {
    const connection = await this.connection(options || this.options)

    this._db = Promise.resolve(connection.db())

    return this._db
  }

  insertData = async ({ collection, data, indexs, silent }) => {
    try {
      const fileCount = data.length || 0

      let db = await this._db
      let cols = await db.collections()
      let colsIncludesCollection = cols.map(c => c.s.name).includes(collection)
      colsIncludesCollection && (await db.collection(collection).drop())
      await db.createCollection(collection)
      indexs && (await db.collection(collection).createIndex(indexs))

      let result = await db.collection(collection).insertMany(data)

      if (!silent) {
        console.log(
          chalk.green(
            `Inserted ${fileCount} ${
              fileCount === 1 ? 'file' : 'files'
            } into ${collection} collection `
          )
        )
      }

      return result
    } catch (error) {
      console.error(error)
      return error
    }
  }

  resolvers = () => {
    return mongoDbResolvers({
      context: {
        ...this.context,
        context,
        [this.namespace]: this,
      },
      typeDefs: this.typeDefs,
      namespace: this.namespace,
    })
  }

  seed = async collections => {
    try {
      if (Array.isArray(collections)) {
        collections.forEach(async collection => {
          await this.insertData(collection)
        })
      } else {
        await this.insertData(collections)
      }
    } catch (error) {
      throw error
    }
  }
}
