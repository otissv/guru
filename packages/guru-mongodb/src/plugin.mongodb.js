import { MongodbClient } from './client.mongodb'
import { mongoDbResolvers } from './resolvers.mongodb'
import { mongoDbTypeDefs } from './typeDefs.mongodb'
import pkg from '../package.json'

export class MongodbPlugin {
  namespace = 'mongodb'
  package = pkg.name
  type = 'MongodbPlugin'

  constructor({ namespace: ns, options, seed, type, packageName }) {
    this.namespace = ns || this.namespace
    this.options = options
    this.package = packageName || this.package
    this.type = type || this.type

    this.mongodbClient = new MongodbClient({
      namespace: this.namespace,
      options: this.options,
    })

    this.seed(seed, this.options)
  }

  seed = async (collections, options) => {
    try {
      if (!collections) return

      await this.mongodbClient.client(options)
      await this.mongodbClient.seed(collections)
    } catch (error) {
      throw error
    }
  }

  client = async options => {
    try {
      return this.mongodbClient.client(options)
    } catch (error) {
      throw error
    }
  }

  resolvers() {
    return mongoDbResolvers(...arguments)
  }

  typeDefs() {
    return mongoDbTypeDefs(...arguments)
  }
}
