import {
  collectionNameError,
  databaseConfigurationError,
  errorLogger,
  meta,
  noDataError,
  noInputError,
  resolverMutationError,
} from 'guru-utils'

import { ApolloError } from 'apollo-server'
import { execute } from '../execute.mongodb'

export function updateOneMutation(params) {
  const namespace = params.namespace
  const moduleName = params.moduleName
  const collection = params.collection
  const paramsContext = params.context
  const resolverName = `${moduleName}UpdateOneMutation`

  return async function updateOneMutationResolver(
    obj,
    args,
    context = {},
    info
  ) {
    try {
      const dbInstance = paramsContext[namespace]
        ? await paramsContext[namespace]
        : context[namespace]

      // Error Checking
      if (!dbInstance) {
        throw new databaseConfigurationError({ namespace, resolverName })
      }
      if (!collection) {
        throw new ApolloError(...collectionNameError({ resolverName }))
      }
      if (!args.data) {
        throw new ApolloError(...noDataError({ resolverName, args: args }))
      }
      if (!args.query) {
        throw new ApolloError(...noInputError({ resolverName, args: args }))
      }

      // Database query
      const mongodb = dbInstance.client()
      const db = await mongodb

      const response = await execute({
        args: args.query,
        col: collection,
        data: {
          $set: {
            ...args.data,
            userId: context.userId,
          },
        },
        db,
        method: 'updateOne',
        options: args.options,
      })

      if (response.error) {
        throw error
      }

      return {
        RESULTS: {
          _id: args.query._id,
          operation: resolverName,
          result: response.result.n > 0 ? 'ok' : 'failed',
          n: response.result.n,
        },
      }
    } catch (error) {
      errorLogger(error)

      return error.extensions
        ? error
        : new ApolloError(
            ...resolverMutationError({
              error,
              namespace,
              resolverName,
            })
          )
    }
  }
}
