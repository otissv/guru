import {
  collectionNameError,
  databaseConfigurationError,
  errorLogger,
  meta,
  noDataError,
  resolverMutationError,
} from 'guru-utils'

import { ApolloError } from 'apollo-server'

export function insertOneMutation(params) {
  const namespace = params.namespace
  const moduleName = params.moduleName
  const collection = params.collection
  const paramsContext = params.context
  const resolverName = `${moduleName}InsertOne`

  return async function insertOneMutationResolver(
    obj,
    args,
    context = {},
    info
  ) {
    try {
      const dbInstance = paramsContext[namespace]
        ? await paramsContext[namespace]
        : context[namespace]
      // Error Checking
      if (!dbInstance) {
        throw new ApolloError(
          ...databaseConfigurationError({ namespace, resolverName })
        )
      }
      if (!collection) {
        throw new ApolloError(...collectionNameError({ resolverName }))
      }

      if (!args.data) {
        throw new ApolloError(...noDataError({ resolverName, args: args }))
      }

      // Arguments
      const {
        data: { _id, ...data },
        options,
      } = args

      // Database query
      const mongodb = dbInstance.client()
      const db = await mongodb
      //
      //
      const response = await db
        .collection(collection)
        .insertOne(
          { ...data, meta: meta({ userId: context.userId, create: true }) },
          options
        )

      if (response.error) {
        throw error
      }

      return {
        RESULTS: {
          _id: response.insertedId,
          operation: resolverName,
          result: response.result.n > 0 ? 'ok' : 'failed',
          n: response.result.n,
        },
      }
    } catch (error) {
      errorLogger(error)

      return error.extensions
        ? error
        : new ApolloError(
            ...resolverMutationError({
              error,
              namespace,
              resolverName,
            })
          )
    }
  }
}
