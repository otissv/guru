import {
  collectionNameError,
  databaseConfigurationError,
  errorLogger,
  meta,
  noDataError,
  noInputError,
  resolverMutationError,
} from 'guru-utils'

import { ApolloError } from 'apollo-server'
import { execute } from '../execute.mongodb'

export function updateManyMutation(params) {
  const namespace = params.namespace
  const moduleName = params.moduleName
  const collection = params.collection
  const paramsContext = params.context
  const resolverName = `${moduleName}UpdateManyMutation`

  return async function updateManyMutationResolver(
    obj,
    args,
    context = {},
    info
  ) {
    try {
      const dbInstance = paramsContext[namespace]
        ? await paramsContext[namespace]
        : context[namespace]

      // Error Checking
      if (!dbInstance) {
        throw new ApolloError(
          ...databaseConfigurationError({ namespace, resolverName })
        )
      }
      if (!collection) {
        throw new ApolloError(...collectionNameError({ resolverName }))
      }
      if (!args.data) {
        throw new ApolloError(...noDataError({ resolverName, args: args }))
      }
      if (!args.query) {
        throw ApolloError(...noInputError({ resolverName, args: args }))
      }

      // Arguments
      const { data, options = {}, query } = args
      const _data = { ...data }
      _data.meta = meta({ userId: context.userId })

      // Database query
      const mongodb = dbInstance.client()
      const response = await mongodb.then(db =>
        execute({
          args: query,
          col: collection,
          db,
          options: { multi: true, ...options },
          data: { $set: _data },
          method: 'updateMany',
        })
      )

      if (response.error) {
        throw error
      }

      return {
        RESULTS: {
          operation: resolverName,
          result: response.result.n > 0 ? 'ok' : 'failed',
          n: response.result.n,
        },
      }
    } catch (error) {
      errorLogger(error)

      return error.extensions
        ? error
        : new ApolloError(
            ...resolverMutationError({
              error,
              namespace,
              resolverName,
            })
          )
    }
  }
}
