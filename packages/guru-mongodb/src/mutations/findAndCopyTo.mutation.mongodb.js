import {
  databaseConfigurationError,
  errorLogger,
  noInputError,
  resolverMutationError,
} from 'guru-utils'

import { ApolloError } from 'apollo-server'
import { execute } from '../execute.mongodb'

export function findAndCopyToMutation(params) {
  const namespace = params.namespace
  const moduleName = params.moduleName
  const collection = params.collection
  const paramsContext = params.context
  const resolverName = `${moduleName}Find`

  return async function findAndCopyToMutationResolver(
    obj,
    args,
    context = {},
    info
  ) {
    try {
      const dbInstance = paramsContext[namespace]
        ? await paramsContext[namespace]
        : context[namespace] // Error Checking
      if (!context || !context[namespace]) {
        throw databaseConfigurationError({ namespace, resolverName })
      }
      if (!collection) {
        throw errors.collectionNameError({ resolverName })
      }
      if (Object.keys(args).length > 0 && !args.options && !args.query) {
        throw noInputError({ resolverName, args: args })
      }

      // Arguments
      const { cursor, projection } = args.options || {}

      // Database execute
      const mongodb = context[namespace].client()
      return mongodb.then(db =>
        new Promise((resolve, reject) => {
          return execute({
            args: args.query,
            col: collection,
            cursor,
            db,
            method: 'find',
            projection,
          }).toArray(async (error, docs) => {
            try {
              if (error) {
                throw error
              }

              const response = await mongodb.then(db =>
                db.collection(collection).insertMany(docs, options)
              )

              if (response.error) {
                throw error
              }

              resolve({
                RESULTS: {
                  ids: response.ops.insertedIds,
                  operation: resolverName,
                  result: response.result.n > 0 ? 'ok' : 'failed',
                  n: response.result.n,
                },
              })
            } catch (error) {
              return Promise.reject(error)
            }
          })
        }).catch(error => {
          errorLogger(error)

          return error.extensions
            ? error
            : new ApolloError(
                resolverQueryError({
                  error,
                  namespace,
                  resolverName,
                })
              )
        })
      )
    } catch (error) {}
  }
}
