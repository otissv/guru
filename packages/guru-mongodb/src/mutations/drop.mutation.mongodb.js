import {
  collectionNameError,
  databaseConfigurationError,
  errorLogger,
  resolverMutationError,
} from 'guru-utils'

import { ApolloError } from 'apollo-server'

export function dropMutation(params) {
  const namespace = params.namespace
  const moduleName = params.moduleName
  const collection = params.collection
  const paramsContext = params.context
  const resolverName = `${moduleName}DropMutation`

  return async function dropMutationResolve(obj, args, context = {}, info) {
    try {
      const dbInstance = paramsContext[namespace]
        ? await paramsContext[namespace]
        : context[namespace]

      // Error Checking
      if (!dbInstance) {
        throw new ApolloError(
          ...databaseConfigurationError({ namespace, resolverName })
        )
      }
      if (!collection) {
        throw new ApolloError(...collectionNameError({ resolverName }))
      }

      // Database query
      const mongodb = dbInstance.client()
      const response = await mongodb.then(db =>
        db.collection(collection).drop()
      )

      if (response.error) {
        throw error
      }

      return {
        RESULTS: {
          operation: resolverName,
          result: response ? 'ok' : 'failed',
        },
      }
    } catch (error) {
      errorLogger(error)

      return error.extensions
        ? error
        : new ApolloError(
            ...resolverMutationError({
              error,
              namespace,
              resolverName,
            })
          )
    }
  }
}
