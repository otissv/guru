import {
  collectionNameError,
  databaseConfigurationError,
  errorLogger,
  meta,
  noDataError,
  resolverMutationError,
} from 'guru-utils'

import { ApolloError } from 'apollo-server'

export function insertManyMutation(params) {
  const namespace = params.namespace
  const moduleName = params.moduleName
  const collection = params.collection
  const paramsContext = params.context
  const resolverName = `${moduleName}InsertMany`

  return async function insertManyMutationResolver(
    obj,
    args,
    context = {},
    info
  ) {
    try {
      const dbInstance = paramsContext[namespace]
        ? await paramsContext[namespace]
        : context[namespace]

      // Error Checking
      if (!dbInstance) {
        throw new ApolloError(
          ...databaseConfigurationError({ namespace, resolverName })
        )
      }
      if (!collection) {
        throw new ApolloError(...collectionNameError({ resolverName }))
      }
      if (!args.data) {
        throw new ApolloError(...noDataError({ resolverName }))
      }
      args.data.forEach(item => {
        if (!item || Object.keys(item).length === 0) {
          throw new ApolloError(...noDataError({ resolverName }))
        }
      })

      // Arguments
      const { data, options } = args

      const tmpArgs = data.map(({ id, ...item }) => ({
        ...item,
        _id: id,
        meta: meta({ userId: context.userId, create: true }),
      }))

      // Database query
      const mongodb = dbInstance.client()
      const db = await mongodb

      const response = await db.collection(collection).insertMany(args.data)

      if (response.error) {
        throw error
      }

      return {
        RESULTS: {
          ids: response.ops.insertedIds,
          operation: resolverName,
          result: response.result.n > 0 ? 'ok' : 'failed',
          n: response.result.n,
        },
      }
    } catch (error) {
      errorLogger(error)

      return error.extensions
        ? error
        : new ApolloError(
            ...resolverMutationError({
              error,
              namespace,
              resolverName,
            })
          )
    }
  }
}
