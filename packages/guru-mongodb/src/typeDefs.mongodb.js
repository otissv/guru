import {
  Box,
  buildTypeDefs,
  getObjectDirectiveArguments,
  typeEngine,
} from 'guru-utils'

import camelCase from 'lodash/fp/camelCase'
import { definitions } from './definitions'
import upperFirst from 'lodash/fp/upperFirst'

export function mongoDbTypeDefs({ typeDefs, namespace, nonInputTypes }) {
  const { inputs, types } = buildTypeDefs(
    typeDefs,
    nonInputTypes,
    ({ current, input }) => {
      const { engine } = getObjectDirectiveArguments(current)
      const dbEngine = typeEngine({
        engine,
        namespace,
        type: 'typeDef',
        name: 'MONGODB',
      })

      if (!dbEngine) return { inputs: '', types: '' }
      const nameCamelCase = camelCase(current.name.value)

      const name = Box(current.name.value)
        .map(str => camelCase(str))
        .map(str => upperFirst(str))
        .fold(str => str)

      const logicalOperators = `
        input InputMongodbLogicalOperators {
          ${input.fields}
          # Joins query clauses with a logical AND returns all documents that match the conditions of both clauses.
          AND: InputMongodbLogicalOperators
          # Inverts the effect of a query expression and returns documents that do not match the query expression.
          NOT: InputMongodbLogicalOperators
          # Joins query clauses with a logical NOR returns all documents that fail to match both clauses.
          NOR: InputMongodbLogicalOperators
          # Joins query clauses with a logical OR returns all documents that match the conditions of either clause.
          OR: InputMongodbLogicalOperators
          SELECT: 
        }
        `

      return {
        inputs: `
        input Input${name} {
          ${input.fields}
        }

        input Input${name}ById {
          _id: [String]
        }

        input Input${name}OneById {
          _id: String
        }
    
        input InputNested${name} {
          ${input.fields}
        }

        input Input${name}Projection {
          ${input.projection}
        }
      `,
        types: `
        type Query {
          ${nameCamelCase}Aggregate(query: Input${name}, projection: Input${name}Projection, options: InputAggregateOptions, cursor: InputMongodbCursor):  [${name}]
          ${nameCamelCase}Count(query: Input${name}, projection: Input${name}Projection, cursor: InputMongodbCursor): ${name}
          ${nameCamelCase}Distinct(query: Input${name}, projection: Input${name}Projection, cursor: InputMongodbCursor): ${name}
          ${nameCamelCase}Find(query: Input${name}, projection: Input${name}Projection, cursor: InputMongodbCursor): [${name}]
          ${nameCamelCase}FindOne(query: Input${name}, projection: Input${name}Projection, cursor: InputMongodbCursor): ${name}
          ${nameCamelCase}FindById(query: Input${name}ById, projection: Input${name}Projection, cursor: InputMongodbCursor): [${name}]
          ${nameCamelCase}FindOneById(query: Input${name}OneById, projection: Input${name}Projection, cursor: InputMongodbCursor): ${name}
          ${nameCamelCase}Resolve(query: Input${name}, projection: Input${name}, cursor: InputMongodbCursor): ${name}
        }

        type Mutation {
          ${nameCamelCase}FindAndCopyTo(query: InputFindAndCopyTo, options: InputFindAndCopyToOptions): ${name}
          ${nameCamelCase}InsertOne(query: Input${name}, data: Input${name}, options: InputInsertOneOptions): ${name}
          ${nameCamelCase}InsertMany(query: Input${name}, data: Input${name}, ): [${name}]
          ${nameCamelCase}Drop(query: Input${name}, data: Input${name}, options: InputDropOptions): ${name}
          ${nameCamelCase}DeleteMany(query: Input${name}, data: Input${name}, options: InputDeleteManyOptions): ${name}
          ${nameCamelCase}DeleteOne(query: Input${name}, data: Input${name}, options: InputDeleteOneOptions): ${name}
          ${nameCamelCase}UpdateOne(query: Input${name}, data: Input${name}, options: InputUpdateOptions): ${name}
          ${nameCamelCase}UpdateMany(query: Input${name}, data: Input${name}, options: InputUpdateManyOptions): [${name}]
          ${nameCamelCase}UpdateManyById(query: Input${name}, data: Input${name}, options: InputUpdateManyByIdOptions): [${name}]
        }
      `,
      }
    }
  )

  return {
    inputs: `
      ${inputs}
      ${definitions.inputs(namespace)}
    `,
    types: `
      ${types}
      ${definitions.types(namespace)}
    `,
  }
}
