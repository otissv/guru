import { mongodbCollections } from './collections.definition.mongodb'
import { mongodbIndex } from './index.definition.mongodb'
import { mongodbInputs } from './inputs.definition.mongodb'
import { mongodbEngine } from './engine.definition.mongodb'

export const definitions = {
  inputs: namespace => mongodbInputs(namespace),
  types: namespace => `
    ${mongodbCollections(namespace)}
    ${mongodbIndex(namespace)}
    ${mongodbEngine(namespace)}
  `,
}
