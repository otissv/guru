import camelCase from 'lodash/camelCase'
import upperFirst from 'lodash/upperFirst'

export function mongodbCollections(namespace) {
  const moduleName = camelCase(namespace)
  const typeName = upperFirst(moduleName)

  return `# ${moduleName}Collection type
type ${typeName}Collection
  @engine(namespace: "${moduleName}", resolver: false, input: false, typeDef: false) {
  # Engine namespace
  engine: String
  # Collection name.
  name: String
  # JSON containing the document data.
  data (query: JSON, projection: JSON, options: InputMongodbQueryOptions): JSON
  # Collections indexes.
  indexes: JSON
  # Number of documents in the collection
  count: Int
  
  RESULTS: [RESULTS]
}

type ${typeName}Document
  @engine(namespace: "${moduleName}", resolver: false, input: false, typeDef: false) {
  # Engine namespace
  engine: String
  # Collection name.
  collection: String
  # JSON containing the document data.
  data (query: JSON, projection: JSON, options: InputMongodbQueryOptions) : JSON
  # Collections indexes.

  RESULTS: [RESULTS]
}

type ${typeName}CollectionStats 
  @engine(namespace: "${moduleName}", resolver: false, input: false, typeDef: false) {
  ns: String
  size: Float
  count: Float
  avgObjSize: Float
  storageSize: Float
  capped: Boolean
  wiredTiger: ${typeName}WireTiger
}

type ${typeName}WireTiger {
  metadata: ${typeName}WireTigerMetadata
  creationString: String
  type: String
  uri: String
  LSM: ${typeName}WireTigerLSM
  blockManager: ${typeName}WireTigerBlockManager
  btree: ${typeName}WireTigerBtree
  cache: ${typeName}WireTigerTCache
  compression: ${typeName}WireTigerCompression
  cursor: ${typeName}WireTigerCursor
  reconciliation: ${typeName}WireTigerReconciliation
  session: ${typeName}WireTigerSession
  transaction: ${typeName}WireTigerTransaction
  nindexes: Int
  indexDetails: JSON
}

type ${typeName}WireTigerMetadata {
  formatVersion: Float
}

type ${typeName}WireTigerLSM {
  bloomFilterFalsePositives: Float
  bloomFilterHits: Float
  bloomFilterMisses: Float
  bloomFilterPagesEvictedFromCache: Float
  bloomFilterPagesReadIntoCache: Float
  bloomFiltersInTheLSMTree: Float
  chunksInTheLSMTree: Float
  highestMergeGenerationInThLSMTree: Float
  queriesThatCouldHaveBenefitedFromABloomFilterThatDidNotExist: Float
  sleepForLSMCheckpointThrottle: Float
  sleepForLSMMergeThrottle: Float
  totalSizeOfBloomFilters: Float
}

type ${typeName}WireTigerBlockManager {
  allocationsRequiringFleExtension: Float
  blocksAllocated: Float
  blocksFreed: Float
  checkpointSize: Float
  fileAllocationUniSize: Float
  fileBytesAvailableForReuse: Float
  fileMagicNumber: Float
  fileMajorVersionNumber: Float
  fileSizeInBytes: Float
  minorVersionNumber: Float
}

type ${typeName}WireTigerBtree {
  btreeCheckpointGeneration: Float
  columnStoreFixedSizeLeafPages: Float
  columnStoreInternalPages: Float
  columnStoreVariableSizeRLEEncodedValues: Float
  columnStoreVariableSizeDeletedValues: Float
  columnStoreVariableSizeLeafPages: Float
  fixedRecordSize: Float
  maximumInternalPageKeySize: Float
  maximumInternalPageSize: Float
  maximumLeafPageKeySize: Float
  maximumLeafPagesize: Float
  maximumLeafPageValueSize: Float
  maximumTreeDepth: Float
  numberOfKeyValuePairs: Float
  overflowPages: Float
  pagesRewrittenByCompaction: Float
  rowStoreInternalPages: Float
  rowStoreLeafPages: Float
}

type ${typeName}WireTigerTCache {
  bytesCurrentlyInTheCache: Float
  bytesDirtyInTheCacheCumulative: Float
  bytesReadIntoCache: Float
  bytesWrittenFromCache: Float
  checkpointBlockedPageEviction: Float
  dataSourcePagesSelectedForEvictionUnableToBeEvicted: Float
  evictionWalkPassesOfAFile: Float
  evictionWalkTargetPagesHistogram09: Float
  evictionWalkTargetPagesHistogram1031: Float
  evictionWalkTargetPagesHistogram128Andhigher: Float
  evictionWalkTargetPagesHistogram3263: Float
  evictionWalkTargetPagesHistogram64128: Float
  evictionWalksAbandoned: Float
  evictionWalksGaveupbecausetheyrestartedtheirWalkTwice: Float
  evictionWalksGaveupbecausetheySawtoManyPagesAndFoundNoCandidates: Float
  evictionWalksGaveupbecausetheySawtoManyPagesAndFoundTooFeCandidates: Float
  evictionWalksReachedEndOfTree: Float
  evictionWalksStartedFromRootOfTree: Float
  evictionWalksStartedFromSavedLocationInTree: Float
  hazardPointerBlockedPageEviction: Float
  inMemoryPagePassedCriteriaToBeSplit: Float
  inMemoryPageSplits: Float
  internalPagesEvicted: Float
  internalPagesSplitDuringEviction: Float
  leafPagesSplitDuringEviction: Float
  modifiedPagesEvicted: Float
  overflowPageReadIntoCache: Float
  pageSplitDuringEvictionDeepenedTheTree: Float
  pageWrittenRequiringCacheOverflowRecords: Float
  pagesReadIntoCache: Float
  pagesReadIntoCacheAfterTruncate: Float
  pagesReadIntoCacheAfterTruncateInPrepareState: Float
  pagesReadIntoCacheRequiringCache0verflowEntries: Float
  pagesRequestedFromTheCache: Float
  pagesSeenByEvicticolumnStoreVariableSizeRLEEncodedValuesonWalk: Float
  pagesWrittenFromCache: Float
  pagesWrittenRequiringInMemoryRestoration: Float
  trackedDirtyBytesInTheCache: Float
  unmodifiedPagesEvicted: Float
}


type ${typeName}WireTigerCacheWalk {
  AverageDifferenceBetweenCurrentEvictionGenerationWhenThePageWasLastConsidered: Float
  AverageOnDiskPageImageSizeSeen: Float
  AverageTimeInCacheForPagesThatHavBeenVisitedByTheEvictionServer: Float
  AverageTimeInCacheForPagesThatHavenoBeenVisitedByTheEvictionServer: Float
  CleanPagesCurrentlyInCache: Float
  CurrentEvictionGeneration: Float
  DirtyPagesCurrentlyInCache: Float
  EntriesInTheRootPage: Float
  InternalPagesCurrentlyInCache: Float
  LeafPagesCurrentlyInCache: Float
  MaximumDifferenceBetweenCurrentEvictionGenerationWhenThePageWasLastConsidered: Float
  MaximumPageSizeSeen: Float
  MinimumOnDiskPageImageSizeSeen: Float
  NumberOfPagesmoduleNameNeverVisitedByEvictionServer: Float
  OnDiskPageImageSizesSmallerThanASingleAllocationUnit: Float
  PagesCreatedInMemoryAndNeverWritten: Float
  PagesCurrentlyQueuedForEviction: Float
  PagesThatCouldNotBeQueuedForEviction: Float
  RefsSkippedDuringCacheTraversal: Float
  SizeOfTheRootPage: Float
  TotalNumberOfPagesCurrentlyInCache: Float
}

type ${typeName}WireTigerCompression {
  compressedPagesRead: Float
  compressedPagesWritten: Float
  pageWrittenFailedToCompress: Float
  pageWrittenWasTooSmallToCompress: Float
  rawCompressionCallFailedAdditionalDataAvailable: Float
  rawCompressionCallFailedNoAdditionalDataAvailable: Float
  rawCompressionCallSucceeded: Float
}

type ${typeName}WireTigerCursor {
  bulkLoadedCursorInsertCalls: Float
  closeCallsThatResultInCache: Float
  createCalls: Float
  cursorOperationRestarted: Float
  cursorInsertKeyAndValueBytesInserted: Float
  cursorRemoveKeyBytesRemoved: Float
  cursorUpdateValueBytesUpdated: Float
  cursorsReusedFromCache: Float
  insertCalls: Float
  modifyCalls: Float
  nextCalls: Float
  prevCalls: Float
  removeCalls: Float
  reserveCalls: Float
  resetCalls: Float
  searchCalls: Float
  searchNearCalls: Float
  truncateCalls: Float
  updateCalls: Float
}

type ${typeName}WireTigerReconciliation {
  dictionaryMatches: Float
  fastPathPagesDeleted: Float
  internalPageKeyBytesDiscardedUsingSuffixCompression: Float
  internalPageMultiBlockWrites: Float
  internalPageOverflowKeys: Float
  leafpageKeyBytesDiscardedUsingPrefixCompression: Float
  leafPageMultiBlockWrites: Float
  leafPageOverflowKeys: Float
  maximumBlocksRequiredForAPage: Float
  overflowValuesWritten: Float
  pageChecksumMatches: Float
  pageReconciliationCalls: Float
  pageReconciliationCallsForEviction: Float
  pagesDeleted: Float
}

type ${typeName}WireTigerSession {
  cachedCursorCount: Float
  objectCompaction: Float
  openCursorCount: Float
}

type ${typeName}WireTigerTransaction {
  updateConflicts: Float
}

type Mutation {
  # Create a MongoDB document
  ${moduleName}CollectionCreate(
    # The name of the collection where the document will be created.
    collection: String
    # JSON containing the document data.
    data: JSON
  ): ${typeName}Collection

  # Drop a MongoDB collection.
  ${moduleName}CollectionDrop(
    # The name of the collection to be dropped.
    name: String
  ): ${typeName}Collection

  # Drop multiple MongoDB collections.
  ${moduleName}CollectionDropMany(
    # The name of the collection to be dropped.
    names: [String]
  ): ${typeName}Collection

  # Create a MongoDB collection
  ${moduleName}CollectionCreate(
    # The name of the collection to create.
    name: String
    # Collection options
    options: InputMongodbCollectionCreateOptions
  ): ${typeName}Collection

  # Inserts a document a MongoDB new document
  ${moduleName}CollectionInsertOneDocument(
    # The name of the collection where the document will be inserted.
    collection: String
    # JSON containing the document data.
    data: JSON
  ): ${typeName}Document

  # Inserts many a MongoDB new document
  ${moduleName}CollectionInsertManyDocuments(
    # The name of the collection where the document will be created.
    collection: String
    # JSON containing the document data.
    data: [JSON]
  ): ${typeName}Document

  # Remove a MongoDB documents
  ${moduleName}CollectionRemoveDocument(
    # The name of the collection where the document will be removed.
    collection: String
    # The ID of the document to be removed.
    _id: String
  ): ${typeName}Collection


  # Remove a MongoDB documents
  ${moduleName}CollectionRemoveDocument(
    # The name of the collection where the document will be removed.
    collection: String
    # The ID of the document to be removed.
    _id: String
  ): ${typeName}Collection

  # Remove documents from a MongoDB collection
  ${moduleName}CollectionRemoveDocuments(
    # The name of the collection to remove documents from.
    collection: String
    # Document ids to be dropped. If no ids provided all document will be removed
    _ids: [String]
  ): ${typeName}Collection

  # Renames a MongoDB collection.
  ${moduleName}CollectionRename(
    # The name of the collection to be renamed.
    name: String
    # The new name of the collection.
    target: String
    # Optional. If true, mongodb drops the target of renameCollection prior to renaming the collection. The default value is false.
    dropTarget: Boolean
  ): ${typeName}Collection


  # Update MongoDB documents
  ${moduleName}CollectionUpdateOneDocument(
    # The name of the collection where the document will be created.
    collection: String
    # JSON containing the document data.
    data: JSON
    query: JSON
  ): ${typeName}Document
}

type Query {
  # Finds all MongoDB collection names
  ${moduleName}CollectionFind(
    # Collection name
    name: String
    query: JSON
    # Database options
    InputMongodbQueryOptions: InputMongodbQueryOptions
  ): [${typeName}Collection]

  # Finds all MongoDB documents in a collection
  ${moduleName}CollectionFindDocuments(
    # Collection name
    collection: String
    query: JSON
    # Database options
    InputMongodbQueryOptions: InputMongodbQueryOptions
  ): ${typeName}Document

  # Returns collection statistics
  ${moduleName}CollectionStats (
    # Collection name
    name: String
    # The scale at which to deliver results. Unless specified, this command returns all data in bytes.
    scale: String
    query: JSON
  ): ${typeName}CollectionStats 
}
  `
}
