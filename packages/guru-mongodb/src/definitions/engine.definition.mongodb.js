import camelCase from 'lodash/camelCase'
import upperFirst from 'lodash/upperFirst'

export function mongodbEngine(namespace) {
  const moduleName = camelCase(namespace)
  const typeName = upperFirst(moduleName)

  return `# ${typeName}Index type
type Query {
  ${moduleName}EngineStats(
    # The scale at which to deliver results. Unless specified, this command returns all data in bytes.
    scale: String
  ): EngineStats

  engineStatsResolve: EngineStats
  engineCollectionResolve: EngineCollection
}
`
}
