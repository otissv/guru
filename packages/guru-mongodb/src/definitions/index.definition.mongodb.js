import camelCase from 'lodash/camelCase'
import upperFirst from 'lodash/upperFirst'

export function mongodbIndex(namespace) {
  const moduleName = camelCase(namespace)
  const typeName = upperFirst(moduleName)

  return `# ${typeName}Index type
type ${typeName}Index
  @engine(namespace: "MONGODB", resolver: false, input: false, typeDef: false) {
  # Engine namespace
  engine: String
  # A JSON string that contains the field and value pairs where the field is the index key and the value describes the type of index for that field. For an ascending index on a field, specify a value of 1; for descending index, specify a value of -1.
  key: String
  # Index name.
  name: String
  # The type of index for that field.
  value: String
}

type Mutation {
  # InsertOnes indexes on a MongoDB collection.
  ${moduleName}IndexCreate(
    # Collection name.
    name: String
    #A list that contains the field and value pairs where the field is the index key and the value describes the type of index for that field. For an ascending index on a field, specify a value of 1; for descending index, specify a value of -1.
    index: InputMongodbIndex
    #Optional. A document that contains a set of options that controls the creation of the index.
    options: InputMongodbIndexCreateOptions
  ): ${typeName}Index

  # Drops or removes the specified index from a MongoDB collection
  ${moduleName}IndexDrop(
    # Collection name.
    name: String
    # Specifies the index to drop.
    index: InputMongodbIndex
  ): ${typeName}Index

  # Drops all indexes other than the required index on the id field of a MongoDB collection
  ${moduleName}IndexDropAll(
    # Collection name.
    name: String
  ): ${typeName}Index
}

type Query {
  # Finds a MongoDB collection's indexes
  ${moduleName}IndexFindByCollection(
    # Collection name.
    name: String
  ): [${typeName}Index]
}
`
}
