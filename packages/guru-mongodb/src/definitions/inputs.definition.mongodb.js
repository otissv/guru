export function mongodbInputs(namespace) {
  return `

# MongoDB input Find And Copy To
input InputFindAndCopyTo {
  # Name of the collection the documents will be copied to.
  collection: String!
}
# MongoDB input Aggregate Options
input InputAggregateOptions {
  # Specifies to return the information on the processing of the pipeline.
  explain: Boolean
  # Enables writing to temporary files.
  allowDiskUse: Boolean
  # Specifies the initial batch size for the query.
  query: JSON
  # Specifies a time limit in milliseconds for processing operations on a query.
  maxTimeMS: Int
  # Variable only if you specify the $out aggregation operator.
  BypassDocumentValidation: Boolean
  #  Specifies the read concern.
  readConcern: JSON
  # Allows specifying language-specific rules for string comparison.
  collation: JSON
}

# MongoDB insertOne Options
input InputInsertOneOptions {
  # The write concern.
  w: JSON
  # The write concern timeout.
  wtimeout: Int
  # Specify a journal write concern.
  j: Boolean
  # Serialize functions on any object.
  serializeFunctions: Boolean
  # Force server to assign id values instead of driver.
  forceServerObjectId: Boolean
  # Allow driver to bypass schema validation.
  bypassDocumentValidation: Boolean
  # If true when an insert fails don't execute the remaining writes. If false continue with remaining inserts when one fails. Only avaible for insertMany.
  ordered: Boolean
}

input InputInsertManyOptions {
  # The write concern.
  w: JSON
  # The write concern timeout.
  wtimeout: Int
  # Specify a journal write concern.
  j: Boolean
  # Serialize functions on any object.
  serializeFunctions: Boolean
  # Force server to assign id values instead of driver.
  forceServerObjectId: Boolean
  # Allow driver to bypass schema validation.
  bypassDocumentValidation: Boolean
  # If true when an insert fails don't execute the remaining writes. If false continue with remaining inserts when one fails. Only avaible for insertMany.
  ordered: Boolean
}

# MongoDB Input Distinct
input InputDistinctOptions {
  # The field for which to return distinct values.
  field: String
  # Specifies the documents from which to retrieve the distinct values.
  query: JSON
  # Allows specifying language-specific rules for string comparison.
  collation: JSON
}
# MongoDB Input Drop
input InputDropOptions {
  # Optional. A document expressing the write concern of the db.collection.drop() operation. 
  writeConcern: String
}

# MongoDB Input Find And Copy To
input InputFindAndCopyToOptions {
  # The write concern.
  w: JSON
  # The write concern timeout.
  wtimeout: Int
  # Specify a journal write concern.
  j: Boolean
  # Serialize functions on any object.
  serializeFunctions: Boolean
  # Force server to assign id values instead of driver.
  forceServerObjectId: Boolean
  # Allow driver to bypass schema validation.
  bypassDocumentValidation: Boolean
  # If true when an insert fails don't execute the remaining writes. If false continue with remaining inserts when one fails. Only avaible for insertMany.
  ordered: Boolean
}

# MongoDB Input Find And Modify
input InputFindAndModifyOptions {
  # The selection criteria for the modification.
  query: JSON
  # Determines which document the operation modifies if the query selects multiple documents.
  sort: JSON
  # Must specify either the remove or the update field. Removes the document specified in the query field.
  remove: JSON
  # Must specify either the remove or the update field. Performs an update of the selected document.
  update: JSON
  # When true, returns the modified document rather than the original.
  new: Boolean
  # A subset of fields to return.
  fields: JSON
  # Used in conjunction with the update field.
  upsert: Boolean
  # Enables db.collection.findAndModify to bypass document validation during the operation.
  bypassDocumentValidation: Boolean
  # Specifies a time limit in milliseconds for processing the operation.
  maxTimeMS: Int
  # A document expressing the write concern. Omit to use the default write concern.
  writeConcern: JSON
  # Allows specifying language-specific rules for string comparison.
  collation: JSON
}

# MongoDB Input Find One And Delete
  input InputFindOneAndDeleteOptions {
    # A subset of fields to return.
    projection: JSON
    # Specifies a sorting order for the documents matched by.
    sort: JSON
    # Specifies a time limit in milliseconds within which the operation must complete within.
    maxTimeMS: Int
    #  Allows users to specify language-specific rules for string comparison, such as rules for lettercase and accent marks.
    collation: JSON
  }


# MongoDB Input Find One And Replace
input InputFindOneAndReplaceOptions {
  #A subset of fields to return.
  projection: JSON
  # The replacement document.
  replacement: JSON
  # Specifies a sorting order for the documents matched by.
  sort: JSON
  # Specifies a time limit in milliseconds within which the operation must complete within.
  maxTimeMS: Int
  # If true inserts the document from the replacement parameter if no document matches. Else, replaces the document that matches.
  upsert: Boolean
  # When true, returns the replacement document instead of the original document.
  returnNewDocument: Boolean
  #  Allows users to specify language-specific rules for string comparison, such as rules for lettercase and accent marks.
  collation: JSON
}

  
# MongoDB Input Find One And Update
input InputFindOneAndUpdateOptions {
  # The update document.
  update: JSON
  #A subset of fields to return.
  projection: JSON
  # Specifies a sorting order for the documents matched by.
  sort: JSON
  # Specifies a time limit in milliseconds within which the operation must complete within.
  maxTimeMS: Int
  # If true inserts the document from the replacement parameter if no document matches. Else, replaces the document that matches.
  upsert: Boolean
  # When true, returns the replacement document instead of the original document.
  returnNewDocument: Boolean
  #  Allows users to specify language-specific rules for string comparison, such as rules for lettercase and accent marks.
  collation: JSON
}

# MongoDB Input DeleteMany
input InputDeleteManyOptions {
  #  To limit the deletion to just one document, set to true.
  justOne: Boolean
  # A document expressing the write concern. Omit to use the default write concern
  writeConcern: JSON
  # Allows users to specify language-specific rules for string comparison, such as rules for lettercase and accent marks.
  collation: JSON
}
# MongoDB Input DeleteOne
input InputDeleteOneOptions {
  #  To limit the deletion to just one document, set to true.
  justOne: Boolean
  # A document expressing the write concern. Omit to use the default write concern
  writeConcern: JSON
  # Allows users to specify language-specific rules for string comparison, such as rules for lettercase and accent marks.
  collation: JSON
}


# MongoDB save Options
input InputSaveOptions {
  # The write concern.
  w: JSON
  # The write concern timeout.
  wtimeout: Int
  # Specify a journal write concern.
  j: Boolean
  # Update operation is an upsert.
  upsert: Boolean
  # Update one/all documents with operation. Available for update operation only.
  multi: Boolean
  # If true when an insert fails don't execute the remaining writes. If false continue with remaining inserts when one fails. Only avaible for insertMany.
  bypassDocumentValidation: Boolean
  # Allows specifying language-specific rules for string comparison.
  collation: JSON
}

# MongoDB update Options
input InputUpdateOptions {
  # The write concern.
  w: JSON
  # The write concern timeout.
  wtimeout: Int
  # Specify a journal write concern.
  j: Boolean
  # Update operation is an upsert.
  upsert: Boolean
  # Update one/all documents with operation. Available for update operation only.
  multi: Boolean
  # If true when an insert fails don't execute the remaining writes. If false continue with remaining inserts when one fails. Only avaible for insertMany.
  bypassDocumentValidation: Boolean
  # Allows specifying language-specific rules for string comparison.
  collation: JSON
}

input InputUpdateManyOptions{
  # The write concern.
  w: JSON
  # The write concern timeout.
  wtimeout: Int
  # Specify a journal write concern.
  j: Boolean
  # Update operation is an upsert.
  upsert: Boolean
  # Update one/all documents with operation. Available for update operation only.
  multi: Boolean
  # If true when an insert fails don't execute the remaining writes. If false continue with remaining inserts when one fails. Only avaible for insertMany.
  bypassDocumentValidation: Boolean
  # Allows specifying language-specific rules for string comparison.
  collation: JSON
}

input InputUpdateManyByIdOptions {
  # The write concern.
  w: JSON
  # The write concern timeout.
  wtimeout: Int
  # Specify a journal write concern.
  j: Boolean
  # Update operation is an upsert.
  upsert: Boolean
  # Update one/all documents with operation. Available for update operation only.
  multi: Boolean
  # If true when an insert fails don't execute the remaining writes. If false continue with remaining inserts when one fails. Only avaible for insertMany.
  bypassDocumentValidation: Boolean
  # Allows specifying language-specific rules for string comparison.
  collation: JSON
}


# MongoDB Input Projection
input InputMongodbProjection {
  # Projects the first element in an array that matches the specified ELEMNET_MATCH condition.
  ELEMENT_MATCH: JSON
  # Projects the first element in an array that matches the query condition.
  FIRST_ELEMENT: Int
  # Projects the document’s score assigned during TEXT operation.
  META: String
  # Limits the number of elements projected from an array. Supports skip and limit slices.
  SLICE: JSON
}

# Optional input options for creating a Mongodb collection.
input InputMongodbCollectionInsertOneOptions {
  # To create a capped collection, specify true. If you specify true, you must also set a maximum size in the size field.
  capped: Boolean
  # Specify false to disable the automatic creation of an index on the id field.
  autoIndexId: Boolean
  #	Specify a maximum size in bytes for a capped collection. Once a capped collection reaches its maximum size, MongoDB removes the older documents to make space for the new documents. The size field is required for capped collections and ignored for other collections.
  size: Int
  #	The maximum number of documents allowed in the capped collection. The size limit takes precedence over this limit. If a capped collection reaches the size limit before it reaches the maximum number of documents, MongoDB removes old documents. If you prefer to use the max limit, ensure that the size limit, which is required for a capped collection, is sufficient to contain the maximum number of documents.
  max: Int
  # Available for the MMAPv1 storage engine only. noPadding flag disables the power of 2 sizes allocation for the collection. With noPadding flag set to true, the allocation strategy does not include additional space to accommodate document growth, as such, document growth will result in new allocation. Use for collections with workloads that are insert-only or in-place updates (such as incrementing counters).
  noPadding: Boolean
  # Available for the WiredTiger storage engine only. Allows users to specify configuration to the storage engine on a per-collection basis when creating a collection.
  storageEngine: String
  # Allows users to specify validation rules or expressions for the collection.
  validator: String
  # Determines how strictly MongoDB applies the validation rules to existing documents during an update.
  validationLevel: String
  # Determines whether to error on invalid documents or just warn about the violations but allow invalid documents to be inserted.
  validationAction: String
  # Allows users to specify a default configuration for indexes when creating a collection.
  indexOptionDefaults: String
  # The name of the source collection or view from which to create the view. The name is not the full namespace of the collection or view; i.e. does not include the database name and implies the same database as the view to create.
  viewOn: String
  # An array that consists of the aggregation pipeline stage. db.createView creates the view by applying the specified pipeline to the viewOn collection or view. The view definition is public; i.e. db.getCollectionInfos() and explain operations on the view will include the pipeline that defines the view. As such, avoid referring directly to sensitive fields and values in view definitions.
  pipeline: String
  # Specifies the default collation for the collection. Collation allows users to specify language-specific rules for string comparison, such as rules for lettercase and accent marks.
  collation: String
}

input InputMongodbQueryOptions {
  _id: String
}

# mongodbIndex keys field
input InputMongodbIndex {
  # The index key.
  key: String
  # The type of index for that field. For an ascending index on a field, specify a value of 1; for descending index, specify a value of -1.
  value: String
}

# # mongodbIndexCreateIndex options fields
input InputMongodbIndexCreateOptions {
  # Builds the index in the background so that building an index does not block other database activities. Specify true to build in the background. The default value is false.
  background: Boolean
  # InsertOnes a unique index so that the collection will not accept insertion of documents where the index key or keys match an existing value in the index. Specify true to create a unique index. The default value is false.
  unique: Boolean
  # The name of the index. If unspecified, MongoDB generates an index name by concatenating the names of the indexed fields and the sort order.
  name: String
  # JSON String. If specified, the index only references documents that match the filter expression.
  partialFilterExpression: String
  #  If true, the index only references documents with the specified field. These indexes use less space but behave differently in some situations (particularly sorts). The default value is false.
  sparse: Boolean
  # Specifies a value, in seconds, as a TTL to control how long MongoDB retains documents in this collection.
  expireAfterSeconds: Int
  # JSON String that allows users to specify configuration to the storage engine on a per-index basis when creating an index.
  storageEngine: String
}

# MongoDB Input Cursors
input InputMongodbCursor {
  # Adds special wire protocol flags that modify the behavior of the query.
  addOption: String
  # Specifies the number of documents to return in each batch.
  batchSize: Int
  # Adds a comment to the query query allowing for tracking the comment in the log.
  comment: String
  # Allows specifing language-specific rules for string comparison.
  collation: InputMongodbCursorCollation
  # Counts the number of documents referenced by a query. No Documents are returned.
  count: Boolean
  # Provides information on the query plan. No Documents are returned.
  explain: JSON
  # The filter object used for the query.
  filter: JSON
  # Alias for limit. The number of documents to limit the query too.
  first: Int
  # Iterates the query to apply a JavaScript function to each document from the query. No Documents are returned.
  forEach: JSON
  # If specified then the query system will only consider plans using the hinted index. No Documents are returned.
  hint: JSON
  # The number of sorted documents to limit the query too. Alias for limit.
  last: JSON
  # The number of documents to limit the query too.
  limit: Int
  # Applies function to each document visited by the query and collects the return values from successive application into an array.
  map: JSON
  # Set the query max to specify the exclusive upper bound for a specific index in order to constrain the results of find.
  max: JSON
  # Constrains the query to only scan the specified number of documents when fulfilling the query
  maxScan: JSON
  # Number of milliseconds to wait before aborting the tailed query.
  maxAwaitTimeMS: JSON
  # Number of milliseconds to wait before aborting the query
  maxTimeMS: JSON
  # Set the query min to specify the exclusive lower bound for a specific index in order to constrain the results of find.
  min: JSON
  # Sets a field projection for the query.
  project: InputMongodbCursorJSON
  # Set the query returnKey
  returnKey: Int
  # Modifies the output of a query by adding a field $recordId to matching documents
  showRecordId: Boolean
  # The number of documents the query should skip before the query returns results.
  skip: Int
  # The order in which the query returns matching documents
  sort: JSON
}

# MongoDB Cursor Boolean
input InputMongodbCursorBoolean {
  name: String
  value: Boolean
}

# MongoDB Cursor JSON
input InputMongodbCursorJSON {
  name: String
  value: Boolean
}

# MongoDB Cursor Collation
input InputMongodbCursorCollation {
  # The ICU locale.
  locale: String
  # Optional. The level of comparison to perform.
  strength: Int
  # Optional. Flag that determines whether to include case comparison at strength level 1 or 2.
  caseLevel: Boolean
  # Optional. A field that determines sort order of case differences during tertiary level comparisons.
  caseFirst: String
  # Optional. Flag that determines whether to compare numeric strings as numbers or as strings.
  numericOrdering: Boolean
  # Optional. Field that determines whether collation should consider whitespace and punctuation as base characters for purposes of comparison.
  alternate: String
  # Optional. Field that determines up to which characters are considered ignorable when alternate: "shifted". Has no effect if alternate: "non-ignorable"
  maxVariable: String
  # Optional. Flag that determines whether strings with diacritics sort from back of the string, such as with some French dictionary ordering.
  backwards: Boolean
  # Optional. Flag that determines whether to check if text require normalization and to perform normalization.
  normalization: Boolean
}

# Mongodb input Distance
input InputMongodbNear {
  # Specifies a geometry in GeoJSON format to geospatial query operators.
  geometry: InputMongodbGeometry
  # Maximum distance in meters
  maxDistance: Int
  # Minimum distance in meters
  minDistance: Int
}

# Mongodb input Filter
input InputMongodbQuery {
  # Matches arrays that contain all elements specified in the query.
  ALL: [String]
  # Matches numeric or binary values in which a set of bit positions all have a value of 0.
  BITS_ALL_CLEAR: JSON
  # Matches numeric or binary values in which a set of bit positions all have a value of 1.
  BITS_ALL_SET: JSON
  #	Matches numeric or binary values in which any bit from a set of bit positions has a value of 0.
  BITS_ANY_CLEAR: JSON
  # Matches numeric or binary values in which any bit from a set of bit positions has a value of 1.
  BITS_ANY_SET: JSON
  # Comments - Adds a comment to a query predicate.
  COMMENT: String
  #Selects documents if element in the array field matches all the specified $elemMatch conditions.
  ELEMNET_MATCH: JSON
  # Matches values that are equal to a specified value.
  EQ: JSON
  # Allows use of aggregation expressions within the query language.
  EXPR: JSON
  # Matches documents that have the specified field.
  EXISTS: Boolean
  # Selects geometries that intersect with a GeoJSON geometry. The 2dsphere index supports GEO_INTERSECTS.
  GEO_INTERSECTS: InputMongodbGeometry
  # Selects geometries within a bounding GeoJSON geometry. The 2dsphere index supports GEO_WITHIN.
  GEO_WITHIN: InputMongodbGeometry
  # Matches values that are equal to a specified value.
  GT: Int
  # Matches values that are greater than or equal to a specified value.
  GTE: Int
  # Matches any of the values specified in an array.
  IN: [JSON]
  # Validate documents against the given JSON Schema.
  JSON_SCHEMA: InputMongodbJsonSchema
  # Matches values that are less than a specified value.
  LT: Int
  # Matches values that are less than a specified value.
  LTE: Int
  # 	Performs a modulo operation on the value of a field and selects documents with a specified result.
  MOD: [Int]
  # Matches values that are less than a specified value.
  NE: JSON
  #Returns geospatial objects in proximity to a point. Requires a geospatial index. The 2dsphere and 2d indexes support NEAR.
  NEAR: InputMongodbNear
  # Returns geospatial objects in proximity to a point on a sphere. Requires a geospatial index. The 2dsphere and 2d indexes support NEAR_SPHERE.
  NEAR_SPHERE: InputMongodbNear
  # Matches none of the values specified in an array.
  NIN: [JSON]
  # Selects documents if a field is of the specified type.
  REGEX: RegExp
  # Used with REGEX to provide options
  REGEX_OPTIONS: String
  # Selects documents if the array field is a specified size.
  SIZE: Int
  # Performs text search on the content of the fields indexed with a text index.
  TEXT: InputMongodbText
  # Selects documents if a field is of the specified type.
  TYPE: JSON
}

# Mongodb input Geometry
input InputMongodbGeometry {
  # GeoJSON object type.
  type: String
  # Array(s) of coordinates. First value's longitude between -180 and 180. Second value's latitude between -90 and 90.
  coordinates: JSON
  #
  crs: InputMongodbGeometryCrs
}

# Mongodb input Geometry Coordinate Reference System
input InputMongodbGeometryCrs {
  type: String
  properties: InputMongodbGeometryCrsProperties
}

# Mongodb input Geometry Coordinate Reference System Properties
input InputMongodbGeometryCrsProperties {
  name: String
}

# MongoDB Input Json Schema
input InputMongodbJsonSchema {
  # String alias or array of string aliases.	Accepts same string aliases used for the type operator
  bsonType: JSON
  # Array of values. Enumerates all possible values of the field
  enum: JSON
  # String or array of unique strings. Enumerates the possible JSON types of the field. Available types are “object”, “array”, “number”, “boolean”, “string”, and “null”.
  type: JSON
  # Array of JSON Schema objects. Field must match all specified schemas.
  allOf: JSON
  # Array of JSON Schema objects. Field must match at least one of the specified schemas.
  anyOf: JSON
  # Array of JSON Schema objects.objects	Field must match exactly one of the specified schemas.
  oneOf: JSON
  # A JSON Schema object. Field must not match the schema.
  not: JSON
  # Field must be a multiple of this value
  multipleOf: Int
  # 	Indicates the maximum value of the field
  maximum: Int
  # If true and field is a number, maximum is an exclusive maximum. Otherwise, it is an inclusive maximum.
  exclusiveMaximum: Boolean
  # Indicates the minimum value of the field.
  minimum: Int
  # If true, minimum is an exclusive minimum. Otherwise, it is an inclusive minimum.
  exclusiveMinimum: Boolean
  # Indicates the maximum length of the field.
  maxLength: Int
  # Indicates the minimum length of the field.
  minLength: Int
  # String containing a regex. Field must match the regular expression.
  pattern: String
  # Indicates the field’s maximum number of properties.
  maxProperties: JSON
  # Indicates the field’s minimum number of properties.
  minProperties: JSON
  # Array of unique strings.	Object’s property set must contain all the specified elements in the array
  required: [String]
  # Boolean or object. If true, additional fields are allowed. If false, they are not. If a valid JSON Schema object is specified, additional fields must validate against the schema. If true, additional fields are allowed. If false, they are not. If a valid JSON Schema object is specified, additional fields must validate against the schema. Defaults to true.
  additionalProperties: JSON
  # A valid JSON Schema where each value is also a valid JSON Schema object.
  properties: JSON
  # In addition to properties requirements, each property name of this object must be a valid regular expression
  patternProperties: JSON
  # Describes field or schema dependencies
  dependencies: JSON
  #	Boolean or object. If an object, must be a valid JSON Schema.
  additionalItems: JSON
  #	Must be either a valid JSON Schema, or an array of valid JSON Schemas.
  items: JSON
  # Indicates the maximum length of array
  maxItems: Int
  # Indicates the minimum length of array
  mintems: Int
  # If true, each item in the array must be unique. Otherwise, no uniqueness constraint is enforced..
  uniqueItems: Boolean
  # A descriptive title string with no effect.
  title: String
  #	A string that describes the schema and has no effect.
  description: String
}

#MongoDB update Options
input InputMongodbText {
  # A string of terms that MongoDB parses and uses to query the text index
  search: String
  # The language that determines the list of stop words for the search and the rules for the stemmer and tokenizer. If not specified, the search uses the default language of the index.
  language: String
  #  boolean flag to enable or disable case sensitive search.
  caseSensitive: Boolean
  # A boolean flag to enable or disable diacritic sensitive search against text indexes. Defaults to false;
  diacriticSensitive: Boolean
}
`
}
