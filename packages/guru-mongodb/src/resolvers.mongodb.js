import camelCase from 'lodash/fp/camelCase'
import gql from 'graphql-tag'

import {
  aggregateQuery,
  countQuery,
  distinctQuery,
  findByIdQuery,
  findOneByIdQuery,
  findOneQuery,
  findQuery,
  resolveQuery,
} from './queries'
import {
  insertManyMutation,
  insertOneMutation,
  dropMutation,
  findAndCopyToMutation,
  deleteManyMutation,
  deleteOneMutation,
  updateManyByIdMutation,
  updateManyMutation,
  updateOneMutation,
} from './mutations'
import {
  getObjectDirectiveArguments,
  notObjectTypeDef,
  typeEngine,
} from 'guru-utils'
import { mongodbDatabaseResolver } from './database'

export function mongoDbResolvers({ context, typeDefs, namespace }) {
  const types = gql`
    ${typeDefs}
  `

  const { Query, Mutation } = mongodbDatabaseResolver(namespace)

  return types.definitions.reduce(
    (previous, current) => {
      if (notObjectTypeDef(current)) return previous

      const { engine } = getObjectDirectiveArguments(current)

      const dbEngine = typeEngine({
        engine,
        namespace,
        type: 'resolver',
        name: 'MONGODB',
      })
      if (!dbEngine) return previous

      const name = camelCase(current.name.value)

      const args = {
        context,
        namespace,
        moduleName: name,
        collection: dbEngine.value.collection,
      }

      return {
        ...previous,

        Mutation: {
          ...previous.Mutation,
          [`${name}Drop`]: dropMutation(args),
          [`${name}FindAndCopyTo`]: findAndCopyToMutation(args),
          [`${name}InsertMany`]: insertManyMutation(args),
          [`${name}InsertOne`]: insertOneMutation(args),
          [`${name}DeleteMany`]: deleteOneMutation(args),
          [`${name}DeleteOne`]: deleteOneMutation(args),
          [`${name}UpdateManyById`]: updateManyByIdMutation(args),
          [`${name}UpdateMany`]: updateManyMutation(args),
          [`${name}UpdateOne`]: updateOneMutation(args),
        },

        Query: {
          ...previous.Query,
          [`${name}Aggregate`]: aggregateQuery(args),
          [`${name}Count`]: countQuery(args),
          [`${name}Distinct`]: distinctQuery(args),
          [`${name}FindById`]: findByIdQuery(args),
          [`${name}FindOneById`]: findOneByIdQuery(args),
          [`${name}FindOne`]: findOneQuery(args),
          [`${name}Find`]: findQuery(args),
          [`${name}Resolve`]: resolveQuery(args),
        },
      }
    },
    { Query, Mutation }
  )
}
