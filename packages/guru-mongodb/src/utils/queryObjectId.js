import { ObjectId } from 'mongodb'
import isEmpty from 'lodash/fp/isEmpty'

export function queryObjectId(args = {}) {
  if (isEmpty(args)) return {}

  let obj = Object.keys(args).reduce((previous, key) => {
    if (args._id && args._id['$in']) {
      return {
        ...previous,
        _id: { $in: args._id['$in' || 'IN'].map(item => ObjectId(item)) },
      }
    }

    if (Array.isArray(args._id)) {
      return { ...previous, _id: { $in: args._id.map(item => ObjectId(item)) } }
    }

    if (Array.isArray(args[key])) {
      return { ...previous, [key]: args[key].map(item => queryObjectId(item)) }
    }

    let tmpArg =
      key === '_id' ? { _id: ObjectId(args[key]) } : { [key]: args[key] }

    return {
      ...previous,
      ...tmpArg,
    }
  }, {})

  return obj
}
