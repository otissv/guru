import camelCase from 'lodash/fp/camelCase'

export function gqlQueryToMongoQuery(query) {
  function transformObj(obj) {
    return Object.keys(obj).reduce((previous, key) => {
      let _key = key === key.toUpperCase() ? `$${camelCase(key)}` : key

      return {
        ...previous,
        [_key]: typeof obj[key] === 'object' ? gqlToMongo(obj[key]) : obj[key],
      }
    }, {})
  }

  return Array.isArray(query)
    ? query.map(obj => transformObj(obj))
    : transformObj(query)
}
