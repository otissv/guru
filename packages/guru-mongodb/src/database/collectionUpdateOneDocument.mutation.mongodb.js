import {
  databaseConfigurationError,
  errorLogger,
  noInputError,
  resolverMutationError,
} from 'guru-utils'
import { ApolloError } from 'apollo-server'

import { queryObjectId } from '../utils'
import { ObjectId } from 'mongodb'

export function collectionUpdateOneDocumentMutation({ namespace, moduleName }) {
  const resolverName = `${moduleName}CollectionUpdateOneDocument`

  return async function collectionUpdateOneDocumentResolver(
    obj,
    args,
    context,
    info
  ) {
    try {
      // Error Checking
      if (!args.collection || !args.data) {
        throw new ApolloError(...noInputError({ resolverName, args: args }))
      }

      const mongodb = context[namespace].client()
      const db = await mongodb

      const data = queryObjectId(args.data)
      const query = queryObjectId(args.query || {})

      const response = await db
        .collection(args.collection)
        .updateOne(query, { $set: data }, args.options)
        .then(({ result }) => {
          return {
            _id: args.collection,
            module: resolverName,
            result: result.n ? 'ok' : 'failed',
            n: result.n,
          }
        })
        .catch(error => {
          throw error
        })

      return { RESULTS: [response] }
    } catch (error) {
      errorLogger(error)

      return error.extensions
        ? error
        : new ApolloError(
            ...resolverMutationError({
              error,
              namespace,
              resolverName,
            })
          )
    }
  }
}
