import {
  databaseConfigurationError,
  errorLogger,
  // meta,
  getInfoSelection,
  noInputError,
  resolverMutationError,
} from 'guru-utils'

import { ApolloError } from 'apollo-server'

export function collectionInsertOneDocumentMutation({ namespace, moduleName }) {
  const resolverName = `${moduleName}CollectionInsertOneDocument`

  return async function collectionInsertOneDocumentResolver(
    obj,
    args,
    context,
    info
  ) {
    try {
      // Error Checking
      if (!args.data && !args.collection) {
        throw new ApolloError(...noInputError({ resolverName, args: args }))
      }

      const selection = getInfoSelection(info)

      const mongodb = context[namespace].client()
      const db = await mongodb
      const dbCollection = db.collection(args.collection)
      const response = await dbCollection.insertOne(
        {
          ...args.data,
          // meta: meta({ userId: context.userId, create: true })
        },
        args.options
      )

      if (response.error) {
        throw error
      }

      return {
        data: [
          {
            ...args.data,
            _id: response.ops[0]._id,
          },
        ],
        RESULTS: [
          {
            _id: response.ops[0]._id,
            operation: resolverName,
            result: response.result.n > 0 ? 'ok' : 'failed',
            n: response.result.n,
          },
        ],
      }
    } catch (error) {
      errorLogger(error)

      return error.extensions
        ? error
        : new ApolloError(
            ...resolverMutationError({
              error,
              namespace,
              resolverName,
            })
          )
    }
  }
}
