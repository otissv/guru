import { ApolloError } from 'apollo-server'
import { resolverQueryError } from 'guru-utils'
import camelCase from 'lodash/camelCase'

export function engineCollectionResolve({ namespace }) {
  const resolverName = `${namespace}EngineCollectionResolve`

  return async function engineCollectionResolveResolver(
    parent,
    args,
    context,
    info
  ) {
    try {
      if (!parent.name) {
        throw new ApolloError(...noInputError({ resolverName, args: args }))
      }

      const queryName = camelCase(parent.name)

      return await context.resolvers.Query[`${queryName}CollectionFind`](
        parent,
        args,
        context,
        info
      )
    } catch (error) {
      console.error(error)
      return new ApolloError(
        resolverQueryError({
          error,
          namespace,
          resolverName: 'engineCollectionResolve',
        })
      )
    }
  }
}
