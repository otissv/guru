import {
  databaseConfigurationError,
  errorLogger,
  noInputError,
  resolverQueryError,
} from 'guru-utils'

import { ApolloError } from 'apollo-server'

export function indexFindByCollectionQuery({ namespace, moduleName }) {
  const resolverName = `${moduleName}IndexFindByCollection`

  return async function indexFindByCollectionResolver(
    obj,
    args,
    context,
    info
  ) {
    try {
      // Error Checking
      if (!context || !context[namespace]) {
        throw new ApolloError(
          ...databaseConfigurationError({ namespace, resolverName })
        )
      }

      if (Object.keys(args).length > 0 && !args.options && !args.name) {
        throw new ApolloError(...noInputError({ resolverName, args: args }))
      }

      const mongodb = context[namespace].client()
      const db = await mongodb
      return await db
        .collection(args.name)
        .indexes()
        .then(indexes => {
          return indexes.map(index => ({
            key: JSON.stringify(index.key),
            name: index.name,
          }))
        })
    } catch (error) {
      errorLogger(error)

      return error.extensions
        ? error
        : new ApolloError(
            ...resolverQueryError({
              error,
              namespace,
              resolverName,
            })
          )
    }
  }
}
