import {
  databaseConfigurationError,
  errorLogger,
  noInputError,
  resolverQueryError,
} from 'guru-utils'

import { ApolloError } from 'apollo-server'

export function engineStatsQuery({ namespace, moduleName }) {
  const resolverName = `${moduleName}EngineStats`

  return async function engineStatsResolver(obj, args, context, info) {
    try {
      // Error Checking
      if (!context || !context[namespace]) {
        throw new ApolloError(
          ...databaseConfigurationError({ namespace, resolverName })
        )
      }

      if (Object.keys(args).length > 0 && !args.options) {
        throw new ApolloError(...noInputError({ resolverName, args: args }))
      }

      const mongodb = context[namespace].client()
      const db = await mongodb

      return await db.stats(args.scale)
    } catch (error) {
      errorLogger(error)

      return error.extensions
        ? error
        : new ApolloError(
            ...resolverQueryError({
              error,
              namespace,
              resolverName,
            })
          )
    }
  }
}
