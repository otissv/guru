import {
  collectionNameError,
  databaseConfigurationError,
  errorLogger,
  // meta,
  noInputError,
  resolverMutationError,
} from 'guru-utils'

import { ApolloError } from 'apollo-server'

export function collectionCreateMutation({ namespace, moduleName }) {
  const resolverName = `${moduleName}collectionCreate`

  return async function collectionCreateResolver(
    obj,
    args,
    context = {},
    info
  ) {
    try {
      // Error Checking
      if (!args.name) {
        throw new ApolloError(...noInputError({ resolverName, args: args }))
      }

      const mongodb = context[namespace].client()
      const db = await mongodb

      const response = await db.createCollection(args.name, args.options)
      return {
        RESULTS: [
          {
            _id: args.name,
            module: resolverName,
            result: response ? 'ok' : 'failed',
            n: response ? 1 : 0,
          },
        ],
      }
    } catch (error) {
      errorLogger(error)

      return error.extensions
        ? error
        : new ApolloError(
            ...resolverMutationError({
              error,
              namespace,
              resolverName,
            })
          )
    }
  }
}
