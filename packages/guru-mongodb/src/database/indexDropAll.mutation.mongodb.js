import { errors } from 'guru-utils'

export function indexDropAllMutation({ namespace, moduleName }) {
  const resolverName = `${moduleName}IndexDropAll`

  return function indexDropAllResolver(obj, args, context, info) {
    // Error Checking
    if (!context || !context[namespace]) {
      throw errors.databaseConfiguration({ namespace, resolverName })
    }

    if (Object.keys(args).length > 0 && !args.options && !args.query) {
      throw errors.noInput({ resolverName, args: args })
    }

    const { name, index } = args
    const indexStr = `${index.key}_${index.value}`

    const mongodb = context[namespace].client()
    return mongodb.then(db =>
      db
        .collection(name)
        .dropIndexes(indexStr)
        .catch(error =>
          errors.resolverMutation({ error, namespace, resolverName })
        )
    )
  }
}
