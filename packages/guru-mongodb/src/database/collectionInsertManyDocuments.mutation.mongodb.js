import {
  databaseConfigurationError,
  errorLogger,
  // meta,
  noInputError,
  resolverMutationError,
} from 'guru-utils'

import { ApolloError } from 'apollo-server'

export function collectionInsertManyDocumentsMutation({
  namespace,
  moduleName,
}) {
  const resolverName = `${moduleName}CollectionInsertManyDocuments`

  return async function collectionInsertManyDocumentResolver(
    obj,
    args,
    context,
    info
  ) {
    try {
      // Error Checking

      if (!args.data && !args.collection) {
        throw new ApolloError(...noInputError({ resolverName, args: args }))
      }

      const mongodb = context[namespace].client()
      const db = await mongodb
      const response = await db.collection(args.collection).insertMany([
        ...args.data,
        // meta: meta({ userId: context.userId, create: true }),
      ])

      if (response.error) {
        throw new ApolloError(
          ...resolverMutationError({
            error: response.error,
            namespace,
            resolverName,
          })
        )
      }

      return {
        data: args.data,
      }
    } catch (error) {
      errorLogger(error)

      return error.extensions
        ? error
        : new ApolloError(
            ...resolverMutationError({
              error,
              namespace,
              resolverName,
            })
          )
    }
  }
}
