import { errors } from 'guru-utils'

export function indexCreateMutation({ namespace, moduleName }) {
  const resolverName = `${moduleName}IndexCreate`

  return function indexCreateMutationResolver(obj, args, context, info) {
    // Error Checking
    if (!context || !context[namespace]) {
      throw errors.databaseConfiguration({ namespace, resolverName })
    }

    if (Object.keys(args).length > 0 && !args.options && !args.query) {
      throw errors.noInput({ resolverName, args: args })
    }

    const { name, index, options } = args
    const indexObj = {
      [index.key]: parseInt(index.value, 10),
    }

    const mongodb = context[namespace].client()
    return mongodb.then(async db => {
      try {
        const indexes = await db
          .collection(name, options)
          .createIndex(indexObj)
          .then(result => {
            const resultSplit = result.split('_')

            return {
              name: resultSplit[0],
              value: resultSplit[1],
            }
          })

        return indexes
      } catch (error) {
        return errors.resolverMutation({ error, namespace, resolverName })
      }
    })
  }
}
