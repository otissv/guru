import {
  databaseConfigurationError,
  errorLogger,
  getInfoSelection,
  getProjectionFromInfoSelection,
  getValueFromInfoSelection,
  noInputError,
  resolverQueryError,
} from 'guru-utils'

import { queryObjectId } from '../utils/queryObjectId'

import { ApolloError } from 'apollo-server'

export function collectionFindQuery({ namespace, moduleName }) {
  const resolverName = `${moduleName}CollectionFind`

  return async function collectionFindResolver(parent, args, context, info) {
    try {
      // Error Checking
      if (!context || !context[namespace]) {
        throw new ApolloError(
          ...databaseConfigurationError({ namespace, resolverName })
        )
      }

      const selection = getInfoSelection(info)
      const collectionName = args.name || selection.arguments.name
      const projection = getProjectionFromInfoSelection(selection)
      const query = getValueFromInfoSelection(selection, 'query')
      const collections = selection.arguments.collections

      const stringify = obj => JSON.stringify(obj, null, 2)

      const mongodb = context[namespace].client()
      const db = await mongodb

      const fndAllCollection = async () => {
        const result = await db.collections()

        return Promise.all(
          result.map(async collection => {
            try {
              const name = collection.s.name
              const dbCollection = db.collection(name)

              const indexes = !selection.fields.indexes
                ? null
                : await dbCollection.indexes().then(indexes => {
                    return indexes.map(index => index.key)
                  })

              const count = !selection.fields.count
                ? null
                : await dbCollection.countDocuments({})

              const data = !selection.fields.data
                ? null
                : await new Promise(resolve => {
                    return dbCollection
                      .find(queryObjectId(query))
                      .project(projection)
                      .toArray((error, docs) => {
                        if (error) {
                          throw resolverQueryError({
                            error,
                            namespace,
                            resolverName,
                          })
                        }
                        console.log(response)

                        const response = docs.map(({ _id, ...doc }) => {
                          return { ...doc, _id: _id.toString() }
                        })

                        resolve(response)
                      })
                  }).catch(error => {
                    throw error
                  })

              return {
                engine: namespace,
                name,
                ...(indexes ? { indexes } : {}),
                ...(count ? { count } : {}),
                ...(data ? { data } : {}),
              }
            } catch (error) {
              throw error
            }
          })
        )
      }

      const findCollectionByName = async collectionName => {
        return new Promise(async (resolve, reject) => {
          try {
            db.collection(collectionName)
              .find(query)
              .project(projection)
              .toArray(async (error, docs) => {
                if (error) {
                  throw error
                }

                const indexes = await db
                  .collection(collectionName)
                  .indexes()
                  .then(indexes => {
                    return indexes.map(index => index.key)
                  })

                const count = await db
                  .collection(collectionName)
                  .countDocuments({})

                resolve([
                  {
                    engine: namespace,
                    name: collectionName,
                    indexes,
                    data: docs,
                    count,
                  },
                ])
              })
          } catch (error) {
            throw new ApolloError(
              ...resolverQueryError({
                error,
                namespace,
                resolverName,
              })
            )
          }
        })
      }

      if (collections) {
        const result = await Promise.all(
          collections.map(
            async collection => await findCollectionByName(collection)
          )
        )

        return result[0]
      } else {
        return collectionName
          ? findCollectionByName(collectionName)
          : fndAllCollection()
      }
    } catch (error) {
      console.error(error)
      errorLogger(error)

      return error.extensions
        ? error
        : new ApolloError(
            ...resolverQueryError({
              error,
              namespace,
              resolverName,
            })
          )
    }
  }
}
