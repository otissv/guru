import {
  collectionNameError,
  databaseConfigurationError,
  errorLogger,
  noInputError,
  resolverMutationError,
} from 'guru-utils'

import { ApolloError } from 'apollo-server'

export function collectionRenameMutation({ namespace, moduleName }) {
  const resolverName = `${moduleName}CollectionRename`

  return async function collectionRenameResolver(
    obj,
    args,
    context = {},
    info
  ) {
    try {
      // Error Checking
      if (!args.name || !args.target) {
        throw new ApolloError(...noInputError({ resolverName, args: args }))
      }

      const mongodb = context[namespace].client()
      const db = await mongodb

      const response = await db
        .collection(args.name)
        .rename(args.target, args.dropTarget)

      return {
        RESULTS: [
          {
            _id: args.target,
            module: resolverName,
            result: response ? 'ok' : 'failed',
            n: response ? 1 : 0,
          },
        ],
      }
    } catch (error) {
      errorLogger(error)

      return error.extensions
        ? error
        : new ApolloError(
            ...resolverMutationError({
              error,
              namespace,
              resolverName,
            })
          )
    }
  }
}
