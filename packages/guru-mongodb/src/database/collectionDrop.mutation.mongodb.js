import {
  // databaseConfigurationError,
  errorLogger,
  noInputError,
  resolverMutationError,
} from 'guru-utils'

import { ApolloError } from 'apollo-server'

export function collectionDropMutation({ namespace, moduleName }) {
  const resolverName = `${moduleName}CollectionDrop`

  return async function collectionDropResolver(obj, args, context = {}, info) {
    try {
      // Error Checking
      if (!args.name) {
        throw new ApolloError(...noInputError({ resolverName, args: args }))
      }

      const mongodb = context[namespace].client()
      const db = await mongodb
      const response = await db.collection(args.name).drop()

      return {
        RESULTS: [
          {
            _id: args.name,
            module: resolverName,
            result: response ? 'ok' : 'failed',
            n: response ? 1 : 0,
          },
        ],
      }
    } catch (error) {
      errorLogger(error)

      return error.extensions
        ? error
        : new ApolloError(
            ...resolverMutationError({
              error,
              namespace,
              resolverName,
            })
          )
    }
  }
}
