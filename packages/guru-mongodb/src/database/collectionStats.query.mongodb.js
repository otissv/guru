import {
  databaseConfigurationError,
  errorLogger,
  noInputError,
  resolverQueryError,
} from 'guru-utils'

import { ApolloError } from 'apollo-server'

export function collectionStatsQuery({ namespace, moduleName }) {
  const resolverName = `${namespace}CollectionStats`

  return async function collectionStatsResolver(obj, args, context, info) {
    try {
      // Error Checking
      if (!context || !context[namespace]) {
        throw new ApolloError(
          ...databaseConfigurationError({ namespace, resolverName })
        )
      }
      if (Object.keys(args).length == 0 || !args.name) {
        throw new ApolloError(...noInputError({ resolverName, args: args }))
      }

      const mongodb = context[namespace].client()
      const db = await mongodb

      const results = await db.collection(args.name).stats(args.scale)

      return results
    } catch (error) {
      errorLogger(error)

      return error.extensions
        ? error
        : new ApolloError(
            ...resolverQueryError({
              error,
              namespace,
              resolverName,
            })
          )
    }
  }
}
