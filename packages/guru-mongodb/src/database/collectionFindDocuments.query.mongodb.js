import { ApolloError } from 'apollo-server'
import {
  databaseConfigurationError,
  errorLogger,
  getInfoSelection,
  getProjectionFromInfoSelection,
  getValueFromInfoSelection,
  noInputError,
  resolverQueryError,
} from 'guru-utils'
import { queryObjectId } from '../utils/queryObjectId'

export function collectionFindDocumentsQuery({ namespace, moduleName }) {
  const resolverName = `${moduleName}CollectionFindDocuments`

  return async function collectionFindDocumentsResolver(
    obj,
    args,
    context,
    info
  ) {
    try {
      // Error Checking
      if (!context || !context[namespace]) {
        throw new ApolloError(
          ...databaseConfigurationError({ namespace, resolverName })
        )
      }

      if (Object.keys(args).length > 0 && !args.collection) {
        throw new ApolloError(...noInputError({ resolverName, args: args }))
      }

      const selection = getInfoSelection(info)
      const projection = getProjectionFromInfoSelection(selection)
      const query = getValueFromInfoSelection(selection, 'query')

      const mongodb = context[namespace].client()
      const db = await mongodb

      return new Promise((resolve, reject) =>
        db
          .collection(args.collection)
          .find(queryObjectId(query))
          .project(projection)
          .toArray((error, docs) => {
            if (error) {
              throw error
            } else {
              resolve({
                engine: namespace,
                collection: args.collection,
                data: docs,
              })
            }
          })
      )
    } catch (error) {
      errorLogger(error)

      return error.extensions
        ? error
        : new ApolloError(
            ...resolverQueryError({
              error,
              namespace,
              resolverName,
            })
          )
    }
  }
}
