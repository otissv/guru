import { errors } from 'guru-utils'

export function collectionRemoveDocumentMutation({ namespace, moduleName }) {
  const resolverName = `${moduleName}CollectionRemoveDocument`

  return function collectionRemoveDocumentResolver(obj, args, context, info) {
    // Error Checking
    if (!context || !context[namespace]) {
      throw errors.databaseConfiguration({ namespace, resolverName })
    }

    if (Object.keys(args).length > 0 && !args.options && !args.query) {
      throw errors.noInput({ resolverName, args: args })
    }

    const { name, _id } = args

    const mongodb = context[namespace].client()
    return mongodb.then(db => {
      return db
        .collection(name)
        .remove({ _id: ObjectId(_id) })
        .then(response => {
          return {
            _id,
            result: response.result.n > 0 ? 'ok' : 'failed',
            removed: response.result.n,
          }
        })
        .catch(error =>
          errors.resolverMutation({ error, namespace, resolverName })
        )
    })
  }
}
