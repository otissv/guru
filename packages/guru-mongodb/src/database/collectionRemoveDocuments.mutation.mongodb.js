import {
  databaseConfigurationError,
  errorLogger,
  noInputError,
  resolverMutationError,
} from 'guru-utils'
import { ApolloError } from 'apollo-server'

import { queryObjectId } from '../utils'
import { ObjectId } from 'mongodb'

export function collectionRemoveDocumentsMutation({ namespace, moduleName }) {
  const resolverName = `${moduleName}CollectionRemoveDocuments`

  return async function collectionRemoveDocumentsResolver(
    obj,
    args,
    context,
    info
  ) {
    try {
      // Error Checking

      if (
        !args.collection ||
        !Array.isArray(args._ids) ||
        args._ids.length === 0
      ) {
        throw new ApolloError(...noInputError({ resolverName, args: args }))
      }

      const mongodb = context[namespace].client()
      const db = await mongodb

      const response = await db
        .collection(args.collection)
        .deleteMany({ _id: { $in: args._ids.map(id => ObjectId(id)) } })
        .then(({ result }) => {
          return {
            _id: args.collection,
            module: resolverName,
            result: result.n ? 'ok' : 'failed',
            n: result.n,
          }
        })
        .catch(error => {
          throw error
        })

      return { RESULTS: [response] }
    } catch (error) {
      errorLogger(error)

      return error.extensions
        ? error
        : new ApolloError(
            ...resolverMutationError({
              error,
              namespace,
              resolverName,
            })
          )
    }
  }
}
