import {
  // databaseConfigurationError,
  errorLogger,
  noInputError,
  resolverMutationError,
} from 'guru-utils'
import { ApolloError } from 'apollo-server'

export function collectionDropManyMutation({ namespace, moduleName }) {
  const resolverName = `${moduleName}CollectionDropMany`

  return async function collectionDropManyResolver(
    obj,
    args,
    context = {},
    info
  ) {
    try {
      console.log(args)
      // Error Checking
      if (!args.names || !args.names.length === 0) {
        throw new ApolloError(...noInputError({ resolverName, args: args }))
      }

      const mongodb = context[namespace].client()
      const db = await mongodb

      const dropCollectionResults = async name =>
        await db
          .collection(name)
          .drop()
          .then(result => ({
            _id: name,
            module: resolverName,
            result: 'ok',
            n: 1,
          }))
          .catch(error => ({
            name,
            _id: name,
            module: resolverName,
            result: 'failed',
            n: 0,
          }))

      const dropCollections = () =>
        args.names.map(async name => await dropCollectionResults(name))

      const response = await Promise.all(await dropCollections())

      return { RESULTS: response }
    } catch (error) {
      errorLogger(error)

      return error.extensions
        ? error
        : new ApolloError(
            ...resolverMutationError({
              error,
              namespace,
              resolverName,
            })
          )
    }
  }
}
