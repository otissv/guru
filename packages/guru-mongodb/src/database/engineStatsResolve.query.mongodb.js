import camelCase from 'lodash/camelCase'
import { ApolloError } from 'apollo-server'
import { noInputError, resolverQueryError } from 'guru-utils'

export function engineStatsResolve({ namespace }) {
  const resolverName = `${namespace}EngineStatsResolve`

  return async function engineStatsResolveResolver(
    parent,
    args,
    context,
    info
  ) {
    try {
      if (!parent.name) {
        throw new ApolloError(...noInputError({ resolverName, args: args }))
      }

      const queryName = camelCase(parent.name)

      return await context.resolvers.Query[`${queryName}EngineStats`](
        null,
        {},
        context,
        info
      )
    } catch (error) {
      console.error(error)
      return new ApolloError(
        resolverQueryError({
          error,
          namespace,
          resolverName: 'engineStatsResolve',
        })
      )
    }
  }
}
