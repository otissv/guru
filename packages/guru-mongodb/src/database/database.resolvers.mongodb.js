import camelCase from 'lodash/camelCase'
import upperFirst from 'lodash/upperFirst'

import { collectionCreateMutation } from './collectionCreate.mutation.mongodb'
import { collectionDropManyMutation } from './collectionDropMany.mutation.mongodb'
import { collectionDropMutation } from './collectionDrop.mutation.mongodb'
import { collectionFindDocumentsQuery } from './collectionFindDocuments.query.mongodb'
import { collectionFindQuery } from './collectionFind.query.mongodb'
import { collectionInsertManyDocumentsMutation } from './collectionInsertManyDocuments.mutation.mongodb'
import { collectionInsertOneDocumentMutation } from './collectionInsertOneDocument.mutation.mongodb'
import { collectionRemoveDocumentMutation } from './collectionRemoveDocument.mutation.mongodb'
import { collectionRemoveDocumentsMutation } from './collectionRemoveDocuments.mutation.mongodb'
import { collectionRenameMutation } from './collectionRename.mutation.mongodb'
import { collectionStatsQuery } from './collectionStats.query.mongodb'
import { collectionUpdateOneDocumentMutation } from './collectionUpdateOneDocument.mutation.mongodb'
import { engineCollectionResolve } from './engineCollectionResolve.query.mongodb'
import { engineStatsQuery } from './engineStats.query.mongodb'
import { engineStatsResolve } from './engineStatsResolve.query.mongodb'
import { indexCreateMutation } from './indexCreate.mutation.mongodb'
import { indexDropAllMutation } from './indexDropAll.mutation.mongodb'
import { indexDropMutation } from './indexDrop.mutation.mongodb'
import { indexFindByCollectionQuery } from './indexFindByCollection.query.mongodb'

export function mongodbDatabaseResolver(namespace) {
  const moduleName = camelCase(namespace)
  const args = {
    namespace,
    moduleName,
  }

  return {
    Mutation: {
      [`${moduleName}CollectionCreate`]: collectionCreateMutation(args),
      [`${moduleName}CollectionDrop`]: collectionDropMutation(args),
      [`${moduleName}CollectionDropMany`]: collectionDropManyMutation(args),
      [`${moduleName}CollectionInsertOneDocument`]: collectionInsertOneDocumentMutation(
        args
      ),
      [`${moduleName}CollectionInsertManyDocuments`]: collectionInsertManyDocumentsMutation(
        args
      ),
      [`${moduleName}CollectionRemoveDocument`]: collectionRemoveDocumentMutation(
        args
      ),
      [`${moduleName}CollectionRemoveDocuments`]: collectionRemoveDocumentsMutation(
        args
      ),
      [`${moduleName}CollectionRename`]: collectionRenameMutation(args),
      [`${moduleName}CollectionUpdateOneDocument`]: collectionUpdateOneDocumentMutation(
        args
      ),
      [`${moduleName}IndexCreate`]: indexCreateMutation(args),
      [`${moduleName}IndexDropAll`]: indexDropAllMutation(args),
      [`${moduleName}IndexDrop`]: indexDropMutation(args),
    },

    Query: {
      [`${moduleName}CollectionFindDocuments`]: collectionFindDocumentsQuery(
        args
      ),
      [`${moduleName}CollectionFind`]: collectionFindQuery(args),
      [`${moduleName}CollectionStats`]: collectionStatsQuery(args),
      [`${moduleName}EngineStats`]: engineStatsQuery(args),
      // [`${moduleName}EngineCollection`]: engineCollectionQuery(args),
      [`${moduleName}IndexFindByCollection`]: indexFindByCollectionQuery(args),
      engineStatsResolve: engineStatsResolve(args),
      engineCollectionResolve: engineCollectionResolve(args),
    },
  }
}
