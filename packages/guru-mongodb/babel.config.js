const babelConfig = require('../../babel.config')

module.exports = {
  presets: [...babelConfig.presets],
  plugins: [...babelConfig.plugins],
}
