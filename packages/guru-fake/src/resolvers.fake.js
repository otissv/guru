import { fakeResolveQuery } from './fake/resolve'
import { fakeResolverQuery } from './fake/fake/resolver.fake'
import { addressResolverQuery } from './fake/address/resolver.address'
import { cardResolverQuery } from './fake/card/resolver.card'
import { cardTransactionResolverQuery } from './fake/custom/cardTransaction/resolver.cardTransaction'
import { colorResolverQuery } from './fake/color/resolver.color'
import { commerceResolverQuery } from './fake/commerce/resolver.commerce'
import { companyResolverQuery } from './fake/company/resolver.company'
import { currencyResolverQuery } from './fake/currency/resolver.currency'
import { databaseResolverQuery } from './fake/database/resolver.database'
import { dateTimeResolverQuery } from './fake/dateTime/resolver.dateTime'
import { financeResolverQuery } from './fake/finance/resolver.finance'
import { hackerResolverQuery } from './fake/hacker/resolver.hacker'
import { imageResolverQuery } from './fake/image/resolver.image'
import { internetResolverQuery } from './fake/internet/resolver.internet'
import { loremResolverQuery } from './fake/lorem/resolver.lorem'
import { meetingResolverQuery } from './fake/custom/meeting/resolver.meeting'
import { messageResolverQuery } from './fake/custom/message/resolver.message'
import { metaResolverQuery } from './fake/custom/meta/resolver.meta'
import { miscResolverQuery } from './fake/misc/resolver.misc'
import { numberResolverQuery } from './fake/number/resolver.number'
import { personResolverQuery } from './fake/person/resolver.person'
import { phoneResolverQuery } from './fake/phone/resolver.phone'
import { systemResolverQuery } from './fake/system/resolver.system'
import { transactionResolverQuery } from './fake/custom/transaction/resolver.transaction'
import { userResolverQuery } from './fake/custom/user/resolver.user'
import { scalarQuery } from './fake/scalar/resolver.scalar'

export const resolvers = {
  Query: {
    ...fakeResolverQuery,
    ...addressResolverQuery,
    ...cardResolverQuery,
    ...cardTransactionResolverQuery,
    ...colorResolverQuery,
    ...commerceResolverQuery,
    ...companyResolverQuery,
    ...currencyResolverQuery,
    ...databaseResolverQuery,
    ...dateTimeResolverQuery,
    ...financeResolverQuery,
    ...hackerResolverQuery,
    ...imageResolverQuery,
    ...internetResolverQuery,
    ...loremResolverQuery,
    ...meetingResolverQuery,
    ...messageResolverQuery,
    ...metaResolverQuery,
    ...miscResolverQuery,
    ...numberResolverQuery,
    ...personResolverQuery,
    ...phoneResolverQuery,
    ...systemResolverQuery,
    ...transactionResolverQuery,
    ...userResolverQuery,
    ...fakeResolveQuery,
    ...scalarQuery,
  },
}
