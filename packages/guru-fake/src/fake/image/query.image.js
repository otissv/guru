import { gql } from '../../helpers.fake'

export const FakeImageQuery = gql`
FakeImage (locale: String): FakeImage
FakeImageList  (limit: Int, locale: String): [FakeImageList]
`
