import faker from 'faker'

import {
  generateListResolver,
  generateResolver,
  getResolveFields,
  queryResolver,
} from '../resolve'

export function imageMock({ parent, args, context, info, selection }) {
  if (args.locale) {
    faker.locale = args.locale
  }

  return {
    avatar: () => faker.image.avatar(),
    dataUri: () => faker.image.dataUri(),
    image: () => faker.image.image(),
    imageAbstract: () => faker.image.abstract(),
    imageAnimals: () => faker.image.animals(),
    imageBusiness: () => faker.image.business(),
    imageCats: () => faker.image.cats(),
    imageCity: () => faker.image.city(),
    imageFashion: () => faker.image.fashion(),
    imageFood: () => faker.image.food(),
    imageNature: () => faker.image.nature(),
    imageNightlife: () => faker.image.nightlife(),
    imagePeople: () => faker.image.people(),
    imageSports: () => faker.image.sports(),
    imageTechnics: () => faker.image.technics(),
    imageTransport: () => faker.image.transport(),
    imageUrl: () => faker.image.imageUrl(),
    ...getResolveFields({ parent, args, context, info, selection }),
  }
}

export const imageResolverQuery = queryResolver({
  resolverName: 'Image',
  mockFn: imageMock,
})
