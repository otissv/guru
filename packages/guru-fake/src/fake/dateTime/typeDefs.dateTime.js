import { gql } from '../../helpers.fake'
import { nested } from '../nested'

export const dateTimeFields = gql`
  #=> "am"
  amPm: String
  #=> 
  between (start: String, end: String): String
  #=> "IV" 
  century: String
  #=> "2001-07-06" (see available formatters http://momentjs.com/docs/#/parsing/string-format/) 
  date: String
  #=> 9
  dayOfMonth: Int
  #=> 4
  dayOfWeek: Int
  #=> 323
  dayOfYear: Int
  #=> "2017-06-17T06:08:08.095Z"
  future: String
  #=> "March"
  month: String
  #=> 5
  monthNumber: Int
  #=> "2016-09-20T18:42:20.204Z"
  past: String
  #=> "2017-03-16T20:38:56.776Z"
  recent: String
  #=> "03:08:02" (see available formatters http://momentjs.com/docs/#/parsing/string-format/) 
  time: String
  #=> "America/Miquelon"
  timezone: String
  #=> 659897901
  unixTime: Int
  #=>  "Wednesday"
  weekday: String
  #=> 1990
  year: String
`

export const FakeDateTimeType = gql`
  type FakeDateTime {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    ${dateTimeFields}
    ${nested}
  }
`

export const FakeDateTimeListType = gql`
  type FakeDateTimeList {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    #=> 1
    idx: Int
    #=> "dateTime_1"
    idxName: String
    #=> "custom_1"
    idxCustom: String
    ${dateTimeFields}
    ${nested}
  }
`
