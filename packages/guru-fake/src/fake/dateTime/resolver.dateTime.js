import faker from 'faker'
import casual from 'casual'
import moment from 'moment'

import {
  generateListResolver,
  generateResolver,
  getResolveFields,
  queryResolver,
} from '../resolve'

export function dateTimeMock({ parent, args, context, info, selection }) {
  if (args.locale) {
    faker.locale = args.locale
  }

  const timeFormat = args.timeFormat || 'HH:mm:ss'
  const dataFormat = args.dataFormat || 'YYYY-MM-DD'

  return {
    amPm: () => casual.am_pm,
    between: () => moment(faker.date.between()).toISOString(),
    century: () => casual.century,
    date: () => casual.date(dataFormat),
    dayOfMonth: () => casual.day_of_month,
    dayOfWeek: () => casual.day_of_week,
    dayOfYear: () => casual.day_of_year,
    future: () => moment(faker.date.future()).toISOString(),
    month: () => faker.date.month(),
    monthNumber: () => casual.month_number,
    past: () => moment(faker.date.past()).toISOString(),
    recent: () => moment(faker.date.recent()).toISOString(),
    time: () => casual.time(timeFormat),
    timezone: () => casual.timezone,
    unixTime: () => casual.unix_time,
    weekday: () => faker.date.weekday(),
    year: () => casual.year,
    ...getResolveFields({ parent, args, context, info, selection }),
  }
}

export const dateTimeResolverQuery = queryResolver({
  resolverName: 'DateTime',
  mockFn: dateTimeMock,
})
