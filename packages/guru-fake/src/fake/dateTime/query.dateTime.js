import { gql } from '../../helpers.fake'

export const FakeDateTimeQuery = gql`
  FakeDateTime (
    locale: String,
    timeFormat: String,
    dataFormat: String
    ): FakeDateTime
  FakeDateTimeList  (
    limit: Int, 
    locale: String,
    timeFormat: String,
    dataFormat: String
  ): [FakeDateTimeList]
`
