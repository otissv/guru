import { gql } from '../../helpers.fake'
import { nested } from '../nested'

export const cardFields = gql`
  #=> "03/04"
  cardExp: String
  #=> "American Express"
  cardType: String
  #=> "4716506247152101" (if no vendor specified then random)
  cardNumber: String
 
`

export const FakeCardType = gql`
  type FakeCard {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    ${cardFields}
    ${nested}
  }
`

export const FakeCardListType = gql`
  type FakeCardList {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    #=> 1
    idx: Int
    #=> "card_1"
    idxName: String
    #=> "custom_1"
    idxCustom: String
    ${cardFields}
    ${nested}
  }
`
