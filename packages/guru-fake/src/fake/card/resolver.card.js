import casual from 'casual'

import {
  generateListResolver,
  generateResolver,
  getResolveFields,
  queryResolver,
} from '../resolve'

export function cardMock({ parent, args, context, info, selection }) {
  if (args.locale) {
    faker.locale = args.locale
  }

  return {
    cardExp: () => casual.card_exp,
    cardNumber: () => casual.card_number(args.cardVendor),
    cardType: () => casual.card_type,
    ...getResolveFields({ parent, args, context, info, selection }),
  }
}

export const cardResolverQuery = queryResolver({
  resolverName: 'Card',
  mockFn: cardMock,
})
