import { gql } from '../../helpers.fake'

export const FakeCardQuery = gql`
  FakeCard (locale: String): FakeCard
  FakeCardList  (limit: Int, locale: String): [FakeCardList]
`
