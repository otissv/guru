import faker from 'faker'

import { randomNumber } from '../../helpers.fake'
import {
  generateListResolver,
  generateResolver,
  getResolveFields,
  queryResolver,
} from '../resolve'

export function phoneMock({ parent, args, context, info, selection }) {
  if (args.locale) {
    faker.locale = args.locale
  }

  return {
    phoneFormats: () => faker.phone.phoneFormats(),
    phoneNumber: () => faker.phone.phoneNumber(),
    phoneNumberFormat: () => faker.phone.phoneNumberFormat(),
    phoneType: () =>
      ['home', 'work', 'mobile'](randomNumber({ min: 0, max: 1000 })),
    ...getResolveFields({ parent, args, context, info, selection }),
  }
}

export const phoneResolverQuery = queryResolver({
  resolverName: 'Phone',
  mockFn: phoneMock,
})
