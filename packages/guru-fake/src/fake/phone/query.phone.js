import { gql } from '../../helpers.fake'

export const FakePhoneQuery = gql`
FakePhone (locale: String): FakePhone
FakePhoneList  (limit: Int, locale: String): [FakePhoneList]
`
