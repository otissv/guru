import { gql } from '../../helpers.fake'
import { nested } from '../nested'

export const phoneFields = gql`
  #=> "###-###-####"
  phoneFormats: String
  #=> "1-928-788-1195"
  phoneNumber: String
  #=> "369-469-9390"
  phoneNumberFormat: String
`

export const FakePhoneType = gql`
  type FakePhone {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    ${phoneFields}
    ${nested}
  }
`

export const FakePhoneListType = gql`
  type FakePhoneList {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    #=> 1
    idx: Int
    #=> "phone_1"
    idxName: String
    #=> "custom_1"
    idxCustom: String
    ${phoneFields}
    ${nested}
  }
`
