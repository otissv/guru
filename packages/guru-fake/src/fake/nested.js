import { FakeAddressQuery } from './address/query.address'
import { FakeCardQuery } from './card/query.card'
import { FakeCardTransactionQuery } from './custom/cardTransaction/query.cardTransaction'
import { FakeColorQuery } from './color/query.color'
import { FakeCommerceQuery } from './commerce/query.commerce'
import { FakeCompanyQuery } from './company/query.company'
import { FakeCurrencyQuery } from './currency/query.currency'
import { FakeDatabaseQuery } from './database/query.database'
import { FakeDateTimeQuery } from './dateTime/query.dateTime'
import { FakeQuery } from './fake/query.fake'
import { FakeFinanceQuery } from './finance/query.finance'
import { FakeHackerQuery } from './hacker/query.hacker'
import { FakeImageQuery } from './image/query.image'
import { FakeInternetQuery } from './internet/query.internet'
import { FakeLoremQuery } from './lorem/query.lorem'
import { FakeMeetingQuery } from './custom/meeting/query.meeting'
import { FakeMessageQuery } from './custom/message/query.message'
import { FakeMetaQuery } from './custom/meta/query.meta'
import { FakeMiscQuery } from './misc/query.misc'
import { FakeNumberQuery } from './number/query.number'
import { FakePersonQuery } from './person/query.person'
import { FakePhoneQuery } from './phone/query.phone'
import { FakeSystemQuery } from './system/query.system'
import { FakeTransactionQuery } from './custom/transaction/query.transaction'
import { FakeUserQuery } from './custom/user/query.user'
import { FakeScalarQuery } from './scalar/query.scalar'

export const nested = [
  FakeAddressQuery,
  FakeCardQuery,
  FakeCardTransactionQuery,
  FakeColorQuery,
  FakeCommerceQuery,
  FakeCompanyQuery,
  FakeDatabaseQuery,
  FakeDateTimeQuery,
  FakeQuery,
  FakeFinanceQuery,
  FakeHackerQuery,
  FakeImageQuery,
  FakeInternetQuery,
  FakeLoremQuery,
  FakeMeetingQuery,
  FakeMessageQuery,
  FakeMetaQuery,
  FakeMiscQuery,
  FakeNumberQuery,
  FakePersonQuery,
  FakePhoneQuery,
  FakeSystemQuery,
  FakeTransactionQuery,
  FakeUserQuery,
  FakeScalarQuery,
].join(`
`)
