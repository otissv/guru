import { gql } from '../../helpers.fake'
import { nested } from '../nested'

export const commerceFields = gql`
    #=>  "magenta"
    color: String
    #=> "Computers"
    department: String,
    #=> 176.00
    price: Int
    #=> "Keyboard"
    product: String,
    #=> "Small"
    productAdjective: String,
    #=> "Fresh"
    productMaterial: String,
    #=> "Incredible Steel Ball"
    productName: String,


 
`

export const FakeCommerceType = gql`
  type FakeCommerce {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    ${commerceFields}
    ${nested}
  }
`

export const FakeCommerceListType = gql`
  type FakeCommerceList {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    #=> 1
    idx: Int
    #=> "commerce_1"
    idxName: String
    #=> "custom_1"
    idxCustom: String
    ${commerceFields}
    ${nested}
  }
`
