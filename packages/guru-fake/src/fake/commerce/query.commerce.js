import { gql } from '../../helpers.fake'

export const FakeCommerceQuery = gql`
  FakeCommerce (locale: String): FakeCommerce
  FakeCommerceList  (limit: Int, locale: String): [FakeCommerceList]
`
