import faker from 'faker'

import {
  generateListResolver,
  generateResolver,
  getResolveFields,
  queryResolver,
} from '../resolve'

export function commerceMock({ parent, args, context, info, selection }) {
  if (args.locale) {
    faker.locale = args.locale
  }

  return {
    color: () => faker.commerce.color(),
    department: () => faker.commerce.department(),
    price: () => faker.commerce.price(),
    product: () => faker.commerce.product(),
    productAdjective: () => faker.commerce.productAdjective(),
    productMaterial: () => faker.commerce.productMaterial(),
    productName: () => faker.commerce.productName(),
    ...getResolveFields({ parent, args, context, info, selection }),
  }
}

export const commerceResolverQuery = queryResolver({
  resolverName: 'Commerce',
  mockFn: commerceMock,
})
