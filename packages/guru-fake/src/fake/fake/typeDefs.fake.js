import { gql } from '../../helpers.fake'
import { nested } from '../nested'

import { addressFields } from '../address/typeDefs.address'
import { cardFields } from '../card/typeDefs.card'
import { colorFields } from '../color/typeDefs.color'
import { commerceFields } from '../commerce/typeDefs.commerce'
import { companyFields } from '../company/typeDefs.company'
import { currencyFields } from '../currency/typeDefs.currency'
import { databaseFields } from '../database/typeDefs.database'
import { dateTimeFields } from '../dateTime/typeDefs.dateTime'
import { fakeFields } from '../fake/typeDefs.fake'
import { financeFields } from '../finance/typeDefs.finance'
import { hackerFields } from '../hacker/typeDefs.hacker'
import { imageFields } from '../image/typeDefs.image'
import { internetFields } from '../internet/typeDefs.internet'
import { loremFields } from '../lorem/typeDefs.lorem'
import { miscFields } from '../misc/typeDefs.misc'
import { numberFields } from '../number/typeDefs.number'
import { personFields } from '../person/typeDefs.person'
import { phoneFields } from '../phone/typeDefs.phone'
import { systemFields } from '../system/typeDefs.system'

export const FakeType = gql`
  type Fake {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    ${addressFields}
    ${cardFields}
    ${colorFields}
    ${commerceFields}
    ${companyFields}
    ${currencyFields}
    ${databaseFields}
    ${dateTimeFields}
    ${fakeFields}
    ${financeFields}
    ${hackerFields}
    ${imageFields}
    ${internetFields}
    ${loremFields}
    ${miscFields}
    ${numberFields}
    ${personFields}
    ${phoneFields}
    ${systemFields}
    ${nested}
  }
`

export const FakeListType = gql`
  type FakeList {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    #=> 1
    idx: Int
    #=> "fake_1"
    idxName: String
    #=> "custom_1"
    idxCustom: String
    ${fakeFields}
    ${nested}
  }
`
