import { gql } from '../../helpers.fake'

export const FakeQuery = gql`
Fake(
  locale: String,
  cardVendor: String,
  timeFormat: String,
  dataFormat: String,
  integerFrom: Int,
  integerTo: Int,
  doubleFrom: Int,
  doubleTo: Int,
  arrayOfDigits: Int,
  arrayOfIntegers: Int,
  arrayOfDoubles: Int,
  sentences: Int,
  words: Int,
  arrayOfWords: Int,

): Fake
FakeList(
  limit: Int, 
  locale: String,
  cardVendor: String,
  timeFormat: String,
  dataFormat: String,
  integerFrom: Int,
  integerTo: Int,
  doubleFrom: Int,
  doubleTo: Int,
  arrayOfDigits: Int,
  arrayOfIntegers: Int,
  arrayOfDoubles: Int,
  sentences: Int,
  words: Int,
  arrayOfWords: Int,
): [FakeList]
`
