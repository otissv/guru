import {
  generateListResolver,
  generateResolver,
  getResolveFields,
  queryResolver,
} from '../resolve'

import { addressMock } from '../address/resolver.address'
import { cardMock } from '../card/resolver.card'
import { colorMock } from '../color/resolver.color'
import { commerceMock } from '../commerce/resolver.commerce'
import { companyMock } from '../company/resolver.company'
import { currencyMock } from '../currency/resolver.currency'
import { databaseMock } from '../database/resolver.database'
import { dateTimeMock } from '../dateTime/resolver.dateTime'
import { financeMock } from '../finance/resolver.finance'
import { hackerMock } from '../hacker/resolver.hacker'
import { imageMock } from '../image/resolver.image'
import { internetMock } from '../internet/resolver.internet'
import { loremMock } from '../lorem/resolver.lorem'
import { miscMock } from '../misc/resolver.misc'
import { numberMock } from '../number/resolver.number'
import { personMock } from '../person/resolver.person'
import { phoneMock } from '../phone/resolver.phone'
import { systemMock } from '../system/resolver.system'
// import { unsplashMock } from '../unsplash'

export function fakeMock({ parent, args, context, info, selection }) {
  if (args.locale) {
    faker.locale = args.locale
  }

  return {
    ...addressMock({ args }),
    ...cardMock({ args }),
    ...colorMock({ args }),
    ...commerceMock({ args }),
    ...companyMock({ args }),
    ...currencyMock({ args }),
    ...databaseMock({ args }),
    ...dateTimeMock({ args }),
    ...financeMock({ args }),
    ...hackerMock({ args }),
    ...imageMock({ args }),
    ...internetMock({ args }),
    ...loremMock({ args }),
    ...miscMock({ args }),
    ...numberMock({ args }),
    ...personMock({ args }),
    ...phoneMock({ args }),
    ...systemMock({ args }),
    ...getResolveFields({ parent, args, context, info, selection }),
  }
}

export const fakeResolverQuery = queryResolver({
  resolverName: '',
  mockFn: fakeMock,
})
