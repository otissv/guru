import faker from 'faker'
import casual from 'casual'

import {
  generateListResolver,
  generateResolver,
  getResolveFields,
  queryResolver,
} from '../resolve'

export function financeMock({ parent, args, context, info, selection }) {
  if (args.locale) {
    faker.locale = args.locale
  }

  const {
    symbol,
    name,
    symbol_native,
    decimal_digits,
    rounding,
    code,
    name_plural,
  } = casual.currency

  return {
    account: () => faker.finance.account(),
    accountName: () => faker.finance.accountName(),
    amount: () => faker.finance.amount(),
    bic: () => faker.finance.bic(),
    bitcoinAddress: () => faker.finance.bitcoinAddress(),
    currency: {
      code: () => code,
      decimalDigits: () => decimal_digits,
      name: () => name,
      namePlural: () => code,
      rounding: () => rounding,
      symbol: () => symbol,
      symbolNative: () => symbol_native,
    },
    currencyCode: () => faker.finance.currencyCode(),
    currencyName: () => faker.finance.account(),
    currencySymbol: () => faker.finance.currencySymbol(),
    ethereumAddress: () => faker.finance.ethereumAddress(),
    iban: () => faker.finance.iban(),
    mask: () => faker.finance.mask(),
    transactionType: () => faker.finance.transactionType(),
    ...getResolveFields({ parent, args, context, info, selection }),
  }
}

export const financeResolverQuery = queryResolver({
  resolverName: 'Finance',
  mockFn: financeMock,
})
