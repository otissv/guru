import { gql } from '../../helpers.fake'
import { nested } from '../nested'

export const financeFields = gql`
  #=> 83055001
  account: Int
  #=> "Money Market Account"
  accountName: String
  #=> 924
  amount: Float
  #=> "CHYAMSN1"
  bic: String
  #=> "32AFLAUMLFRL7NJOUBMIXA6T3T5BLE1"
  bitcoinAddress: String
  #=> 
  currency: FakeCurrency
  #=>
  ethereumAddress: String
  #=> "CR5343910980656600057"
  iban: String
  #=> "0298"
  mask: String
  #=> "payment"
  transactionType: String
`

export const FakeFinanceType = gql`
  type FakeFinance {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    ${financeFields}
    ${nested}
  }
`

export const FakeFinanceListType = gql`
  type FakeFinanceList {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    #=> 1
    idx: Int
    #=> "finance_1"
    idxName: String
    #=> "custom_1"
    idxCustom: String
    ${financeFields}
    ${nested}
  }
`
