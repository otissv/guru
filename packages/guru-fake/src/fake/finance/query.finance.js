import { gql } from '../../helpers.fake'

export const FakeFinanceQuery = gql`
FakeFinance (locale: String): FakeFinance
FakeFinanceList  (limit: Int, locale: String): [FakeFinanceList]
`
