import { gql } from '../../helpers.fake'
import { nested } from '../nested'

export const databaseFields = gql`
  #=> "utf8_unicode_ci"
  collation: String,
  #=> "token"
  column: String,
  #=> "MyISAM"
  engine: String,
  #=> "mediumint"
  type: String,
`

export const FakeDatabaseType = gql`
  type FakeDatabase {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    ${databaseFields}
    ${nested}
  }
`

export const FakeDatabaseListType = gql`
  type FakeDatabaseList {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    #=> 1
    idx: Int
    #=> "database_1"
    idxName: String
    #=> "custom_1"
    idxCustom: String
    ${databaseFields}
    ${nested}
  }
`
