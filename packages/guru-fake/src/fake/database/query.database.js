import { gql } from '../../helpers.fake'

export const FakeDatabaseQuery = gql`
  FakeDatabase (locale: String): FakeDatabase
  FakeDatabaseList  (limit: Int, locale: String): [FakeDatabaseList]
`
