import faker from 'faker'
import casual from 'casual'

import {
  generateListResolver,
  generateResolver,
  getResolveFields,
  queryResolver,
} from '../resolve'

export function databaseMock({ parent, args, context, info, selection }) {
  if (args.locale) {
    faker.locale = args.locale
  }

  return {
    collation: () => faker.database.collation(),
    column: () => faker.database.column(),
    engine: () => faker.database.engine(),
    type: () => faker.database.type(),
    ...getResolveFields({ parent, args, context, info, selection }),
  }
}

export const databaseResolverQuery = queryResolver({
  resolverName: 'Database',
  mockFn: databaseMock,
})
