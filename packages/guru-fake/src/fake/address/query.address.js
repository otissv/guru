import { gql } from '../../helpers.fake'

export const FakeAddressQuery = gql`
  FakeAddress (locale: String): FakeAddress
  FakeAddressList  (limit: Int, locale: String): [FakeAddressList]
`
