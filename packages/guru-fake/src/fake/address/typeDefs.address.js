import { gql } from '../../helpers.fake'
import { nested } from '../nested'

export const addressFields = gql`
  #=> 827
  buildingNumber: Int
  #=> "Marisaland"
  city: String
  #=> "fort"
  cityPrefix: String
  #=> "town"
  citySuffix: String
  #=>  "Japan"
  country: String
  #=> "PG"
  countryCode: String
  #=>  -64.7417
  latitude: Float
  #=> 143.732
  longitude: Float
  #=> "743"
  room: String
  #=> "Georgia"
  state: String
  #=> "LA"
  stateAbbr: String
  #=> "36617 Carolina Bridge"
  streetAddress: String
  #=> Kayli Drive"
  streetName: String
  #=> "b"
  streetPrefix: String
  #=> "Skyway",
  streetSuffix: String
  #=> "67804"
  zipCode: String
`

export const FakeAddressType = gql`
  type FakeAddress {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    ${addressFields}
    ${nested}
  }
`

export const FakeAddressListType = gql`
  type FakeAddressList {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    #=> 1
    idx: Int
    #=> "address_1"
    idxName: String
    #=> "custom_1"
    idxCustom: String
    ${addressFields}
    ${nested}
  }
`
