import faker from 'faker'
import casual from 'casual'

import { randomNumber } from '../../helpers.fake'
import {
  generateListResolver,
  generateResolver,
  getResolveFields,
  queryResolver,
} from '../resolve'

export function addressMock({ parent, args, context, info, selection }) {
  if (args.locale) {
    faker.locale = args.locale
  }

  return {
    buildingNumber: () => casual.building_number,
    city: () => faker.address.city(),
    cityPrefix: () => faker.address.cityPrefix(),
    citySuffix: () => faker.address.citySuffix(),
    country: () => faker.address.country(),
    countryCode: () => faker.address.countryCode(),
    latitude: () => faker.address.latitude(),
    longitude: () => faker.address.longitude(),
    room: () => `${randomNumber({ min: 1, max: 1000 })}`,
    state: () => faker.address.state(),
    stateAbbr: () => faker.address.stateAbbr(),
    streetAddress: () => faker.address.streetAddress(),
    streetName: () => faker.address.streetName(),
    streetPrefix: () => faker.address.streetPrefix(),
    streetSuffix: () => faker.address.streetSuffix(),
    zipCode: () => faker.address.zipCode(),
    ...getResolveFields({ parent, args, context, info, selection }),
  }
}

export const addressResolverQuery = queryResolver({
  resolverName: 'Address',
  mockFn: addressMock,
})
