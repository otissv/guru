import { gql } from '../../helpers.fake'

export const FakeCurrencyQuery = gql`
  FakeCurrency (locale: String): FakeCurrency
  FakeCurrencyList  (limit: Int, locale: String): [FakeCurrencyList]
`
