import faker from 'faker'
import casual from 'casual'

import {
  generateListResolver,
  generateResolver,
  getResolveFields,
  queryResolver,
} from '../resolve'

export function currencyMock({ parent, args, context, info, selection }) {
  if (args.locale) {
    faker.locale = args.locale
  }

  const {
    symbol,
    name,
    symbol_native,
    decimal_digits,
    rounding,
    code,
    name_plural,
  } = casual.currency

  return {
    currencyCode: () => code,
    decimalDigits: () => decimal_digits,
    currencyName: () => name,
    namePlural: () => code,
    rounding: () => rounding,
    currencySymbol: () => symbol,
    currencySymbolNative: () => symbol_native,
    ...getResolveFields({ parent, args, context, info, selection }),
  }
}

export const currencyResolverQuery = queryResolver({
  resolverName: 'Currency',
  mockFn: currencyMock,
})
