import { gql } from '../../helpers.fake'
import { nested } from '../nested'

export const currencyFields = gql`
  #=> "TOP"
  currencyCode: String
  #=> 2
  decimalDigits: Int
  #=> Tongan Paʻanga
  currencyName: String
  #=> "Tongan paʻanga
  namePlural: String
  #=> "T$"
  currencySymbol: String
  #=> "T$"
  currencySymbolNative: String
  #=> 0
  rounding: Int 
`

export const FakeCurrencyType = gql`
  type FakeCurrency {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    ${currencyFields}
    ${nested}
  }
`

export const FakeCurrencyListType = gql`
  type FakeCurrencyList {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    #=> 1
    idx: Int
    #=> "currency_1"
    idxName: String
    #=> "custom_1"
    idxCustom: String
    ${currencyFields}
    ${nested}
  }
`
