import faker from 'faker'

import {
  generateListResolver,
  generateResolver,
  getResolveFields,
  queryResolver,
} from '../resolve'

export function hackerMock({ parent, args, context, info, selection }) {
  if (args.locale) {
    faker.locale = args.locale
  }

  return {
    hackerAbbreviation: () => faker.hacker.abbreviation(),
    hackerAdjective: () => faker.hacker.adjective(),
    hackerIngverb: () => faker.hacker.ingverb(),
    hackerNoun: () => faker.hacker.noun(),
    hackerPhrase: () => faker.hacker.phrase(),
    hackerVerb: () => faker.hacker.verb(),
    ...getResolveFields({ parent, args, context, info, selection }),
  }
}

export const hackerResolverQuery = queryResolver({
  resolverName: 'Hacker',
  mockFn: hackerMock,
})
