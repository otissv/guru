import { gql } from '../../helpers.fake'
import { nested } from '../nested'

export const hackerFields = gql`
  #=> "GB"
  hackerAbbreviation: String
  #=> "multi-byte"
  hackerAdjective: String
  #=> "pixel"
  hackerNoun: String
  #=> "program"
  hackerVerb: String
  #=> "bypassing"
  hackerIngverb: String
  #=> "Try to transmit the RSS protocol maybe it will calculate the digital circuit!"
  hackerPhrase: String
`

export const FakeHackerType = gql`
  type FakeHacker {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    ${hackerFields}
    ${nested}
  }
`

export const FakeHackerListType = gql`
  type FakeHackerList {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    #=> 1
    idx: Int
    #=> "hacker_1"
    idxName: String
    #=> "custom_1"
    idxCustom: String
    ${hackerFields}
    ${nested}
  }
`
