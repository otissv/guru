import { gql } from '../../helpers.fake'

export const FakeHackerQuery = gql`
  FakeHacker (locale: String): FakeHacker
  FakeHackerList  (limit: Int, locale: String): [FakeHackerList]
`
