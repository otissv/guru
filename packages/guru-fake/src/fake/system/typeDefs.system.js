import { gql } from '../../helpers.fake'
import { nested } from '../nested'

export const systemFields = gql`
  #=>  "6eef1ffd-afc2-402c-9d96-cb081d0985d6"
  _id: String
  #=>  "jpe",
  commonFileExtension: String
  #=> "synthesize.pdf"
  commonFileName: String
  #=> "text"
  commonFileType: String
  #=>
  directoryPath: String
  #=> "pgp"
  fileExtension: String
  #=> "ball.3dml"
  fileName: String
  #=> "018e150d-2bb6-444d-a8c3-e1415f2310d6"
  filePath: String
  #=> "application/urc-ressheet+xml"
  mimeType: String
  #=> "0.8.0"
  semver: String
  #=> "367560b4-fd53-4808-a87a-053e179184c4"
  uuid: String
`

export const FakeSystemType = gql`
  type FakeSystem {
    #=> "2f4dc6ba-bd25-4e66-b369-43a13e0cf150"
    id: String
    ${systemFields}
    ${nested}
  }
`

export const FakeSystemListType = gql`
  type FakeSystemList {
    #=> "2f4dc6ba-bd25-4e66-b369-43a13e0cf150"
    id: String
    #=> 1
    idx: Int
    #=> "system_1"
    idxName: String
    #=> "custom_1"
    idxCustom: String
    ${systemFields}
    ${nested}
  }
`
