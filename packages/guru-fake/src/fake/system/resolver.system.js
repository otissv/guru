import faker from 'faker'
import casual from 'casual'

import {
  generateListResolver,
  generateResolver,
  getResolveFields,
  queryResolver,
} from '../resolve'

export function systemMock({ parent, args, context, info, selection }) {
  if (args.locale) {
    faker.locale = args.locale
  }

  return {
    _id: () => faker.random.uuid(),
    commonFileExtension: () => faker.system.commonFileExt(),
    commonFileName: () => faker.system.commonFileName(),
    commonFileType: () => faker.system.commonFileType(),
    directoryPath: () => faker.system.directoryPath(),
    fileExtension: () => faker.system.fileExt(),
    fileName: () => faker.system.fileName(),
    filePath: () => faker.system.filePath(),
    mimeType: () => faker.system.mimeType(),
    semver: () => faker.system.semver(),
    uuid: () => faker.random.uuid(),
    ...getResolveFields({ parent, args, context, info, selection }),
  }
}

export const systemResolverQuery = queryResolver({
  resolverName: 'System',
  mockFn: systemMock,
})
