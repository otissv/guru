import { gql } from '../../helpers.fake'

export const FakeSystemQuery = gql`
FakeSystem (locale: String): FakeSystem
FakeSystemList  (limit: Int, locale: String): [FakeSystemList]
`
