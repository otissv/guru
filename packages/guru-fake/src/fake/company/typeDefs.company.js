import { gql } from '../../helpers.fake'
import { nested } from '../nested'

export const companyFields = gql`
  #=> "turn-key visualize communities"
  bs: String
  #=> "transparent"
  bsAdjective: String
  #=> "matrix"
  bsBuzz: String
  #=> "functionalities"
  bsNoun: String
  #=> "Synchronised optimal concept" 
  catchPhrase: String
  #=> "Cross-platform"
  catchPhraseAdjective: String
  #=> "homogeneous"
  catchPhraseDescriptor: String
  #=> "frame"
  catchPhraseNoun: String
  #=> "Cole Wuckert and Strosin"
  companyName: String
  #=> "Inc" 
  companySuffix: String
  #=> "Inc and SonsLLCGroup"
  suffixes: String
`

export const FakeCompanyType = gql`
  type FakeCompany {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    ${companyFields}
    ${nested}
  }
`

export const FakeCompanyListType = gql`
  type FakeCompanyList {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    #=> 1
    idx: Int
    #=> "company_1"
    idxName: String
    #=> "custom_1"
    idxCustom: String
    ${companyFields}
    ${nested}
  }
`
