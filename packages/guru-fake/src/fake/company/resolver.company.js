import faker from 'faker'
import casual from 'casual'

import { randomNumber } from '../../helpers.fake'
import {
  generateListResolver,
  generateResolver,
  getResolveFields,
  queryResolver,
} from '../resolve'

export function companyMock({ parent, args, context, info, selection }) {
  if (args.locale) {
    faker.locale = args.locale
  }

  return {
    bs: () => faker.company.bs(),
    bsAdjective: () => faker.company.bsAdjective(),
    bsBuzz: () => faker.company.bsBuzz(),
    bsNoun: () => faker.company.bsNoun(),
    catchPhrase: () => faker.company.catchPhrase(),
    catchPhraseAdjective: () => faker.company.catchPhraseAdjective(),
    catchPhraseDescriptor: () => faker.company.catchPhraseDescriptor(),
    catchPhraseNoun: () => faker.company.catchPhraseNoun(),
    companyName: () => faker.company.companyName(),
    companySuffix: () => faker.company.companySuffix(),
    suffixes: () => faker.company.suffixes()[randomNumber({ min: 0, max: 3 })],
    ...getResolveFields({ parent, args, context, info, selection }),
  }
}

export const companyResolverQuery = queryResolver({
  resolverName: 'Company',
  mockFn: companyMock,
})
