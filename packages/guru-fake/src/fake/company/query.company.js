import { gql } from '../../helpers.fake'

export const FakeCompanyQuery = gql`
  FakeCompany (locale: String): FakeCompany
  FakeCompanyList  (limit: Int, locale: String): [FakeCompanyList]
`
