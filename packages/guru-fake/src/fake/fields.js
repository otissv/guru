import { addressFields } from './address/typeDefs.address'
import { cardFields } from './card/typeDefs.card'
import { cardTransactionFields } from './cardTransaction/typeDefs.cardTransaction'
import { colorFields } from './color/typeDefs.color'
import { commerceFields } from './commerce/typeDefs.commerce'
import { companyFields } from './company/typeDefs.company'
import { currencyFields } from './currency/typeDefs.currency'
import { databaseFields } from './database/typeDefs.database'
import { dateTimeFields } from './dateTime/typeDefs.dateTime'
import { fakeFields } from './fake/typeDefs.fake'
import { financeFields } from './finance/typeDefs.finance'
import { hackerFields } from './hacker/typeDefs.hacker'
import { imageFields } from './image/typeDefs.image'
import { internetFields } from './internet/typeDefs.internet'
import { loremFields } from './lorem/typeDefs.lorem'
import { meetingFields } from './meeting/typeDefs.meeting'
import { messageFields } from './message/typeDefs.message'
import { metaFields } from './meta/typeDefs.meta'
import { miscFields } from './misc/typeDefs.misc'
import { numbersFields } from './numbers/typeDefs.numbers'
import { personFields } from './person/typeDefs.person'
import { phoneFields } from './phone/typeDefs.phone'
import { systemFields } from './system/typeDefs.system'
import { transactionFields } from './transaction/typeDefs.transaction'
// import { unsplashFields } from './unsplash/typeDefs.unsplash'
import { userFields } from './user/typeDefs.user'

export const fields = [
  addressFields,
  cardFields,
  colorFields,
  commerceFields,
  companyFields,
  currencyFields,
  databaseFields,
  dateTimeFields,
  fakeFields,
  financeFields,
  hackerFields,
  imageFields,
  internetFields,
  loremFields,
  miscFields,
  numbersFields,
  personFields,
  phoneFields,
  systemFields,
].join(`
`)
