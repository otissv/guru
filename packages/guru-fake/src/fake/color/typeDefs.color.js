import { gql } from '../../helpers.fake'
import { nested } from '../nested'

export const colorFields = gql`
  #=> "DarkOliveGreen"
  colorName: String
  #=> "maroon"
  safeColorName: String
  #=> "#2e4e1f"
  rgbHex: String
  #=> [ 194, 193, 166, ]
  rgbArray: [Int]
`

export const FakeColorType = gql`
  type FakeColor {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    ${colorFields}
    ${nested}
  }
`

export const FakeColorListType = gql`
  type FakeColorList {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    #=> 1
    idx: Int
    #=> "color_1"
    idxName: String
    #=> "custom_1"
    idxCustom: String
    ${colorFields}
    ${nested}
  }
`
