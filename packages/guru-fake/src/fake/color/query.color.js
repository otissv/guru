import { gql } from '../../helpers.fake'

export const FakeColorQuery = gql`
  FakeColor (locale: String) : FakeColor
  FakeColorList (limit: Int, locale: String): [FakeColorList]
`
