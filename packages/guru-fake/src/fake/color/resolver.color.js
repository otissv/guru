import casual from 'casual'

import {
  generateListResolver,
  generateResolver,
  getResolveFields,
  queryResolver,
} from '../resolve'

export function colorMock({ parent, args, context, info, selection }) {
  return {
    colorName: () => casual.color_name,
    rgbArray: () => casual.rgb_array,
    rgbHex: () => casual.rgb_hex,
    safeColorName: () => casual.safe_color_name,
    ...getResolveFields({ parent, args, context, info, selection }),
  }
}

export const colorResolverQuery = queryResolver({
  resolverName: 'Color',
  mockFn: colorMock,
})
