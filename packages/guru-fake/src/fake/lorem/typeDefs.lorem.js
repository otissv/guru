import { gql } from '../../helpers.fake'
import { nested } from '../nested'

export const loremFields = gql`
  #=> [ "voluptas" "atque" "vitae" "vel" "dolor" "saepe" "ut" ]
  arrayOfWords: [String]
  #=> "Vel et rerum nostrum quia. Dolorum fuga nobis sit natus consequatur."
  description: String
  #=> "k"
  letter: String
  #=> "Sit optio omnis modi nam rem quis. ..."
  lines: String
  #=> "Est facilis totam eum distinctio tenetur ratione. Omnis qui voluptas. Expedita ab optio eos quia qui dolores."
  paragraph: String
  #=>"Est facilis totam eum distinctio tenetur ratione. Omnis qui voluptas. Expedita ab optio eos quia qui dolores. ..."
  paragraphs: String
  #=> "Laborum eius porro consequatur."
  sentence: String
  #=> "Dolorum fuga nobis sit natus consequatur. Laboriosam sapiente. Natus quos ut."
  sentences: String
  #=> "Qui iste similique iusto."
  shortDescription: String
  #=> "quibusdam-dolorem-perspiciatis"
  slug: String
  #=> "saepe quia molestias voluptates et"
  string: String
  #=> "Nemo tempore natus non accusamus eos placeat nesciunt. et fugit ut odio nisi dolore non ... (long text)"
  text: String
  #=> "Systematic nobis"
  title: String
  #=> "voluptatem"
  word: String
  #=> "sed quis ut beatae id adipisci aut"
  words: String
`

export const FakeLoremType = gql`
  type FakeLorem {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    ${loremFields}
    ${nested}
  }
`

export const FakeLoremListType = gql`
  type FakeLoremList {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    #=> 1
    idx: Int
    #=> "lorem_1"
    idxName: String
    #=> "custom_1"
    idxCustom: String
    ${loremFields}
    ${nested}
  }
`
