import { gql } from '../../helpers.fake'

export const FakeLoremQuery = gql`
  FakeLorem (
    locale: String,
    arrayOfWords: Int
    sentences: Int,  
    words: Int,
    ): FakeLorem
  FakeLoremList  (
    limit: Int, 
    locale: String,
    arrayOfWords: Int
    sentences: Int,  
    words: Int,
  ): [FakeLoremList]
  `
