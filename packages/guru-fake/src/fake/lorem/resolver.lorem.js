import faker from 'faker'
import casual from 'casual'

import {
  generateListResolver,
  generateResolver,
  getResolveFields,
  queryResolver,
} from '../resolve'

export function loremMock({ parent, args, context, info, selection }) {
  if (args.locale) {
    faker.locale = args.locale
  }

  const arrayOfWords = args.arrayOfWords || 1
  const sentences = args.sentences || 1
  const words = args.words || 1

  return {
    arrayOfWords: () => casual.array_of_words(arrayOfWords),
    description: () => casual.description,
    letter: () => faker.helpers.randomize(),
    lines: () => faker.lorem.lines(),
    paragraph: () => faker.lorem.paragraph(),
    paragraphs: () => faker.lorem.paragraphs(),
    sentence: () => faker.lorem.sentence(),
    sentences: () => casual.sentences(sentences),
    shortDescription: () => casual.short_description,
    slug: () => faker.lorem.slug(),
    string: () => casual.string,
    text: () => faker.lorem.text(),
    title: () => casual.title,
    word: () => faker.lorem.word(),
    words: () => casual.words(words),
    ...getResolveFields({ parent, args, context, info, selection }),
  }
}

export const loremResolverQuery = queryResolver({
  resolverName: 'Lorem',
  mockFn: loremMock,
})
