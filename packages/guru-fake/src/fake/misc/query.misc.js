import { gql } from '../../helpers.fake'

export const FakeMiscQuery = gql`
FakeMisc (locale: String): FakeMisc
FakeMiscList  (limit: Int, locale: String): [FakeMiscList]
`
