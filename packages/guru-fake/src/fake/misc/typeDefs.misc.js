import { gql } from '../../helpers.fake'
import { nested } from '../nested'

export const miscFields = gql`
  #=> "ru"
  languageCode: String
  #=> "hi_IN"
  locale: String
  #=> true
  boolean: Boolean
  #=> "z"
  alphaNumeric: String
  #=> "c"
  randomize: String
`

export const FakeMiscType = gql`
  type FakeMisc {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    ${miscFields}
    ${nested}
  }
`

export const FakeMiscListType = gql`
  type FakeMiscList {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    #=> 1
    idx: Int
    #=> "misc_1"
    idxName: String
    #=> "custom_1"
    idxCustom: String
    ${miscFields}
    ${nested}
  }
`
