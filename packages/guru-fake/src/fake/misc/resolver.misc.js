import faker from 'faker'
import casual from 'casual'

import {
  generateListResolver,
  generateResolver,
  getResolveFields,
  queryResolver,
} from '../resolve'

export function miscMock({ parent, args, context, info, selection }) {
  if (args.locale) {
    faker.locale = args.locale
  }

  return {
    languageCode: () => casual.language_code,
    locale: () => faker.random.locale(),
    boolean: () => faker.random.boolean(),
    alphaNumeric: () => faker.random.alphaNumeric(),
    randomize: () => faker.helpers.randomize(),
    ...getResolveFields({ parent, args, context, info, selection }),
  }
}

export const miscResolverQuery = queryResolver({
  resolverName: 'Misc',
  mockFn: miscMock,
})
