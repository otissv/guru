import { gql } from '../../helpers.fake'

export const FakeNumberQuery = gql`
FakeNumber (locale: String): FakeNumber
FakeNumberList  (limit: Int, locale: String): [FakeNumberList]
`
