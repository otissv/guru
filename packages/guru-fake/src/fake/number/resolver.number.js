import faker from 'faker'
import casual from 'casual'

import { randomNumber } from '../../helpers.fake'
import {
  generateListResolver,
  generateResolver,
  getResolveFields,
  queryResolver,
} from '../resolve'

export function numberMock({ parent, args, context, info, selection }) {
  if (args.locale) {
    faker.locale = args.locale
  }

  const integerFrom = args.integerFrom || -1000
  const integerTo = args.integerTo || 1000
  const doubleFrom = args.doubleFrom || -1000
  const doubleTo = args.doubleTo || 1000
  const arrayOfDigits = args.arrayOfDigits || 7
  const arrayOfIntegers = args.arrayOfIntegers || 7
  const arrayOfDoubles = args.arrayOfDoubles || 7
  const randomFrom = args.randomFrom || 0
  const randomTo = args.randomTo || 1000

  return {
    arrayOfDigits: () => casual.array_of_digits(arrayOfDigits),
    arrayOfDoubles: () => casual.array_of_doubles(arrayOfDoubles),
    arrayOfIntegers: () => casual.array_of_integers(arrayOfIntegers),
    double: () => casual.double(doubleFrom, doubleTo),
    integer: () => casual.integer(integerFrom, integerTo),
    random: () => faker.random.number(),
    randomBetween: () => randomNumber({ min: randomFrom, max: randomTo }),
    ...getResolveFields({ parent, args, context, info, selection }),
  }
}

export const numberResolverQuery = queryResolver({
  resolverName: 'Number',
  mockFn: numberMock,
})
