import { gql } from '../../helpers.fake'
import { nested } from '../nested'

export const numberFields = gql`
  #=> [ 4, 8, 3, 1, 7, 6, 6 ]
  arrayOfDigits: [Int],
  #=> [ -866.3755785673857, -166.62194719538093, ...]
  arrayOfDoubles: [Float],
  #=> [ -105, -7, -532, -596, -430, -957, -234 ]
  arrayOfIntegers: [Int],
  #=> -234.12987444
  double: Float,
  #=> 632
  integer: Int,
  #=> 0.7171590146608651 
  random: Float,
  #=> 4
  randomBetween: Int
`

export const FakeNumberType = gql`
  type FakeNumber {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    ${numberFields}
    ${nested}
  }
`

export const FakeNumberListType = gql`
  type FakeNumberList {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    #=> 1
    idx: Int
    #=> "number_1"
    idxName: String
    #=> "custom_1"
    idxCustom: String
    ${numberFields}
    ${nested}
  }
`
