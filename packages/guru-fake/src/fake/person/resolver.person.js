import faker from 'faker'
import casual from 'casual'
import { getInfoSelection } from 'guru-utils'

import {
  generateListResolver,
  generateResolver,
  getResolveFields,
  queryResolver,
} from '../resolve'

export function personMock({ parent, args, context, info, selection }) {
  if (args.locale) {
    faker.locale = args.locale
  }

  return {
    email: () => faker.internet.email(),
    firstName: () => faker.name.firstName(),
    fullName: () => `${faker.name.firstName()} ${faker.name.lastName()}`,
    jobArea: () => faker.name.jobArea(),
    jobDescriptor: () => faker.name.jobDescriptor(),
    jobTitle: () => faker.name.title(),
    jobType: () => faker.name.jobType(),
    lastName: () => faker.name.lastName(),
    name: () =>
      `${faker.name.prefix()} ${faker.name.firstName()} ${faker.name.lastName()}`,
    namePrefix: () => faker.name.prefix(),
    nameSuffix: () => faker.name.suffix(),
    username: () => faker.internet.userName(),
    ...getResolveFields({ parent, args, context, info, selection }),
  }
}

export const personResolverQuery = queryResolver({
  resolverName: 'Person',
  mockFn: personMock,
})
