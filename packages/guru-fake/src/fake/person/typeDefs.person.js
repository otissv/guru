import { gql } from '../../helpers.fake'
import { nested } from '../nested'

export const personFields = gql`
  #=> "Josue.Hessel@claire.us"
  email: String
  #=> "Derek"
  firstName: String
  #=> "Kadin Torphy"
  fullName: String
  #=> "Paradigm"
  jobArea: String
  #=> "District"
  jobDescriptor: String
  #=> "Internal Implementation Planner"
  jobTitle: String
  #=> "Strategist"
  jobType: String
  #=> "Considine"
  lastName: String
  #=> "Alberto"
  name: String
  #=> "Jr."
  namePrefix: String
  #=> "Cole Wuckert and Strosin"
  nameSuffix: String
  #=> "Darryl"
  username: String
`

export const FakePersonType = gql`
type FakePerson {
  #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
  id: String
  ${personFields}
  ${nested}
}
`

export const FakePersonListType = gql`
type FakePersonList {
  #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
  id: String
  #=> 1
  idx: Int
  #=> "person_1"
  idxName: String
  #=> "custom_1"
  idxCustom: String
  ${personFields}
  ${nested}
}
`
