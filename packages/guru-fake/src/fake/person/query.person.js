import { gql } from '../../helpers.fake'

export const FakePersonQuery = gql`
  FakePerson (locale: String): FakePerson
  FakePersonList  (limit: Int, locale: String): [FakePersonList]
`
