import { gql } from '../../../helpers.fake'

export const FakeMeetingQuery = gql`
  FakeMeeting (locale: String): FakeMeeting
  FakeMeetingList  (limit: Int, locale: String): [FakeMeetingList]
`
