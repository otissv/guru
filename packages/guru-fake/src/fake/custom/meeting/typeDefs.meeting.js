import { gql } from '../../../helpers.fake'
import { nested } from '../../nested'

export const meetingFields = gql`
  #=> false
  allDay: Boolean
  #=> "Beatae praesentium ipsum architecto sit sed laudantium et."
  description: String
  #=> "2017-03-26T08:10:00.000Z"
  end: String
  #=> 
  invited: [FakePerson]
  #=> "2017-03-10T08:07:00.000Z"
  start: String
  #=> "Sit debitis magni"
  title: String
`

export const FakeMeetingType = gql`
  type FakeMeeting {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    ${meetingFields}
    ${nested}
  }
`

export const FakeMeetingListType = gql`
  type FakeMeetingList {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    #=> 1
    idx: Int
    #=> "meeting_1"
    idxName: String
    #=> "custom_1"
    idxCustom: String
    ${meetingFields}
    ${nested}
  }
`
