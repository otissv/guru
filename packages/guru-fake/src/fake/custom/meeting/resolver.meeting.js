import faker from 'faker'
import casual from 'casual'

import { getDates } from '../../../helpers.fake'
import {
  generateListResolver,
  generateResolver,
  getResolveFields,
  queryResolver,
} from '../../resolve'

import { addressMock } from '../../address/typeDefs.address'
import { personMock } from '../../person/typeDefs.person'

export function meetingMock({ parent, args, context, info, selection }) {
  if (args.locale) {
    faker.locale = args.locale
  }

  return {
    allDay: () => faker.random.boolean(),
    address: () => addressMock({ args }),
    invited: () => [
      personMock({ args }),
      personMock({ args }),
      personMock({ args }),
    ],
    description: () => faker.lorem.sentence(),
    end: () => getDates().endDate,
    start: () => getDates().startDate,
    title: () => casual.title,
    ...getResolveFields({ parent, args, context, info, selection }),
  }
}

export const meetingResolverQuery = queryResolver({
  resolverName: 'Meeting',
  mockFn: meetingMock,
})
