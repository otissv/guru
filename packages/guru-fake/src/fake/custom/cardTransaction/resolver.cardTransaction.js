import faker from 'faker'
import casual from 'casual'

import {
  generateListResolver,
  generateResolver,
  getResolveFields,
  queryResolver,
} from '../../resolve'

import { cardMock } from '../../card/resolver.card'
import { transactionMock } from '../transaction/resolver.transaction'

export function cardTransactionMock({
  parent,
  args,
  context,
  info,
  selection,
}) {
  if (args.locale) {
    faker.locale = args.locale
  }

  return {
    ...cardMock({ args }),
    ...transactionMock({ args }),
    ...faker.helpers.createCard().accountHistory[0],
    ...getResolveFields({ parent, args, context, info, selection }),
  }
}

export const cardTransactionResolverQuery = queryResolver({
  resolverName: 'CardTransaction',
  mockFn: cardTransactionMock,
})
