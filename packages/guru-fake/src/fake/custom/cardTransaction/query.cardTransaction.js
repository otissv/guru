import { gql } from '../../../helpers.fake'

export const FakeCardTransactionQuery = gql`
  FakeCardTransaction (
    locale: String,
    cardVendor: String
  ): FakeCardTransaction
  FakeCardTransactionList  (
    limit: Int, 
    locale: String,
    cardVendor: String
  ): [FakeCardTransactionList]
`
