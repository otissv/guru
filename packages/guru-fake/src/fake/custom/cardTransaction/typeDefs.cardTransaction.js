import { gql } from '../../../helpers.fake'
import { nested } from '../../nested'
import { cardFields } from '../../card/typeDefs.card'
import { transactionFields } from '../transaction/typeDefs.transaction'

export const cardTransactionFields = gql`
  ${cardFields}
  ${transactionFields}
`

export const FakeCardTransactionType = gql`
  type FakeCardTransaction {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    ${cardTransactionFields}
    ${nested}
  }
`

export const FakeCardTransactionListType = gql`
  type FakeCardTransactionList {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    #=> 1
    idx: Int
    #=> "cardTransaction_1"
    idxName: String
    #=> "custom_1"
    idxCustom: String
    ${cardTransactionFields}
    ${nested}
  }
`
