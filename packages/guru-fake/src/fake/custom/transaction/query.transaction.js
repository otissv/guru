import { gql } from '../../../helpers.fake'

export const FakeTransactionQuery = gql`
FakeTransaction (locale: String, cardVendor: String): FakeTransaction
FakeTransactionList  (limit: Int, locale: String, cardVendor: String): [FakeTransactionList]
`
