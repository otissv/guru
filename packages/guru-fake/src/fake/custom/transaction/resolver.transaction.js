import faker from 'faker'
import casual from 'casual'
import moment from 'moment'

import { randomNumber } from '../../../helpers.fake'
import {
  generateListResolver,
  generateResolver,
  getResolveFields,
  queryResolver,
} from '../../resolve'

import { currencyMock } from '../../currency/resolver.currency'

export function transactionMock({ parent, args, context, info, selection }) {
  if (args.locale) {
    faker.locale = args.locale
  }

  return {
    ...currencyMock({ args }),
    amount: () => faker.finance.amount(),
    date: () => moment(faker.date.past()).toISOString(),
    company: () => faker.company.companyName(),
    accountName: () => faker.finance.accountName(),
    type: () => faker.finance.transactionType(),
    account: () => faker.finance.account(),
    ...getResolveFields({ parent, args, context, info, selection }),
  }
}

export const transactionResolverQuery = queryResolver({
  resolverName: 'Transaction',
  mockFn: transactionMock,
})
