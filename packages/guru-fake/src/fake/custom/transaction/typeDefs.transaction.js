import { gql } from '../../../helpers.fake'
import { nested } from '../../nested'
import { currencyFields } from '../../currency/typeDefs.currency'

export const transactionFields = gql`
  ${currencyFields},
  #=> "75880972"
  account: String
  #=> "Home Loan Account 2415"
  accountName: String
  #=> "288.00"
  amount: Float
  #=> "Rutherford, Prohaska and Kuhic"
  company: String
  #=> "2012-02-01T23:00:00.000Z"
  date: String
  #=> "withdrawal"
  type: String

`

export const FakeTransactionType = gql`
  type FakeTransaction {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    ${transactionFields}
    ${nested}
  }
`

export const FakeTransactionListType = gql`
  type FakeTransactionList {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    #=> 1
    idx: Int
    #=> "transaction_1"
    idxName: String
    #=> "custom_1"
    idxCustom: String
    ${transactionFields}
    ${nested}
  }
`
