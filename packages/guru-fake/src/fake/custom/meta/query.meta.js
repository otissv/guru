import { gql } from '../../../helpers.fake'

export const FakeMetaQuery = gql`
FakeMeta (
  locale: String,
): FakeMeta
FakeMetaList  (
  limit: Int, 
  locale: String,
): [FakeMetaList]
`
