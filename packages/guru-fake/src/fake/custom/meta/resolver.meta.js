import faker from 'faker'
import moment from 'moment'

import {
  generateListResolver,
  generateResolver,
  getResolveFields,
  queryResolver,
} from '../../resolve'

export function metaMock({ parent, args, context, info, selection }) {
  if (args.locale) {
    faker.locale = args.locale
  }

  return {
    created: () => moment(faker.date.past()).toISOString(),
    createdBy: () => userMock({ args }),
    updated: () => moment(faker.date.recent()).toISOString(),
    updatedBy: () => userMock({ args }),
    ...getResolveFields({ parent, args, context, info, selection }),
  }
}

export const metaResolverQuery = queryResolver({
  resolverName: 'Meta',
  mockFn: metaMock,
})
