import { gql } from '../../../helpers.fake'
import { nested } from '../../nested'

export const metaFields = gql`
  #=> returns a user
  created: String
  #=> "2016-03-23T13:48:40.797Z"
  createdBy: FakeUser
  #=> "2017-03-18T02:51:39.287Z"
  updated: String
  #=> returns a user
  updatedBy: FakeUser
`

export const FakeMetaType = gql`
  type FakeMeta {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    ${metaFields}
    ${nested}
  }
`

export const FakeMetaListType = gql`
  type FakeMetaList {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    #=> 1
    idx: Int
    #=> "meta_1"
    idxName: String
    #=> "custom_1"
    idxCustom: String
    ${metaFields}
    ${nested}
  }
`
