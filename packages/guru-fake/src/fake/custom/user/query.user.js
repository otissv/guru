import { gql } from '../../../helpers.fake'

export const FakeUserQuery = gql`
  FakeUser (
    locale: String,
    roles: [String]
  ): FakeUser
  FakeUserList (
    limit: Int,
    locale: String,
    roles: [String]
  ): [FakeUserList]  
`
