import { gql } from '../../../helpers.fake'
import { nested } from '../../nested'
import { personFields } from '../../person/typeDefs.person'

export const userFields = gql`
  ${personFields}
  #=>
  userAvatar: String
  #=> "FzWVa3Mn9IHXM0J"
  password: String
  #=>
  phoneNumber: String
  #=>
  website: String
  roles: [String]
`

export const FakeUserType = gql`
  type FakeUser {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    ${userFields}
    ${nested}
  }
`

export const FakeUserListType = gql`
  type FakeUserList {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    #=> 1
    idx: Int
    #=> "user_1"
    idxName: String
    #=> "custom_1"
    idxCustom: String
    ${userFields}
    ${nested}
  }
`
