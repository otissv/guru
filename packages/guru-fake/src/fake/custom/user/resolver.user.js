import faker from 'faker'
import casual from 'casual'

import {
  generateListResolver,
  generateResolver,
  getResolveFields,
  queryResolver,
} from '../../resolve'
import { personMock } from '../../person/resolver.person'

export function userMock({ parent, args, context, info, selection }) {
  if (args.locale) {
    faker.locale = args.locale
  }

  return {
    ...personMock({ args }),
    userAvatar: () => faker.image.avatar(),
    password: () => faker.internet.password(),
    phoneNumber: () => faker.phone.phoneNumber(),
    website: () => faker.internet.domainName(),
    roles: () => args.roles || ['user'],
    ...getResolveFields({ parent, args, context, info, selection }),
  }
}

export const userResolverQuery = queryResolver({
  resolverName: 'User',
  mockFn: userMock,
})
