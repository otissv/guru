import { gql } from '../../../helpers.fake'
import { nested } from '../../nested'

export const messageFields = gql`
  #=> "Quisquam et itaque esse optio possimus aut molestias vel..."
  body: String
  #=> ["Mario_Simonis@yahoo.com"]
  cc: [String]
  #=> "2016-10-27T22:19:27.657Z"
  date: String
  #=> "Zackary.Mante@yahoo.com"
  from: String
  #=> "Recusandae eos laudantium"
  subject: String
  #=> ["Joseph.Bayer88@gmail.com"]
  to: [String]
`

export const FakeMessageType = gql`
  type FakeMessage {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    ${messageFields}
    ${nested}
  }
`

export const FakeMessageListType = gql`
  type FakeMessageList {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    #=> 1
    idx: Int
    #=> "message_1"
    idxName: String
    #=> "custom_1"
    idxCustom: String
    ${messageFields}
    ${nested}
  }
`
