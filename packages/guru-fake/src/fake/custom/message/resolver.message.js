import faker from 'faker'
import casual from 'casual'
import moment from 'moment'

import {
  generateListResolver,
  generateResolver,
  getResolveFields,
  queryResolver,
} from '../../resolve'

export function messageMock({ parent, args, context, info, selection }) {
  if (args.locale) {
    faker.locale = args.locale
  }

  return {
    body: () => faker.lorem.paragraphs(),
    cc: () => [faker.internet.email()],
    date: () => moment(faker.date.past()).toISOString(),
    from: () => faker.internet.email(),
    subject: () => casual.title,
    to: () => [faker.internet.email()],
    ...getResolveFields({ parent, args, context, info, selection }),
  }
}

export const messageResolverQuery = queryResolver({
  resolverName: 'Message',
  mockFn: messageMock,
})
