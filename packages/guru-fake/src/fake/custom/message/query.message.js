import { gql } from '../../../helpers.fake'

export const FakeMessageQuery = gql`
  FakeMessage (locale: String): FakeMessage
  FakeMessageList  (limit: Int, locale: String): [FakeMessageList]
`
