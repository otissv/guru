export const scalarQuery = {
  FakeString: (parent, args, _context, _info) => {
    return args.value
  },

  FakeInt: (parent, args, _context, _info) => {
    return args.value
  },

  FakeFloat: (parent, args, _context, _info) => {
    return args.value
  },

  FakeBoolean: (parent, args, _context, _info) => {
    return args.value
  },
}
