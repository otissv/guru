import { gql } from '../../helpers.fake'

export const scalarFields = gql`
  FakeString: String
  FakeInt: Int
  FakeFloat: Float
  FakeBoolean: Boolean
  FakeID: ID
`
