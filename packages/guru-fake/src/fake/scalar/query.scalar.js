import { gql } from '../../helpers.fake'

export const FakeScalarQuery = gql`
  FakeString(value: String) : String
  FakeInt(value: Int) : Int
  FakeFloat(value: Float) : Float
  FakeBoolean(value: Boolean) : Boolean
`
