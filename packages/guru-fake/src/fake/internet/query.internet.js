import { gql } from '../../helpers.fake'

export const FakeInternetQuery = gql`
FakeInternet (locale: String): FakeInternet
FakeInternetList  (limit: Int, locale: String): [FakeInternetList]
`
