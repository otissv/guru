import faker from 'faker'
import casual from 'casual'

import { randomNumber } from '../../helpers.fake'
import {
  generateListResolver,
  generateResolver,
  getResolveFields,
  queryResolver,
} from '../resolve'

export function internetMock({ parent, args, context, info, selection }) {
  if (args.locale) {
    faker.locale = args.locale
  }

  return {
    email: () => faker.internet.email(),
    exampleEmail: () => faker.internet.exampleEmail(),
    userName: () => faker.internet.userName(),
    protocol: () => faker.internet.protocol(),
    url: () => faker.internet.url(),
    domainName: () => faker.internet.domainName(),
    domainSuffix: () => faker.internet.domainSuffix(),
    domainWord: () => faker.internet.domainWord(),
    ip: () => faker.internet.ip(),
    ipv6: () => faker.internet.ipv6(),
    userAgent: () => faker.internet.userAgent(),
    mac: () => faker.internet.mac(),
    password: () => faker.internet.password(),
    ...getResolveFields({ parent, args, context, info, selection }),
  }
}

export const internetResolverQuery = queryResolver({
  resolverName: 'Internet',
  mockFn: internetMock,
})
