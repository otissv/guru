import { gql } from '../../helpers.fake'
import { nested } from '../nested'

export const internetFields = gql`
  #=> "Kennith_Runte@gmail.com"
  internetEmail: String 
  #=> "Sincere.MacGyver37@example.com"
  exampleEmail: String 
  #=> "Javier.Block"
  userName: String 
  #=> "http"
  protocol: String
  #=> "http://elvera.biz"
  url: String 
  #=> "beatrice.name"
  domainName: String
  #=> "name"
  domainSuffix: String 
  #=> "jaylon"
  domainWord: String 
  #=> "83.153.195.189"
  ip: String 
  #=> "2dfe:e2c9:811c:6207:12b6:e226:f9a4:cfae"
  ipv6: String 
  #=> "Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 5.0; Trident/5.1)"
  userAgent: String
  #=> "04:e7:9e:85:af:e2"
  mac: String 
  #=> "FzWVa3Mn9IHXM0J"
  password: String
`

export const FakeInternetType = gql`
  type FakeInternet {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    ${internetFields}
    ${nested}
  }
`

export const FakeInternetListType = gql`
  type FakeInternetList {
    #=> '2f4dc6ba-bd25-4e66-b369-43a13e0cf150'
    id: String
    #=> 1
    idx: Int
    #=> "internet_1"
    idxName: String
    #=> "custom_1"
    idxCustom: String
    ${internetFields}
    ${nested}
  }
`

export const FakeInternetQuery = gql`
  FakeInternet (locale: String): FakeInternet
  FakeInternetList  (limit: Int, locale: String): [FakeInternetList]
`
