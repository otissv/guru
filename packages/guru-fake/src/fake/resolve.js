import faker from 'faker'
import times from 'lodash/fp/times'
import { getInfoSelection } from 'guru-utils'

export function generateResolver({ parent, args, context, info, mockFn }) {
  const _args = parent || args
  const selection = getInfoSelection(info)

  return {
    id: () => faker.random.uuid(),
    ...mockFn({ parent: null, args: _args, context, info, selection }),
  }
}

export function generateListResolver({ parent, args, context, info, mockFn }) {
  const obj = parent || args
  let _args = {
    ...obj,
    limit: !obj.limit ? 5 : obj.limit > 100 ? 100 : obj.limit,
  }

  const selection = getInfoSelection(info)

  return times(i => ({
    idx: i,
    idxName: `fake_${i}`,
    idxCustom: _args.prefix ? `${_args.prefix}_${i}` : null,
    id: faker.random.uuid(),
    ...mockFn({ parent: null, args: _args, context, info, selection }),
  }))(_args.limit)
}

export function getResolveFields({ parent, args, context, info, selection }) {
  const resolveFieldsReducer = (acc, key) => {
    if (key.substr(0, 4) !== 'Fake') {
      return acc
    }

    const _args = selection.fields[key].arguments
    const resolver = context.resolvers.Query[key]

    const _info = {
      fieldName: key,
      fieldNodes: [
        info.fieldNodes[0].selectionSet.selections.find(
          s => s.name.value === key
        ),
      ],
      variableValues: {},
    }

    return {
      ...acc,
      [key]: resolver(parent, _args, context, _info),
    }
  }

  const initialValue = {
    // String: (parent, _args, context, _info) => {
    //   console.log(JSON.stringify(selection, null, 2))
    // },
  }

  return selection
    ? Object.keys(selection.fields).reduce(resolveFieldsReducer, initialValue)
    : {}
}

export function queryResolver({ resolverName, mockFn }) {
  return {
    [`Fake${resolverName}`]: (parent, args, context, info) => {
      return generateResolver({
        parent,
        args,
        context,
        info,
        mockFn,
      })
    },

    [`Fake${resolverName}List`]: (parent, args, context, info) => {
      return generateListResolver({
        parent,
        args,
        context,
        info,
        mockFn,
      })
    },
  }
}
