import moment from 'moment'

export const gql = (arr, ...args) =>
  [arr[0], ...args, ...arr.slice(1, arr.length)].join('')

export function randomNumber({ min, max, interval = 1 }) {
  let r = Math.floor((Math.random() * (max - min + interval)) / interval)
  return r * interval + min
}

export function getDates() {
  const startDate = moment(new Date())
    .month(randomNumber({ min: 0, max: 11 }))
    .hour(randomNumber({ min: 0, max: 23 }))
    .minute(randomNumber({ min: 0, max: 59 }))

  const endDate = moment(new Date())
    .month(randomNumber({ min: startDate.month(), max: 11 }))
    .hour(randomNumber({ min: 0, max: 23 }))
    .minute(randomNumber({ min: 0, max: 59 }))

  return {
    startDate: startDate.toISOString(),
    endDate: endDate.toISOString(),
  }
}

export function resolve({ connector, context, method, query }) {
  return context.connectors[connector][method]({
    args: query.createdBy,
    ...context,
  })
}
