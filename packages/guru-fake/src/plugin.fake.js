import { resolvers } from './resolvers.fake'
import { typeDefs } from './typeDefs.fake'

import pkg from '../package.json'

export class FakePlugin {
  namespace = 'fake'
  package = pkg.name
  type = 'FakePlugin'

  resolvers() {
    return resolvers
  }

  typeDefs() {
    return { types: typeDefs }
  }
}
