import { FakeListType, FakeType } from './fake/fake/typeDefs.fake'
import { FakeQuery } from './fake/fake/query.fake'
import {
  FakeAddressListType,
  FakeAddressType,
} from './fake/address/typeDefs.address'
import { FakeAddressQuery } from './fake/address/query.address'
import { FakeCardListType, FakeCardType } from './fake/card/typeDefs.card'
import { FakeCardQuery } from './fake/card/query.card'
import {
  FakeCardTransactionListType,
  FakeCardTransactionType,
} from './fake/custom/cardTransaction/typeDefs.cardTransaction'
import { FakeCardTransactionQuery } from './fake/custom/cardTransaction/query.cardTransaction'
import { FakeColorListType, FakeColorType } from './fake/color/typeDefs.color'
import { FakeColorQuery } from './fake/color/query.color'
import {
  FakeCommerceListType,
  FakeCommerceType,
} from './fake/commerce/typeDefs.commerce'
import { FakeCommerceQuery } from './fake/commerce/query.commerce'
import {
  FakeCompanyListType,
  FakeCompanyType,
} from './fake/company/typeDefs.company'
import { FakeCompanyQuery } from './fake/company/query.company'
import {
  FakeCurrencyListType,
  FakeCurrencyType,
} from './fake/currency/typeDefs.currency'

import { FakeCurrencyQuery } from './fake/currency/query.currency'
import {
  FakeDatabaseListType,
  FakeDatabaseType,
} from './fake/database/typeDefs.database'
import { FakeDatabaseQuery } from './fake/database/query.database'
import {
  FakeDateTimeListType,
  FakeDateTimeType,
} from './fake/dateTime/typeDefs.dateTime'
import { FakeDateTimeQuery } from './fake/dateTime/query.dateTime'
import {
  FakeFinanceListType,
  FakeFinanceType,
} from './fake/finance/typeDefs.finance'
import { FakeFinanceQuery } from './fake/finance/query.finance'
import {
  FakeHackerListType,
  FakeHackerType,
} from './fake/hacker/typeDefs.hacker'
import { FakeHackerQuery } from './fake/hacker/query.hacker'
import { FakeImageListType, FakeImageType } from './fake/image/typeDefs.image'
import { FakeImageQuery } from './fake/image/query.image'
import {
  FakeInternetListType,
  FakeInternetType,
} from './fake/internet/typeDefs.internet'
import { FakeInternetQuery } from './fake/internet/query.internet'
import { FakeLoremListType, FakeLoremType } from './fake/lorem/typeDefs.lorem'
import { FakeLoremQuery } from './fake/lorem/query.lorem'
import {
  FakeMeetingListType,
  FakeMeetingType,
} from './fake/custom/meeting/typeDefs.meeting'
import { FakeMeetingQuery } from './fake/custom/meeting/query.meeting'
import {
  FakeMessageListType,
  FakeMessageType,
} from './fake/custom/message/typeDefs.message'
import { FakeMessageQuery } from './fake/custom/message/query.message'
import {
  FakeMetaListType,
  FakeMetaType,
} from './fake/custom/meta/typeDefs.meta'
import { FakeMetaQuery } from './fake/custom/meta/query.meta'
import { FakeMiscListType, FakeMiscType } from './fake/misc/typeDefs.misc'
import { FakeMiscQuery } from './fake/misc/query.misc'
import {
  FakeNumberListType,
  FakeNumberType,
} from './fake/number/typeDefs.number'
import { FakeNumberQuery } from './fake/number/query.number'
import {
  FakePersonListType,
  FakePersonType,
} from './fake/person/typeDefs.person'
import { FakePersonQuery } from './fake/person/query.person'
import { FakePhoneListType, FakePhoneType } from './fake/phone/typeDefs.phone'
import { FakePhoneQuery } from './fake/phone/query.phone'
import {
  FakeSystemListType,
  FakeSystemType,
} from './fake/system/typeDefs.system'
import { FakeSystemQuery } from './fake/system/query.system'
import {
  FakeTransactionListType,
  FakeTransactionType,
} from './fake/custom/transaction/typeDefs.transaction'
import { FakeTransactionQuery } from './fake/custom/transaction/query.transaction'
import {
  FakeUserType,
  FakeUserListType,
} from './fake/custom/user/typeDefs.user'
import { FakeUserQuery } from './fake/custom/user/query.user'
import { FakeScalarQuery } from './fake/scalar/query.scalar'

export const schemaQueries = [
  FakeQuery,
  FakeAddressQuery,
  FakeCardQuery,
  FakeCardTransactionQuery,
  FakeColorQuery,
  FakeCommerceQuery,
  FakeCompanyQuery,
  FakeCurrencyQuery,
  FakeDatabaseQuery,
  FakeDateTimeQuery,
  FakeFinanceQuery,
  FakeHackerQuery,
  FakeImageQuery,
  FakeInternetQuery,
  FakeLoremQuery,
  FakeMeetingQuery,
  FakeMessageQuery,
  FakeMetaQuery,
  FakeMiscQuery,
  FakeNumberQuery,
  FakePersonQuery,
  FakePhoneQuery,
  FakeSystemQuery,
  FakeTransactionQuery,
  FakeUserQuery,
  FakeScalarQuery,
].join(`
`)

export const schemaTypeDefinitions = [
  FakeListType,
  FakeType,
  FakeAddressType,
  FakeAddressListType,
  FakeCardType,
  FakeCardListType,
  FakeCardTransactionListType,
  FakeCardTransactionType,
  FakeColorListType,
  FakeColorType,
  FakeCommerceListType,
  FakeCommerceType,
  FakeCompanyListType,
  FakeCompanyType,
  FakeCurrencyListType,
  FakeCurrencyType,
  FakeDatabaseListType,
  FakeDatabaseType,
  FakeDateTimeListType,
  FakeDateTimeType,
  FakeFinanceListType,
  FakeFinanceType,
  FakeHackerListType,
  FakeHackerType,
  FakeImageListType,
  FakeImageType,
  FakeInternetListType,
  FakeInternetType,
  FakeLoremListType,
  FakeLoremType,
  FakeMeetingListType,
  FakeMeetingType,
  FakeMessageListType,
  FakeMetaListType,
  FakeMetaType,
  FakeMessageType,
  FakeMiscListType,
  FakeMiscType,
  FakeNumberListType,
  FakeNumberType,
  FakePersonListType,
  FakePersonType,
  FakePhoneListType,
  FakePhoneType,
  FakeSystemListType,
  FakeSystemType,
  FakeTransactionListType,
  FakeTransactionType,
  FakeUserListType,
  FakeUserType,
].join(`
`)

export const typeDefs = `
${schemaTypeDefinitions}

type Query{
  ${schemaQueries}
}
`
