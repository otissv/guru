import { Kind } from 'graphql'
import chalk from 'chalk'
import gql from 'graphql-tag'

const isNode =
  Object.prototype.toString.call(
    typeof process !== 'undefined' ? process : 0
  ) === '[object process]'

// TODO: engine does not exist error. in validation

export function verifyConfig({ typeDefs, resolvers, schemaDirectives }) {
  const types = gql`
    ${typeDefs}
  `

  const resolversQuery = resolvers.Query || {}
  const resolversMutation = resolvers.Mutation || {}
  const _resolvers = {
    Query: Object.keys(resolversQuery).reduce(
      (previous, key) => [...previous, key],
      []
    ),
    Mutation: Object.keys(resolversMutation).reduce(
      (previous, key) => [...previous, key],
      []
    ),
  }

  const _schema = types.definitions.reduce(
    (previous, definition) => {
      const getFields = (fields, type) =>
        definition.fields.reduce(
          (previous, field) => [...previous, field.name.value],
          previous[type]
        )

      if (
        definition.kind === Kind.OBJECT_TYPE_DEFINITION &&
        (definition.name.value === 'Query' ||
          definition.name.value === 'Mutation')
      ) {
        const Query =
          definition.name.value === 'Query'
            ? getFields(definition.fields, 'Query')
            : previous.Query
        const Mutation =
          definition.name.value === 'Mutation'
            ? getFields(definition.fields, 'Mutation')
            : previous.Mutation
        return { Query, Mutation, Directives: previous.Directives }
      }

      if (definition.kind === Kind.OBJECT_TYPE_DEFINITION) {
        const Directives = definition.directives.reduce(
          (p, c) => [...p, c.name.value],
          []
        )

        return {
          Query: previous.Query,
          Mutation: previous.Mutation,
          Directives,
        }
      }

      return previous
    },
    { Query: [], Mutation: [], Directives: [] }
  )

  // check directive are in schema
  schemaDirectives

  Object.keys(schemaDirectives).forEach(directive => {
    const directiveErrors = []
    if (!_schema.Directives.includes(directive)) {
    }

    if (directiveErrors.length > 1) {
      console.log(
        `${chalk.red('\nThe following are in directives are not defined')}
${directiveErrors.join('\n')}
        `
      )

      if (isNode) process.exit(0)
    }
  })

  // Check schema operations are in resolver methods
  function resolversHasSchemaOperations() {
    let queryErrors = []
    let mutationErrors = []

    // Checks
    _schema.Query.forEach(
      operation =>
        !_resolvers.Query.includes(operation) && queryErrors.push(operation)
    )
    _schema.Mutation.forEach(
      operation =>
        !_resolvers.Mutation.includes(operation) &&
        mutationErrors.push(operation)
    )

    // Errors
    if (queryErrors.length > 1 || mutationErrors.length > 1) {
      if (queryErrors.length > 1) {
        console.log(`${chalk.red(
          '\nThe following are in schema Query but not in resolvers Query'
        )}
${queryErrors.join('\n')}`)
      }

      if (mutationErrors.length > 1) {
        // TODO: engine does not exist error. in validation

        console.log(`${chalk.red(
          '\nThe following are in schema Mutation but not in resolvers Mutation'
        )}
${mutationErrors.join('\n')}`)
      }

      if (isNode) process.exit(0)
    }
  }

  function schemaHasResolversOperations() {
    let queryErrors = []
    let mutationErrors = []

    //Checks
    _resolvers.Query.forEach(
      operation =>
        !_schema.Query.includes(operation) && queryErrors.push(operation)
    )
    _resolvers.Mutation.forEach(
      operation =>
        !_schema.Mutation.includes(operation) && mutationErrors.push(operation)
    )

    //Errors
    if (queryErrors.length > 1 || mutationErrors.length > 1) {
      if (queryErrors.length > 1) {
        console.log(`${chalk.red(
          '\nThe following are in resolvers Query but not in schema Query'
        )}
${queryErrors.join('\n')}`)
      }

      if (mutationErrors.length > 1) {
        console.log(`${chalk.red(
          '\nThe following are in resolvers Mutation but not in schema Mutation'
        )}
${mutationErrors.join('\n')}`)
      }

      if (isNode) process.exit(0)
    }
  }

  resolversHasSchemaOperations()
  schemaHasResolversOperations()

  //TODO:
  // 1. Object Type is in resolver but not in Schema
  // 2. check resolve directives is are i schema operations
  // 3 check nested directive is boolean
  // 4. check engine directive have correct correct fields and engine name
}
