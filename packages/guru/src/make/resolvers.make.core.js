import { makeTypeResolve } from './typeResolve.make.core'
import isEmpty from 'lodash/isEmpty'
import merge from 'lodash/merge'

function makeOperation(context) {
  return (obj, type) => {
    const keys = Object.keys(obj)

    return keys.length > 0
      ? keys.reduce(
          (previous, key) => ({
            ...previous,
            [key]: function(root, args, parentContext, info) {
              return obj[key](
                root,
                args,
                { ...context, ...parentContext },
                info
              )
            },
          }),
          {}
        )
      : {}
  }
}

export async function makeResolvers({ context, resolvers, typeDefs }) {
  try {
    const operations = makeOperation(context)
    const typeResolve = makeTypeResolve(typeDefs)

    const builtResolvers = await Object.keys(resolvers).reduce(
      async (acc, key) => {
        try {
          const previous = await acc
          const current = resolvers[key]
          const query = (current.Query && current.Query) || {}
          const mutation = (current.Mutation && current.Mutation) || {}

          const methods =
            typeof current === 'function'
              ? await current({
                  context,
                  typeDefs,
                  namespace: key,
                })
              : {}

          const queryResult = {
            ...previous.Query,
            ...methods.Query,
            ...operations(query, 'Query'),
          }

          const mutationResult = {
            ...previous.Mutation,
            ...methods.Mutation,
            ...operations(mutation, 'Mutation'),
          }

          return {
            ...previous,
            ...current,
            ...(isEmpty(queryResult) ? {} : { Query: queryResult }),
            ...(isEmpty(mutationResult) ? {} : { Mutation: mutationResult }),
          }
        } catch (error) {
          console.error(error)
          return error
        }
      },
      {}
    )

    return merge(typeResolve, builtResolvers)
  } catch (error) {
    console.error(error)
    return error
  }
}
