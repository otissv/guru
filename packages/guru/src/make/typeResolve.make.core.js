import camelCase from 'lodash/fp/camelCase'
import find from 'lodash/fp/find'
import gql from 'graphql-tag'
import { notObjectTypeDef } from 'guru-utils'
import chalk from 'chalk'

function getObjectNames(typeDefs) {
  const types =
    typeDefs === 'string'
      ? gql`
          ${typeDefs}
        `
      : typeDefs

  return types.definitions.reduce((previous, type) => {
    if (notObjectTypeDef(type)) return previous
    return [...previous, type.name.value]
  }, [])
}

function getFieldKind({ field, objectNames }) {
  return field.type.kind === 'ListType'
    ? {
        kind: 'ListType',
        name: field.type.type.name.value,
        value: field.name.value,
      }
    : objectNames.includes(field.type.name.value)
    ? {
        kind: 'NamedType',
        name: field.type.name.value,
        value: field.name.value,
      }
    : {
        kind: 'NamedType',
        name: field.type.name.value,
        value: field.name.value,
      }
}

function getFieldDirectives(field) {
  return field.directives.reduce(
    (previous, directive) => ({
      ...previous,
      [directive.name.value]: directive.arguments[0].value.value,
    }),
    {}
  )
}

function getTypeRelationships(typeDefs, callback) {
  const types = gql`
    ${typeDefs}
  `
  const objectNames = getObjectNames(types)

  return types.definitions.reduce((previous, type) => {
    if (notObjectTypeDef(type)) return previous

    const fields = type.fields.reduce((prev, field) => {
      const fieldKind = getFieldKind({ field, objectNames })

      const notType = !find(name => name === fieldKind.name)(objectNames)

      if (notType) return prev

      const { nested, resolve } = getFieldDirectives(field)

      return (
        callback &&
        callback({ previous: prev, kind: fieldKind, nested, resolve, type })
      )
    }, {})

    return { ...previous, ...fields }
  }, {})
}

function relationships(typeDefs) {
  return getTypeRelationships(typeDefs, ({ previous, kind, type }) => {
    const objList = previous[type.name.value]
      ? [...previous[type.name.value]]
      : []

    objList.push(kind)

    return {
      ...previous,
      [type.name.value]: objList,
    }
  })
}

export function makeTypeResolve(typeDefs) {
  const callback = ({ previous, kind, nested, resolve, type }) => {
    const obj = previous[type.name.value]
      ? { ...previous[type.name.value] }
      : {}

    return {
      ...previous,
      [type.name.value]: {
        ...obj,
        [kind.value]: (obj, args, context, info) => {
          const rootArg = obj[kind.value]
          // if nested return self
          if (nested) {
            let tmp
            if (Array.isArray(rootArg)) {
              tmp = [...rootArg].reduce((previous, current) => {
                if (current._id) {
                  current.id = current._id
                  delete current._id
                }

                return [...previous, current]
              }, [])
            } else {
              tmp = { ...rootArg }
              if (tmp._id) {
                tmp.id = rootArg._id
                delete tmp._id
              }
            }

            return tmp || rootArg
          }

          // redirect to resolve function
          const operationResolveName = `${camelCase(kind.name)}Resolve`

          // check is user defined resolve function else use generated
          const resolver = resolve ? resolve : operationResolveName
          const _args = { query: rootArg }

          // if (typeof context.resolvers.Query[resolver] !== 'function') {
          //   console.log(
          //     chalk.red(`Resolve Error, ${resolver} is not a function.`)
          //   )
          // }

          return context.resolvers.Query[resolver]
            ? context.resolvers.Query[resolver](obj, _args, context, info)
            : rootArg
        },
      },
    }
  }

  return getTypeRelationships(typeDefs, callback)
}
