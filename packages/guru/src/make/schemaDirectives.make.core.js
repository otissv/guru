import { extendSchemaDirectives } from '../extend/extendSchemaDirectives.core'
import merge from 'lodash/fp/merge'

export function makeSchemaDirectives(schemaDirectives = {}) {
  return merge(schemaDirectives)(extendSchemaDirectives)
}
