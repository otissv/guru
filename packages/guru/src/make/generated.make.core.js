export function makeGeneratedTypes({ typeDefs, plugins, nonInputTypes }) {
  let memorize = []

  return Object.keys(plugins.typeDefs).reduce(async (previous, key) => {
    try {
      const pluginType = plugins[key].type
      let pluginsTypeDefs = await plugins.typeDefs[key]({
        typeDefs,
        namespace: key,
        nonInputTypes,
      })

      if (!memorize.includes(pluginType)) {
        memorize.push(pluginType)
      } else {
        pluginsTypeDefs.inputs = ''
      }

      const inputs = pluginsTypeDefs.inputs ? pluginsTypeDefs.inputs.trim() : ''
      const types = pluginsTypeDefs.types ? pluginsTypeDefs.types.trim() : ''

      return `${await previous}

${inputs}

${types}
    `
    } catch (error) {
      console.log(error)
      return ''
    }
  }, '')
}
