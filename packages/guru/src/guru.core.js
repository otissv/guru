import { extendTypes, nonInputTypes } from './extend/extendedTypes.core'

import { extendResolvers } from './extend/extendResolvers.core'
import { makeGeneratedTypes } from './make/generated.make.core'
import { makeResolvers } from './make/resolvers.make.core'
import { makeSchemaDirectives } from './make/schemaDirectives.make.core'
import { makeTypes } from './make/types.make.core'
import { maybe } from 'guru-utils'
import merge from 'lodash/fp/merge'
import { verifyConfig } from './verifyConfig.core'

export async function guru(
  { typeDefs, resolvers, context, schemaDirectives, ...graphql },
  plugins = { resolvers: {}, typeDefs: {} }
) {
  try {
    let _types = []
    let _typeFNs = []
    let _sharedTypes = []
    let _typeDefs = []

    if (typeDefs) {
      if (Array.isArray(typeDefs)) {
        typeDefs.forEach(typeDef => {
          const maybeType = maybe()

          typeof typeDef === 'function'
            ? maybeType(typeDef) && _typeFNs.push(typeDef)
            : maybeType(typeDef) && _types.push(typeDef)
        })
      } else {
        if (typeof typeDefs === 'function') {
          _typeFNs = typeDefs
        } else {
          _types.push(typeDefs)
        }
      }
    }

    const generated = await makeGeneratedTypes({
      typeDefs: _types,
      plugins: plugins,
      nonInputTypes,
    })

    _typeDefs = await makeTypes(
      [
        extendTypes,
        ..._typeFNs.map(fn => fn(_types).types),
        ...[generated, ..._types],
      ].filter(types => types !== '')
    )

    const resolverContext = merge(plugins)(context || {})

    const _resolvers = await makeResolvers({
      context: resolverContext,
      resolvers: {
        ...plugins.resolvers,
        ...resolvers,
      },
      typeDefs: await makeTypes([_typeDefs]),
    })

    const config = {
      typeDefs: _typeDefs,
      resolvers: merge(extendResolvers)(_resolvers || {}),
      schemaDirectives: makeSchemaDirectives(schemaDirectives),
    }

    verifyConfig(config)

    const _context = { ...resolverContext }
    delete _context.resolvers
    delete _context.typeDefs
    delete _context.middlewareModules
    delete _context.resolverModules
    delete _context.routeModules
    delete _context.typeDefModules
    delete _context.typeDefJsModules

    return {
      ...graphql,
      ...config,
      context: _context,
    }

    return {}
  } catch (error) {
    throw error
  }
}
