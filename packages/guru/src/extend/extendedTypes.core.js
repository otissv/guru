export const nonInputTypes = [
  'COUNT',
  'ERROR',
  'META',
  'Mutation',
  'NODE',
  'PING',
  'Query',
  'RESULTS',
  'Subscription',
]

export const extendTypes = `
directive @engine(namespace: String, collection: String, resolver: Boolean, input: Boolean, typeDef: Boolean) on  OBJECT
directive @nested(value: Boolean) on FIELD_DEFINITION 
directive @auth(username: String, password: String, provider: String) on QUERY 

scalar DateTime
scalar Email
scalar JSON
scalar LimitedString
scalar Password
scalar RegExp
scalar URL
scalar UUID

type COUNT {
  n: Int
}

type Engine {
  # Engine namespace
  name: String
  # Engine statistics
  stats: EngineStats
  collections (collections: [String]): [EngineCollection]
}

type EngineCollection {
  # Collection name.
  name: String
  # List of IDs of the collections Documents.
  ids: [String]
  # JSON containing the document data.
  data (query: JSON, projection: JSON, options: JSON): JSON
  # Collections indexes.
  indexes: JSON
  # Number of documents in collection
  count: Int
  RESULTS: [RESULTS]
}


# Engine statistics
type EngineStats {
  # Contains the name of the database.
  db: String
  # Contains a count of the number of collections in that database.
  collections: Int
  # 
  views: Int
  # Contains a count of the number of objects (i.e. documents) in the database across all collections.
  objects: Float
  # The average size of each document in bytes. This is the dataSize divided by the number of documents. The scale argument does not affect the avgObjSize value.
  avgObjSize: Float
  # The total size of the uncompressed data held in this database. The dataSize decreases when you remove documents.
  dataSize: Float
  # The total amount of space allocated to collections in this database for document storage. The storageSize does not decrease as you remove or shrink documents. This value may be smaller than dataSize for databases using the WiredTiger storage engine with compression enabled.
  storageSize: Float
  # Contains a count of the number of extents in the database across all collections.
  numExtents: Float
  # Contains a count of the total number of indexes across all collections in the database.
  indexes: Float
  # The total size of all indexes created on this database.
  indexSize: Float
  # Total size of all disk space in use on the filesystem where MongoDB stores data.
  fsUsedSize: Float
  # Total size of all disk space in use on the filesystem where MongoDB stores data.e value of fileSize only reflects the size of the data files for the database and not the namespace file.
  fsTotalSize: Float
  # 
  ok: Int
}

type ERROR {
  type:    String
  message: String
}

type META {
  createdAt: String
  createdBy: DateTime
  updatedAt: String
  updatedBy: DateTime
}

type NODE {
  id: String
  COUNT: COUNT
  RESULTS: RESULTS
  ERROR: ERROR
  META: META
}

type PING {
  # Returns the status of the server
  status: String
}


type Query {
  # Checks the endpoint is working
  PING: PING
  engines (name: String, collections: [String]): [Engine]
}

type RESULTS {
  id:     String
  module: String
  result: String
  error:  ERROR
  n:      Int
}

enum ACTIVE_STATUS {
  ACTIVE
  DEACTIVATED
}
`
