import { DefaultDirective } from '../directives/default.directive'
import { FieldDirective } from '../directives/field.directive'

import { SchemaDirectiveVisitor } from 'graphql-tools'

export class ObjectDirective extends SchemaDirectiveVisitor {
  visitObject(field) {
    return field
  }
}

//

export const extendSchemaDirectives = {
  // engine: ObjectDirective,
  // default: DefaultDirective,
  // // resolve: schema operation,
  // nested: FieldDirective,
  // auth: [user:read, user:update, user:create:, user:delete, user:all]
}
