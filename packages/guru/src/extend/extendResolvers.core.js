import {
  GraphQLDateTime,
  GraphQLEmail,
  GraphQLLimitedString,
  GraphQLPassword,
  GraphQLURL,
  GraphQLUUID,
} from 'graphql-custom-types'

import GraphQLJSON from 'graphql-type-json'
import GraphQLRegExp from './GraphQLRegExp'

export const extendResolvers = {
  DateTime: GraphQLDateTime,
  Email: GraphQLEmail,
  JSON: {
    serialize(value) {
      return GraphQLJSON.parseValue(value)
    },
    parseValue(value) {
      return GraphQLJSON.parseValue(value)
    },
    parseLiteral(ast) {
      return GraphQLJSON.parseLiteral(ast)
    },
  },
  LimitedString: GraphQLLimitedString,
  Password: GraphQLPassword,
  Query: {
    engines: (parent, args, context, info) => {
      return args.name
        ? context.engines.filter(e => e === args.name)[0] && [
            { name: args.name },
          ]
        : context.engines.map(engine => ({ name: engine }))
    },
    PING: () => ({ status: 'OK' }),
  },
  RegExp: GraphQLRegExp,
  URL: GraphQLURL,
  UUID: GraphQLUUID,
}
