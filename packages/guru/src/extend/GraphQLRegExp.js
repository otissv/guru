import { GraphQLScalarType } from 'graphql'
import { Kind } from 'graphql/language'

function identity(value) {
  return value
}

function parseLiteral(ast) {
  switch (ast.kind) {
    case Kind.STRING:
      return new RegExp(ast.value)
    default:
      throw new Error(`Expected string, got ${ast.kind}`)
  }
}

const RegExp = new GraphQLScalarType({
  name: 'RegExp',
  description: 'JS RegExp represented as string',
  serialize: identity,
  parseValue: identity,
  parseLiteral,
})

export default RegExp
