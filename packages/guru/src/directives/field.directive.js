import { SchemaDirectiveVisitor } from 'graphql-tools'

export class FieldDirective extends SchemaDirectiveVisitor {
  visitFieldDefinition(field) {
    return field
  }
}
