import { GraphQLNonNull, GraphQLScalarType } from 'graphql'

import { SchemaDirectiveVisitor } from 'graphql-tools'

class ParseType extends GraphQLScalarType {
  constructor(type, args) {
    super({
      name: `DefaultValue`,

      // For more information about GraphQLScalar type (de)serialization,
      // see the graphql-js implementation:
      // https://github.com/graphql/graphql-js/blob/31ae8a8e8312/src/type/definition.js#L425-L446

      // Serializes an internal value to include in a response.
      serialize(value) {
        return value
      },

      // Parses an externally provided value to use as an input.
      parseValue(value) {
        return type.parseValue()
      },

      // Parses an externally provided literal value to use as an input.
      parseLiteral(ast) {
        return type.parseLiteral(ast)
      },
    })
  }
}

export class DefaultDirective extends SchemaDirectiveVisitor {
  visitInputFieldDefinition(field) {
    this.wrapType(field)
  }

  visitFieldDefinition(field) {
    this.wrapType(field)
  }

  // Replace field.type with a custom GraphQLScalarType that enforces the default

  wrapType(field) {
    if (
      field.type instanceof GraphQLNonNull &&
      field.type.ofType instanceof GraphQLScalarType
    ) {
      field.type = new GraphQLNonNull(
        new ParseType(field.type.ofType, this.args.value)
      )
    } else if (field.type instanceof GraphQLScalarType) {
      field.type = new ParseType(field.type, this.args)
    } else {
      throw new Error(`Not a scalar type: ${field.type}`)
    }
  }
}
