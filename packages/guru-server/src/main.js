import bodyParser from 'body-parser'
import express from 'express'
import { ApolloServer, gql } from 'apollo-server-express'
import { getMappedPlugins, maybeArray } from 'guru-utils'
import { guru } from 'guru'
import { makeExecutableSchema } from 'graphql-tools'
import { parse } from 'graphql'

function graphqlValidateQuery(req, res, next, typeDefs) {
  next()
}

const dev = process.env.NODE_ENV !== 'prod'

export function getResolvers({ app, config, plugins, resolvers }) {
  return resolvers.map(i => {
    if (typeof i.resolver === 'function')
      return i.resolver({ app, config, plugins })

    if (i.resolver) return i.resolver

    if (typeof i.default.resolver === 'function')
      return i.default.resolver({ app, config, plugins })
    if (i.default.resolver) return i.default.resolver
  })
}

async function pluginModules(mappedPlugins) {
  try {
    return {
      typeDefModules: await mappedPlugins.typeDefModules.reduce(
        async (previous, current) => previous + '\n' + (await current),
        ''
      ),
      middlewareModules: await mappedPlugins.middlewareModules.reduce(
        async (previous, current) => {
          const previousMiddleware = await previous
          return [...previousMiddleware].concat(await current)
        },
        []
      ),
      routeModules: await mappedPlugins.routeModules.reduce(
        async (previous, current) => {
          const previousRoutes = await previous
          return [...previousRoutes].concat(await current)
        },
        []
      ),
      resolverModules: await mappedPlugins.resolverModules.reduce(
        async (previous, current) => [...previous].concat(await current),
        []
      ),
      typeDefJsModules: await mappedPlugins.typeDefJsModules.reduce(
        async (previous, current) => [...previous].concat(await current),
        []
      ),
    }
  } catch (error) {
    throw error
  }
}

export async function main(config) {
  try {
    const app = express()

    const { graphql, middleware, routes, plugins } = config

    const mappedPlugins = getMappedPlugins(plugins)
    const {
      middlewareModules,
      resolverModules,
      routeModules,
      typeDefModules,
      typeDefJsModules,
    } = await pluginModules(mappedPlugins)

    app.use(bodyParser.raw({ extended: false }))
    app.use(bodyParser.text({ defaultCharset: 'utf-8' }))
    app.use(bodyParser.urlencoded({ extended: false }))
    app.use(bodyParser.json())

    // Config middleware
    if (middleware && typeof middleware !== 'function') {
      throw new Error('config.middleware must be a function')
    } else if (middleware) {
      middleware({ app, config, plugins: mappedPlugins })
    }

    // Load middleware modules
    middlewareModules &&
      middlewareModules.forEach(({ middleware }) =>
        middleware({ app, config, plugins: mappedPlugins })
      )

    // Load routes modules
    routeModules.forEach(({ routes }) =>
      routes({ app, config, plugins: mappedPlugins })
    )

    // // Config routes
    // if (!routes && typeof routes !== 'function') {
    //   throw new Error('config.routes must be a function')
    // } else if (routes) {
    //   routes({ app, config, plugins: mappedPlugins })
    // }

    const _resolvers = getResolvers({
      app,
      config,
      plugins,
      resolvers: resolverModules,
    })

    const _typeDefJsModules = typeDefJsModules.reduce(
      (previous, current) => `${previous}
    ${current.typeDef({ app, config, plugins: mappedPlugins })}
    `,
      ''
    )

    app.disable('x-powered-by').use('/ping', (req, res) => {
      res.send('Server online')
    })

    if (!Array.isArray(graphql)) {
      throw 'config.routes must be an array'
    } else {
      graphql.forEach(async item => {
        try {
          const {
            context,
            options,
            resolvers,
            typeDefs,
            schemaDirectives,
          } = await guru(
            {
              ...item.config,
              typeDefs: maybeArray(item.config.typeDefs, [])((isArray, array) =>
                isArray
                  ? [_typeDefJsModules, typeDefModules, ...array]
                  : item.config.typeDefs
                  ? [_typeDefJsModules, typeDefModules, item.config.typeDefs]
                  : [_typeDefJsModules, typeDefModules]
              ),

              resolvers: Array.isArray(item.config.resolvers)
                ? [..._resolvers, ...item.config.resolvers]
                : item.config.resolvers
                ? [..._resolvers, item.config.resolvers]
                : _resolvers,
            },
            mappedPlugins
          )

          app.get(`${item.route}/schema`, (req, res) =>
            res.json({ schema: typeDefs })
          )
          app.get(`${item.route}/schema/tag`, (req, res) =>
            res.json({
              schema: gql`
                ${typeDefs}
              `,
            })
          )

          app.get(`${item.route}/resolvers`, (req, res) => {
            function mapResolvers(resolvers) {
              return Object.keys(resolvers).reduce(
                (previous, key) => ({
                  ...previous,
                  [key]:
                    typeof resolvers[key] === 'string'
                      ? resolvers[key]
                      : Object.keys(resolvers[key]).reduce(
                          (p, k) => [...p, k],
                          []
                        ),
                }),
                {}
              )
            }

            return res.json({ resolvers: mapResolvers(resolvers) })
          })

          // app.get(`${item.route}`, (req, res, next) => {
          //   res
          //     .status(404)
          //     .json({ code: 'NOT_FOUND', message: 'Endpoint not Found' })
          // })

          app.use(item.route, (req, res, next) => {
            return graphqlValidateQuery(req, res, next, typeDefs)
          })
          const server = new ApolloServer(
            {
              schema: makeExecutableSchema({
                resolvers,
                typeDefs: [typeDefs],
                schemaDirectives,
              }),
              context: function(args) {
                return {
                  ...args.context,
                  ...context,
                  ...(typeof item.config.context === 'function'
                    ? item.config.context({
                        ...args,
                        context: { ...args.context, ...context },
                      })
                    : item.config.context),
                  typeDefs,
                  resolvers,
                }
              },
              ...options,
            },
            {}
          )

          const routeMiddleware = item.middleware
            ? item.middleware
            : [(req, res, next) => next()]

          app.use(item.route, ...routeMiddleware)

          server.applyMiddleware({ app, path: item.route })

          app.use('*', (req, res) => {
            res
              .status(404)
              .json({ code: 'NOT_FOUND', message: 'Endpoint not Found' })
          })
        } catch (error) {
          throw error
        }
      })
    }

    return app
  } catch (error) {
    throw error
  }
}
