import http from 'http'
import os from 'os'
import dotenv from 'dotenv'

import { main } from './main'

dotenv.config({ path: process.cwd() })
const getEnv = c => process.env[c]

const dev = getEnv('NODE_ENV') === 'dev' || getEnv('NODE_ENV') === 'development'

const PORT = getEnv('PORT') || getEnv('PORT') || 9200
const HOST = getEnv('HOST') || 'localhost'

function serverStartedMessage() {
  const host = dev ? `http://${HOST}:${PORT}` : HOST

  // Network Address
  let externalAddress
  const networkInterfaces = os.networkInterfaces()

  if (networkInterfaces.en0 === true) {
    externalAddress = networkInterfaces.en0[0].address
  } else if (networkInterfaces.wlp4s0) {
    externalAddress = networkInterfaces.wlp4s0[0].address
  }

  const env = dev ? 'development' : 'production'

  return process.stdout.write(`
Guru Express server in ${env} mode.
  - IDE address           = ${host}
  - GraphQL endpoint:     = ${host}/graphql
${
  externalAddress
    ? '  - External IDE address  = ' + 'http://' + externalAddress + ':' + PORT
    : ''
}
${
  externalAddress
    ? '  - External IDE endpoint = ' +
      'http://' +
      externalAddress +
      ':' +
      PORT +
      '/graphql'
    : ''
}

`)
}

export async function guruServer(config = {}) {
  let app = await main({ ...config })
  const server = http.createServer(app)
  let currentApp = app

  return {
    app,
    start: async ({ port } = {}) => {
      server.listen(PORT, error => {
        if (error) {
          console.log(error)
        }

        serverStartedMessage()
      })

      try {
        app = await main({ ...config })
        server.removeListener('request', currentApp)
        server.on('request', app)
        currentApp = app
      } catch (error) {
        console.error(error)
      }
    },
  }
}
