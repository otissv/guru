import { globPathsAsync, globPluginsPathsAsync } from './utils.modules'

export async function resolverModules(plugins) {
  try {
    const configPath = await globPathsAsync({
      fileName: 'modules/**/*.resolver.js',
    })

    const pluginsPath = await globPluginsPathsAsync({
      fileName: '*.resolver.js',
      plugins: plugins || [],
    })

    return [...pluginsPath, ...configPath].map(path => require(path))
  } catch (error) {
    console.error(error)
  }
}
