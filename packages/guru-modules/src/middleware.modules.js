import { globPathsAsync, globPluginsPathsAsync } from './utils.modules'

export async function middlewareModules(plugins) {
  try {
    const configPath = await globPathsAsync({
      fileName: 'modules/**/*middleware.js',
    })

    const pluginsPath = await globPluginsPathsAsync({
      fileName: '*middleware.js',
      plugins: plugins || [],
    })

    return [...pluginsPath, ...configPath].map(path => require(path))
  } catch (error) {
    console.error(error)
  }
}
