import { middlewareModules } from './middleware.modules'
import pkg from '../package.json'
import { resolverModules } from './resolver.modules'
import { routeModules } from './route.modules'
import { typeDefJsModules } from './typeDefJs.modules'
import { typeDefModules } from './typeDef.modules'

export class ModulesPlugin {
  plugins
  package = pkg.name
  namespace = 'GURU_MODULES'

  middlewareModules(plugins) {
    this.plugins = this.plugins || plugins
    return middlewareModules(this.plugins).catch(error => {
      throw error
    })
  }

  resolverModules(plugins) {
    this.plugins = this.config || plugins
    return resolverModules(this.plugins).catch(error => {
      throw error
    })
  }

  routeModules(plugins) {
    this.plugins = this.plugins || plugins

    return routeModules(this.plugins).catch(error => {
      throw error
    })
  }

  typeDefModules(plugins) {
    this.plugins = this.plugins || plugins
    return typeDefModules(this.plugins).catch(error => {
      throw error
    })
  }

  typeDefJsModules(plugins) {
    this.plugins = this.plugins || plugins
    return typeDefJsModules(this.plugins).catch(error => {
      throw error
    })
  }
}
