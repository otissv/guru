import { globPathsAsync, globPluginsPathsAsync } from './utils.modules'

export async function routeModules(plugins) {
  try {
    const configPath = await globPathsAsync({
      fileName: 'modules/**/*.routes.js',
    })

    const pluginsPath = await globPluginsPathsAsync({
      fileName: '*.routes.js',
      plugins: plugins || [],
    })

    return [...pluginsPath, ...configPath].map(path => require(path))
  } catch (error) {
    console.error(error)
  }
}
