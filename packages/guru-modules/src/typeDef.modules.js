import { globGQLModulePathsAsync, globPluginsAsync } from './utils.modules'

import fs from 'fs'

export async function typeDefModules(plugins) {
  try {
    const configPath = await globGQLModulePathsAsync()
    const pluginsPath = await globPluginsAsync({
      fileName: '*.graphql',
      plugins: plugins || [],
    })

    return [...pluginsPath, ...configPath].reduce((previous, current) => {
      const typeDef = fs.readFileSync(current, 'utf8')
      return `${previous}
    ${typeDef}`
    }, '')
  } catch (error) {
    console.error(error)
  }
}
