const serverDir = `${process.cwd()}/dist`

import glob from 'glob-promise'

export function globGQLModulePathsAsync() {
  return glob(`${serverDir}/modules/**/*.graphql`).catch(error =>
    process.stdout.write(error)
  )
}

export function globPathsAsync({ fileName }) {
  return glob(`${serverDir}/${fileName}`).catch(error =>
    process.stdout.write(error)
  )
}

export function globPluginsAsync({ ext, fileName, plugins }) {
  const _ext = ext ? `*.${ext}` : ''

  return plugins.reduce(async (previous, plugin) => {
    const _previous = await previous

    const resolvePlugin = require.resolve(plugin.package)
    const pluginPath = resolvePlugin.substr(0, resolvePlugin.lastIndexOf('/'))
    const paths = await glob(`${pluginPath}/**/${fileName}${_ext}`)

    try {
      return [..._previous, ...paths]
    } catch (error) {
      console.error(error)
    }
  }, Promise.resolve(''))
}

export function globPluginsPathsAsync({ fileName, plugins }) {
  return globPluginsAsync({ fileName, plugins })
}
