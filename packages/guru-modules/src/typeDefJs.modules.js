import { globPathsAsync, globPluginsAsync } from './utils.modules'

export async function typeDefJsModules(plugins) {
  try {
    const configPath = await globPathsAsync({
      fileName: 'modules/**/*.typeDef.js',
    })

    const pluginsPath = await globPluginsAsync({
      fileName: '*.typeDef.js',
      plugins: plugins || [],
    })

    return [...pluginsPath, ...configPath].reduce((previous, current) => {
      return [...previous, require(current)]
    }, [])
  } catch (error) {
    console.error(error)
  }
}
