import cors from 'cors'
import morgan from 'morgan'
import { guruServer } from 'guru-server'
import { FakePlugin } from 'guru-fake'

const config = {
  graphql: [
    {
      route: '/',
      config: {
        options: {
          introspection: true,
        },
      },
    },
  ],

  plugins: [new FakePlugin()],

  middleware: ({ app }) => {
    app.use(cors())
    app.use(morgan('tiny'))
  },
}

const server = guruServer(config)

server
  .then(({ start }) => start())
  .catch(error => {
    console.log(new Error(error))
    process.exit(0)
  })
