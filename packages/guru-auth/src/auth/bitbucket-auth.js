import { Strategy as BitbucketStrategy } from 'passport-bitbucket-oauth2'
import passport from 'passport'
import { loginError } from 'guru-utils'

import { createOrUpdateOauthUser, getEngine } from '../queries.auth'

export function middleware({ app, config, failureRedirect, options }) {
  const { db, namespace } = getEngine(plugins)

  passport.use(
    new BitbucketStrategy(options, async function(
      accessToken,
      refreshToken,
      profile,
      done
    ) {
      try {
        const user = await createOrUpdateOauthUser({
          db,
          namespace,
          profile,
          provider: 'bitbucket',
        })

        done(null, user)
      } catch (error) {
        console.error(error)
        const [errorMessage, errorCode] = loginError()

        done(null, {
          result: 'failed',
          error: {
            code: errorCode,
            message: errorMessage,
          },
        })
      }
    })
  )

  app.use(
    '/auth/bitbucket/callback',
    passport.authenticate('bitbucket', {
      failureRedirect: failureRedirect || '/auth/unauthorized',
      ...options,
    }),
    (req, res) => res.json(req.user)
  )

  app.use('/auth/bitbucket', passport.authenticate('bitbucket'))

  return app
}
