import { ExtractJwt, Strategy as JwtStrategy } from 'passport-jwt'
import {
  createLocalUser,
  findUser,
  getEngine,
  generateSecrets,
} from '../queries.auth'
import passport from 'passport'
import { loginError } from 'guru-utils'
import { getPublicAuthKeySync } from '../token.auth'
import { query } from 'winston'

export function middleware({ app, config, options, plugins }) {
  const publicKey = getPublicAuthKeySync()
  const opts = {
    ...options,
    secretOrKey: getPublicAuthKeySync(),
  }

  passport.use(
    new JwtStrategy(opts, function(payload, done) {
      return done(null, payload)
    })
  )

  function authorized(req, res, next) {
    const { db, namespace } = getEngine(plugins)

    passport.authenticate(
      'jwt',
      { session: false },
      async (_, payload, error) => {
        try {
          if (error || !payload) {
            const [errorMessage, errorCode] = loginError()

            return res.status(401).json({
              result: 'failed',
              error: {
                code: errorCode,
                message: errorMessage,
              },
            })
          }

          const { password, meta, ...user } = await findUser({
            db,
            namespace,
            query: { username: payload.username },
          })

          req.user = user
        } catch (error) {
          next(error)
        }
        next()
      }
    )(req, res, next)
  }

  app.use('/auth/authenticate', authorized, (req, res, next) => {
    res.json(req.user)
  })

  app.use('/auth/register', async (req, res) => {
    try {
      const { db, namespace } = getEngine(plugins)
      const { username, email, password, name, roles } = req.body
      const user = await createLocalUser({
        db,
        namespace,
        data: { username, email, password, name },
      })

      res.json(user)
    } catch (error) {
      console.error(error)
      const [errorMessage, errorCode] = loginError()

      res.status(401).json({
        result: 'failed',
        error: {
          code: errorCode,
          message: errorMessage,
        },
      })
    }
  })

  return app
}
