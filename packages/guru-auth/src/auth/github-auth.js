import { Strategy as GitHubStrategy } from 'passport-github2'
import passport from 'passport'
import { loginError } from 'guru-utils'

import { createOrUpdateOauthUser, getEngine } from '../queries.auth'

export function middleware({ app, config, failureRedirect, options, plugins }) {
  const { db, namespace } = getEngine(plugins)

  passport.use(
    new GitHubStrategy(options, async function(
      accessToken,
      refreshToken,
      profile,
      done
    ) {
      try {
        const user = await createOrUpdateOauthUser({
          db,
          namespace,
          profile,
          provider: 'github',
        })

        done(null, user)
      } catch (error) {
        console.error(error)
        const [errorMessage, errorCode] = loginError()

        done(null, {
          result: 'failed',
          error: {
            code: errorCode,
            message: errorMessage,
          },
        })
      }
    })
  )

  app.use(
    '/auth/github/callback',
    passport.authenticate('github', {
      failureRedirect: failureRedirect || '/auth/unauthorized',
      ...options,
    }),
    (req, res) => res.json(req.user)
  )

  app.use('/auth/github', passport.authenticate('github'))

  return app
}
