import { Strategy as LocalStrategy } from 'passport-local'
import { createLocalUser, getEngine, findUser } from '../queries.auth'
import passport from 'passport'
import { loginError } from 'guru-utils'
import { isValidateHash } from '../token.auth'

export function middleware({ app, config, options, plugins }) {
  const { db, namespace } = getEngine(plugins)

  passport.use(
    new LocalStrategy(async function(username, password, done) {
      if (!username || !password) return

      try {
        const { password: passwordHash, meta, ...user } = await findUser({
          db,
          namespace,
          query: { username },
        })

        if (isValidateHash(password, passwordHash)) {
          throw Error('Incorrect name or password')
        } else {
          done(null, user)
        }
      } catch (error) {
        console.error(error)
        const [errorMessage, errorCode] = loginError()

        done(null, {
          result: 'failed',
          error: {
            code: errorCode,
            message: errorMessage,
          },
        })
      }
    })
  )

  app.use(
    '/auth/signin',
    passport.authenticate('local', {
      failureRedirect: '/auth/unauthorized',
      ...options,
    }),
    (req, res) => res.json(req.user)
  )

  app.use('/auth/signup', async (req, res) => {
    try {
      const { db, namespace } = getEngine(plugins)
      const user = await createLocalUser({
        db,
        namespace,
        data: req.body,
      })

      res.json(user)
    } catch (error) {
      console.error(error)
      const [errorMessage, errorCode] = loginError()

      res.status(401).json({
        result: 'failed',
        error: {
          code: errorCode,
          message: errorMessage,
        },
      })
    }
  })

  return app
}
