import { Strategy as WindowsLiveStrategy } from 'passport-windowslive'
import passport from 'passport'
import { loginError } from 'guru-utils'

import { createOrUpdateOauthUser, getEngine } from '../queries.auth'

export function middleware({ app, config, failureRedirect, options }) {
  const { db, namespace } = getEngine(plugins)

  passport.use(
    new WindowsLiveStrategy(options, async function(
      accessToken,
      refreshToken,
      profile,
      done
    ) {
      try {
        const user = await createOrUpdateOauthUser({
          db,
          namespace,
          profile,
          provider: 'windowslive',
        })

        done(null, user)
      } catch (error) {
        console.log(error)
        done(null, {
          result: 'failed',
          error: {
            code: errorCode,
            message: errorMessage,
          },
        })
      }
    })
  )
  app.use(
    '/auth/windowslive/callback',
    passport.authenticate('windowslive', {
      failureRedirect: failureRedirect || '/auth/unauthorized',
      ...options,
    }),
    (req, res) => res.json(req.user)
  )
  app.use('/auth/windowslive', passport.authenticate('windowslive'))

  return app
}
