import passport from 'passport'
import { logout } from './logout.auth'

export function authInit({ app, config, plugins }) {
  passport.serializeUser(function(user, done) {
    done(null, user)
  })

  passport.deserializeUser(function(user, done) {
    done(null, user)
  })

  app.use(passport.initialize())
  app.use(passport.session())

  app.use('/auth/unauthorized', (req, res) => {
    res.status(401).json({
      error: {
        code: 'UNAUTHORIZED',
        message: 'Unauthorized access',
      },
    })
  })

  logout({ app, config, plugins })

  return app
}
