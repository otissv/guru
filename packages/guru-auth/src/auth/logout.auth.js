import { parseQueryString, logoutError } from 'guru-utils'

import { findAndLogout, getEngine } from '../queries.auth'

export function logout({ app, failedRedirect, redirect, plugins }) {
  app.use('/auth/logout', async (req, res) => {
    try {
      // get logout details
      const { username, token } = await parseQueryString(req.url)
      Object.keys(req.body || {}).length > 0
        ? req.body
        : req.url.indexOf('?') >= 0
        ? await parseQueryString(req.url)
        : {}

      if (!username || !token) {
        throw new Error()
      }

      // find user
      const { db, namespace } = getEngine(plugins)
      const user = await findAndLogout({
        app,
        db,
        namespace,
        plugins,
        data: { username, token },
      })

      req.logout()

      if (req.error) {
        throw new Error()
      }

      // success
      const message = 'User logged out was successful.'
      if (redirect) {
        req.message = message
        return res.redirect(301, redirect || '/logout')
      } else {
        return res.json({
          result: 'ok',
          message: 'User logged out was successful.',
        })
      }

      return app
    } catch (error) {
      console.error(error)

      if (failedRedirect) {
        return res.redirect(301, failedRedirect)
      }

      const [message, code] = logoutError()

      return res.status(400).json({
        result: 'failed',
        error: {
          code,
          message,
        },
      })
    }
  })
}
