import { Strategy as AzureOAuthStrategy } from 'passport-azure-oauth'
import passport from 'passport'
import { loginError } from 'guru-utils'

import { createOrUpdateOauthUser, getEngine } from '../queries.auth'

export function middleware({ app, config, failureRedirect, options }) {
  const { db, namespace } = getEngine(plugins)

  passport.use(
    new AzureOAuthStrategy(options, async function(
      accessToken,
      refreshToken,
      profile,
      done
    ) {
      try {
        const user = await createOrUpdateOauthUser({
          db,
          namespace,
          profile,
          provider: 'azure',
        })

        done(null, user)
      } catch (error) {
        console.error(error)
        const [errorMessage, errorCode] = loginError()

        done(null, {
          result: 'failed',
          error: {
            code: errorCode,
            message: errorMessage,
          },
        })
      }
    })
  )

  app.use(
    '/auth/azure/callback',
    passport.authenticate('azure', {
      failureRedirect: failureRedirect || '/auth/unauthorized',
      ...options,
    }),
    (req, res) => res.json(req.user)
  )
  app.use('/auth/azure', passport.authenticate('azure'))

  return app
}
