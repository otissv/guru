import { Strategy as FacebookStrategy } from 'passport-facebook'
import passport from 'passport'
import { loginError } from 'guru-utils'

import { createOrUpdateOauthUser, getEngine } from '../queries.auth'

export function middleware({ app, config, options, plugins }) {
  const { db, namespace } = getEngine(plugins)

  passport.use(
    new FacebookStrategy(options, async function(
      accessToken,
      refreshToken,
      profile,
      done
    ) {
      try {
        const user = await createOrUpdateOauthUser({
          db,
          namespace,
          profile,
          provider: 'facebook',
        })

        done(null, user)
      } catch (error) {
        console.error(error)
        const [errorMessage, errorCode] = loginError()

        done(null, {
          result: 'failed',
          error: {
            code: errorCode,
            message: errorMessage,
          },
        })
      }
    })
  )

  app.use(
    '/auth/facebook/callback',
    passport.authenticate('facebook', {
      failureRedirect: '/auth/unauthorized',
      ...options,
    }),
    (req, res, next) => {
      console.log('callback', req.session.redirectTo)
      req.session.redirectTo
        ? res.redirect(req.session.redirectTo)
        : res.json(req.user)
      res.json(req.user)
    }
  )
  app.use('/auth/facebook', passport.authenticate('facebook'))

  return app
}
