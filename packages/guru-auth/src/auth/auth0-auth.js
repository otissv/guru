import Auth0Strategy from 'passport-auth0'
import passport from 'passport'
import { loginError } from 'guru-utils'

import { createOrUpdateOauthUser, getEngine } from '../queries.auth'

export function middleware({ app, config, failureRedirect, options }) {
  const { db, namespace } = getEngine(plugins)

  passport.use(
    new Auth0Strategy(options, async function(
      accessToken,
      refreshToken,
      extraParams,
      profile,
      done
    ) {
      try {
        const user = await createOrUpdateOauthUser({
          db,
          namespace,
          profile,
          provider: 'auth0',
        })

        done(null, user)
      } catch (error) {
        const [errorMessage, errorCode] = loginError()

        done(null, {
          result: 'failed',
          error: {
            code: errorCode,
            message: errorMessage,
          },
        })
      }
    })
  )

  app.use(
    '/auth/auth0/callback',
    passport.authenticate('auth0', {
      failureRedirect: failureRedirect || '/auth/unauthorized',
      ...options,
    }),
    (req, res) => res.json(req.user)
  )
  app.use('/auth/auth0', passport.authenticate('auth0'))

  return app
}
