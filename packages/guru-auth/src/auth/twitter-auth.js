import { Strategy as TwitterStrategy } from 'passport-twitter'
import passport from 'passport'
import { loginError } from 'guru-utils'

import { createOrUpdateOauthUser, getEngine } from '../queries.auth'

export function middleware({ app, config, failureRedirect, options }) {
  const { db, namespace } = getEngine(plugins)

  passport.use(
    new TwitterStrategy(options, async function(
      req,
      accessToken,
      refreshToken,
      profile,
      done
    ) {
      try {
        const user = await createOrUpdateOauthUser({
          db,
          namespace,
          profile,
          provider: 'twitter',
        })

        done(null, user)
      } catch (error) {
        done(null, {
          result: 'failed',
          error: {
            code: errorCode,
            message: errorMessage,
          },
        })
      }
    })
  )

  app.use(
    '/auth/twitter/callback',
    passport.authenticate('twitter', {
      failureRedirect: failureRedirect || '/auth/unauthorized',
      ...options,
    }),
    (req, res) => res.json(req.user)
  )
  app.use('/auth/twitter', passport.authenticate('twitter'))

  return app
}
