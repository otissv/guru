import { types } from '../types.auth'

export function typeDef({ app, config, plugins }) {
  const engine = plugins.GURU_AUTH.config.engine

  return types(engine)
}
