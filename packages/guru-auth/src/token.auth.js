import bcrypt from 'bcryptjs'
import fs from 'fs'
import jwt from 'jsonwebtoken'
import util from 'util'
import dotenv from 'dotenv'
import selfSigned from 'selfsigned'
import { loginError } from 'guru-utils'

dotenv.config()

const promisify = util.promisify
const readFile = promisify(fs.readFile)
const writeFile = promisify(fs.writeFile)

const sign = promisify(jwt.sign)
const verify = promisify(jwt.verify)
const generateSelfSigned = promisify(selfSigned.generate)

export const ALGORITHM = 'RS256'

export async function generateAuthKeys({ attributes = [], options = {} } = {}) {
  try {
    const keys = await generateSelfSigned(null, {
      days: 30,
      keySize: 1024,
      ...options,
    })

    writeFile(`${process.cwd()}/.public.auth`, keys.public)
    writeFile(`${process.cwd()}/.private.auth`, keys.private)
  } catch (error) {
    console.log(error)
    return error
  }
}

export async function getPublicAuthKey() {
  try {
    return await readFile(`${process.cwd()}/.public.auth`, 'utf8')
  } catch (error) {
    console.log(error)
    return error
  }
}

export function getPublicAuthKeySync() {
  try {
    return fs.readFileSync(`${process.cwd()}/.public.auth`, 'utf8')
  } catch (error) {
    console.log(error)
    return error
  }
}

export async function getPrivateAuthKey() {
  try {
    return await readFile(`${process.cwd()}/.private.auth`, 'utf8')
  } catch (error) {
    console.log(error)
    return error
  }
}

export function getPrivateAuthKeySync() {
  try {
    return fs.readFileSync(`${process.cwd()}/.private.auth`, 'utf8')
  } catch (error) {
    console.log(error)
    return error
  }
}

export function generateHash(str) {
  return bcrypt.hashSync(str, bcrypt.genSaltSync(10), null)
}

export function isValidateHash(str, hash) {
  return bcrypt.compareSync(str, hash)
}

export async function jwtSign({ payload, options = {} }) {
  try {
    const opts = {
      algorithm: ALGORITHM,
      expiresIn: '1d',
      ...options,
    }

    const privateKey = await getPrivateAuthKey()

    return await sign(payload, privateKey, opts)
  } catch (error) {
    console.log(error)
    return error
  }
}

export async function jwtVerify({ token, options }) {
  try {
    const opts = {
      algorithms: ['RS256'],
      expiresIn: '1d',
      ...options,
    }

    const publicKey = await getPublicAuthKey()
    return await verify(token, publicKey, opts)
  } catch (error) {
    console.log(error)
    return { error }
  }
}
