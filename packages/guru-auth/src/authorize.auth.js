import dotenv from 'dotenv'
dotenv.config()

export async function authorize({ context, info }) {
  try {
    const {
      connectors: { permission },
      user,
    } = context

    if (user) {
      const operation = info.fieldName

      // Find permission's roles
      const permissions = await new permission().findMany(null, null, {
        ...context,
        options: {
          projection: { id: 0 },
        },
      })

      if (!permissions || permissions.length === 0) return true

      // Get operation  permission's roles
      const operationsPermission = permissions
        .filter(p => p.operation === operation)
        .reduce((p, c) => [...p, ...c.roles.map(r => r.role.toUpperCase())], [])

      // Check operation against user roles
      const filtered = user.roles.filter(
        r => operationsPermission.indexOf(r) >= 0
      )

      if (filtered.length > 0) {
        return true
      } else {
        return false
      }
    } else {
      return true
    }
    // Check operation against user roles
  } catch (error) {
    console.error(error)
    return error
  }
}
