import dotenv from 'dotenv'
import pkg from '../package.json'
import { middleware } from './middleware.auth'
import { resolvers } from './resolvers.auth'

dotenv.config()

export class AuthPlugin {
  namespace = 'GURU_AUTH'
  package = pkg.name

  constructor({ engine, config, namespace }) {
    this.config = {
      engine: engine,
    }

    this.namespace = namespace || this.namespace
  }

  middlewareModules() {
    return [{ middleware }]
  }

  resolvers() {
    return resolvers(...arguments)
  }
}
