export const types = engine => `
type Note @engine(namespace: "${engine}", collection: "notes") {
  _id: String
  flag: Boolean
  note: String
}

type Entity @engine(namespace: "${engine}", collection: "entity") {
  _id: String
  company: Company
  contacts: [Contact]
  name: String
  notes: [Note]
  status: ACTIVE_STATUS
  type: String
}

# Address type
type Address @engine(namespace: "${engine}", collection: "addresses") {
  _id: String
  address_1: String
  address_2: String
  city: String
  country: String
  county: String
  notes: [Note]
  status: ACTIVE_STATUS
  zipCode: String
}

# Company type
type Company @engine(namespace: "${engine}", collection: "companies") {
  _id: String
  addresses: [Address]
  name: String
  notes: [Note]
  status: ACTIVE_STATUS      
}

# Contact 
type Contact @engine(namespace: "${engine}", collection: "contacts")  {
  _id: String
  address: Address
  emails: Emails
  notes: [Note]
  owner: User
  phones: [Phone]
  status: ACTIVE_STATUS
}

# Email type
type Emails @engine(namespace: "${engine}", collection: "emails") {
  _id: String
  # Email address
  value: Email
  # Email type
  type: String
  # Meta Data
  META: META
  # Request results
  RESULTS: RESULTS
  # Count number of items
  COUNT: COUNT
  notes: [Note]
  status: ACTIVE_STATUS

}

# Job type
type Job @engine(namespace: "${engine}", collection: "jobs") {
  _id: String
  # Job area
  area: String
  # Job description 
  description: String
  # Job title
  title: String
  # Job type
  type: String
  # Meta Data
  META: META
  # Request results
  RESULTS: RESULTS
  # Count number of items
  COUNT: COUNT
  notes: [Note]
  status: ACTIVE_STATUS
}

# Phone Type
type Phone @engine(namespace: "${engine}", collection: "phone") {
  _id: String
  # Phone number
  value: String
  # Phone number type
  type: String
  # Meta Data
  META: META
  # Request results
  RESULTS: RESULTS
  # Count number of items
  COUNT: COUNT
  notes: [Note]
  status: ACTIVE_STATUS
}

# Create permission type
type Permission @engine(namespace: "${engine}", collection: "permissions") {
  # Permission ID.
  _id: String
  # Resolver operation name.
  operation: String
  # Authorized roles for permission.
  roles: [Role]
  # Count number of items
  # Request results
  RESULTS: RESULTS
  # Count number of items
  COUNT: COUNT
  # Meta Data
  META: META
  notes: [Note]
  status: ACTIVE_STATUS
}

# Person Type
type Person  @engine(namespace: "${engine}", collection: "person") {
  _id: String
  # Person's family name.
  familyName: String
  # Person's given name.
  givenName: String
  # Person's middle name.
  middleName: String
  # Meta Data
  META: META
  # Request results
  RESULTS: RESULTS
  # Count number of items
  COUNT: COUNT
  notes: [Note]
  status: ACTIVE_STATUS
}

# Role Type
type Role @engine(namespace: "${engine}", collection: "roles") {
  # Role ID.
  _id: String
  # Authorized role.
  name: String
  # Roles description.
  description: String
  # Meta Data
  META: META
  # Request results
  RESULTS: RESULTS
  # Count number of items
  COUNT: COUNT
  notes: [Note]
  status: ACTIVE_STATUS
}

# User Type
type User @engine(namespace: "${engine}", collection: "users") {
  # User's id
  _id: String
  # Time and date of the last time a user logged in.
  lastLogin: String
  # The provider the last user used to log in.
  lastLoggedInWith: String
  # User's encrypted password.
  password: String
  # User's username
  username: String
  # Json Web Token
  token: String
  # Invalidated tokens
  invalidTokens: [String]
  # List of user's roles.
  roles: [Role]
  # Auth0 ID
  auth0Id: [String]
  # Azuer ID
  azuerId: String
  # Bitbucket ID
  bitbucketId: String
  # Auth0 API ID
  auth0apiId: String
  # Facebook ID
  facebookId: String
  # GitHub ID
  githubId: String
  # Gitlab ID
  gitlabId: String
  # Google ID
  googleId: String
  # Instagram ID
  instagramId: String
  # Jwt ID
  jwtId: String
  # windowslive ID
  windowsliveId: String
  # twitter ID
  twitterId: String
  # Meta Data
  META: META
  # Request results
  RESULTS: RESULTS
  # Count number of items
  COUNT: COUNT
  notes: [Note]
  status: ACTIVE_STATUS
}

# User Profile type
type UserProfile @engine(namespace: "${engine}", collection: "user_profiles") {
  # User profile ID
  _id: String
  # User addresses
  addresses: [Address]
  # User company
  company: Company
  # User Avatar
  avatar: String
  # User's ID
  user: User
  # User bio
  bio: String
  # User's display name
  displayName: String
  # User's email address
  emails: [Emails]
  # URL to user's photos image.
  photos: [String]
  # User's website.
  website: String
  # Users phone numbers
  phoneNumbers: [Phone]
  # User's organization
  organization: String
  # User's location
  location: String
  # Meta Data
  META: META
  # Request results
  RESULTS: RESULTS
  # Count number of items
  COUNT: COUNT
  status: ACTIVE_STATUS
}

type Mutation {
  authDelete (
    data: InputAuthAuthenticate
  ): User

  authLogin (
    data: InputAuthLogin
  ): User

  authOauthLogin (
    data: InputAuthOauthLogin
  ): User

  authLogout(
    data: InputAuthLogout
  ): User

  authRegister(
    data: InputAuthRegister
  ): User

  authRevoke(
    data: InputAuthRevoke
  ): User
}

type Query {
  authLogin(
    data: InputAuthLogin
  ): User

  authAuthenticate (
    query: InputAuthAuthenticate
  ): User
}

input InputAuthAuthenticate {
  username: String!
  token: String!
}

input InputAuthLogin {
  username: String!
  password: String!
}

input InputAuthOauthLogin {
  provider: String!
}

input InputAuthLogout {
  username: String!
  token: String!
}

input InputAuthRegister {
  username: String!
  password: String!
}

input InputAuthRevoke {
  username: String!
  invalidTokens: [String]
}
`
