import dotenv from 'dotenv'
import passport from 'passport'
import url from 'url'
import { parse } from 'graphql'

import { authInit } from './auth/init-auth'
import { generateAuthKeys } from './token.auth'

import { middleware as auth0Middleware } from './auth/auth0-auth'
import { middleware as azureMiddleware } from './auth/azure-auth'
import { middleware as bitbucketMiddleware } from './auth/bitbucket-auth'
import { middleware as facebookMiddleware } from './auth/facebook-auth'
import { middleware as githubMiddleware } from './auth/github-auth'
import { middleware as gitlabMiddleware } from './auth/gitlab-auth'
import { middleware as googleMiddleware } from './auth/google-auth'
import { middleware as instagramMiddleware } from './auth/instagram-auth'
import { middleware as jwtMiddleware } from './auth/jwt-auth'
import { middleware as linkedinMiddleware } from './auth/linkedin-auth'
import { middleware as localMiddleware } from './auth/local-auth'
import { middleware as twitterMiddleware } from './auth/twitter-auth'
import { middleware as windowsliveMiddleware } from './auth/windowslive-auth'

dotenv.config()

const storeRedirectToInSession = (req, res, next) => {
  req.session.redirectTo = req.originalUrl
  console.log
  next()
}

export function authenticate(req, res, next) {
  let provider = 'jwt'
  storeRedirectToInSession(req, res, next)

  console.log(req.user)
  if (!req.user) {
    passport.authenticate('facebook')(req, res, next)
    // try {
    //   const definition = parse(req.body.query).definitions.find(
    //     d =>
    //       d.operation === 'query' ||
    //       d.operation === 'mutation' ||
    //       d.operation === 'subscription'
    //   )

    //   const authDirective = definition.directives.find(
    //     d => d.name.value === 'auth'
    //   )
    //   const auth = authDirective.arguments.reduce(
    //     (acc, arg) => ({
    //       ...acc,
    //       [arg.name.value]: arg.value.value,
    //     }),
    //     {}
    //   )
    //   provider = auth.provider
    //   req.auth = auth
    // } catch (error) {
    //   provider = req.body.provider
    // } finally {
    //   passport.authenticate(provider, (error, user, info) => {
    //     console.log({ error, user, info })

    //     next()
    //   })(req, res, next)
    // }
  }
}

export async function middleware(args) {
  // generateAuthKeys()

  const { app, config, plugins } = args
  const hasValues = obj => Object.values(obj).every(val => Boolean(val))

  if (!config.auth || !hasValues(config.auth)) {
    throw Error('auth config is invalid or missing')
  }

  function getStrategies(options) {
    return {
      auth0: () => auth0Middleware({ ...args, options }),
      azure: () => azureMiddleware({ ...args, options }),
      bitbucket: () => bitbucketMiddleware({ ...args, options }),
      facebook: () => facebookMiddleware({ ...args, options }),
      github: () => githubMiddleware({ ...args, options }),
      gitlab: () => gitlabMiddleware({ ...args, options }),
      google: () => googleMiddleware({ ...args, options }),
      instagram: () => instagramMiddleware({ ...args, options }),
      jwt: () => jwtMiddleware({ ...args, options }),
      linkedin: () => linkedinMiddleware({ ...args, options }),
      twitter: () => twitterMiddleware({ ...args, options }),
      windowslive: () => windowsliveMiddleware({ ...args, options }),
      windowslive: () => windowsliveMiddleware({ ...args, options }),
      local: () => localMiddleware({ ...args, options }),
    }
  }

  authInit({ app, ...args })

  Object.keys(config.auth).forEach(strategyName => {
    const strategy = getStrategies(config.auth[strategyName])
    strategy[strategyName] && strategy[strategyName]()
  })
}
