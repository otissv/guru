import {
  createLocalUser,
  deleteUser,
  findAndLogout,
  findUser,
  updateUser,
} from './queries.auth'

import { jwtSign, jwtVerify, isValidateHash } from './token.auth'

import { ApolloError } from 'apollo-server'

import {
  invalidAuthenticationError,
  databaseConfigurationError,
  noInputError,
  logoutError,
  errorLogger,
  resolverMutationError,
  resolverQueryError,
  loginError,
} from 'guru-utils'

export function resolvers({ context, typeDefs, namespace }) {
  const engine = context.GURU_AUTH.config.engine
  return {
    Mutation: {
      authDelete: async (parent, args, context) => {
        const resolverName = 'authDelete'
        try {
          if (!context || !context[namespace]) {
            throw new ApolloError(
              ...databaseConfigurationError({ namespace, resolverName })
            )
          }

          if (!args.data || !args.data.username || !args.data.token) {
            throw new ApolloError(...noInputError({ resolverName, args: args }))
          }

          const db = context[engine]

          const user = await findUser({
            db,
            namespace: engine,
            query: {
              username: args.data.username,
            },
          })

          if (!user) {
            throw invalidAuthenticationError()
          }

          const { error, ...decoded } = await jwtVerify({
            token: args.data.token,
          })

          if (
            error ||
            decoded.username !== user.username ||
            !isValidateHash(decoded.password, user.password)
          ) {
            throw invalidAuthenticationError()
          }

          return await deleteUser({
            db,
            namespace: engine,
            query: { username: args.data.username },
          })
        } catch (error) {
          errorLogger(error)

          return error.extensions
            ? error
            : new ApolloError(
                ...resolverMutationError({
                  error,
                  namespace,
                  resolverName,
                })
              )
        }
      },

      authLogin: async (parent, args, context) => {
        const resolverName = 'authAuthenticate'

        try {
          if (!context || !context[namespace]) {
            throw new ApolloError(
              ...databaseConfigurationError({ namespace, resolverName })
            )
          }

          if (!args.data || !args.data.username || !args.data.password) {
            throw new ApolloError(...noInputError({ resolverName, args: args }))
          }

          const db = context[engine]

          const result = await findUser({
            db,
            namespace: engine,
            query: {
              username: args.data.username,
            },
          })

          if (!result) {
            throw new ApolloError(...loginError())
          }

          if (!isValidateHash(args.data.password, result.password)) {
            throw Error('Incorrect name or password')
          }

          const token = await jwtSign({
            payload: {
              username: args.data.username,
              password: args.data.password,
            },
          })

          const { password, meta, ...user } = result
          return { ...user, token }
        } catch (error) {
          errorLogger(error)

          return error.extensions
            ? error
            : new ApolloError(
                ...resolverMutationError({
                  error,
                  namespace,
                  resolverName,
                })
              )
        }
      },

      authLogout: async (parent, args, context) => {
        const resolverName = 'authLogout'

        try {
          if (!context || !context[namespace]) {
            throw new ApolloError(
              ...databaseConfigurationError({ namespace, resolverName })
            )
          }
          if (!args.data && !args.data.username) {
            throw new ApolloError(...noInputError({ resolverName, args: args }))
          }

          const db = context[engine]

          const user = await findAndLogout({
            db,
            namespace,
            data: { username: args.data.username, token: args.data.token },
          })
          if (user.token) {
            throw logoutError()
          }
          return user
        } catch (error) {
          errorLogger(error)

          return error.extensions
            ? error
            : new ApolloError(
                ...resolverMutationError({
                  error,
                  namespace,
                  resolverName,
                })
              )
        }
      },

      authRegister: async (parent, args, context) => {
        const resolverName = 'authRegister'

        try {
          if (!context || !context[namespace]) {
            throw new ApolloError(
              ...databaseConfigurationError({ namespace, resolverName })
            )
          }

          if (!args.data || !args.data.username || !args.data.password) {
            throw new ApolloError(...noInputError({ resolverName, args: args }))
          }

          const db = context[engine]

          return await createLocalUser({
            db,
            namespace,
            data: args.data,
          })
        } catch (error) {
          errorLogger(error)

          return error.extensions
            ? error
            : new ApolloError(
                ...resolverMutationError({
                  error,
                  namespace,
                  resolverName,
                })
              )
        }
      },

      authRevoke: async (parent, args, context) => {
        const resolverName = 'authRevoke'

        try {
          if (!context || !context[namespace]) {
            throw new ApolloError(
              ...databaseConfigurationError({ namespace, resolverName })
            )
          }

          if (!args.data || !args.data.invalidTokens) {
            throw new ApolloError(...noInputError({ resolverName, args: args }))
          }

          const db = context[engine]

          const result = await findUser({
            db,
            namespace: engine,
            data: {
              username: args.data.username,
            },
          })
          const invalidTokens = args.data.invalidTokens
            ? args.data.invalidTokens
            : []

          return await userUpdateOne({
            query: { username },
            data: { invalidTokens: [] },
            db,
            context,
            typeDefs,
            namespace,
          })
        } catch (error) {
          errorLogger(error)

          return error.extensions
            ? error
            : new ApolloError(
                ...resolverMutationError({
                  error,
                  namespace,
                  resolverName,
                })
              )
        }
      },
    },

    Query: {
      authAuthenticate: async (parent, args, context) => {
        const resolverName = 'authAuthenticate'

        try {
          if (!context || !context[namespace]) {
            throw new ApolloError(
              ...databaseConfigurationError({ namespace, resolverName })
            )
          }

          if (!args.query || !args.query.username || !args.query.token) {
            throw new ApolloError(...noInputError({ resolverName, args: args }))
          }

          const db = context[engine]

          const result = await findUser({
            db,
            namespace: engine,
            query: {
              username: args.data.username,
            },
          })

          if (!result) {
            throw invalidAuthenticationError()
          }

          const { error, ...decoded } = await jwtVerify({
            token: args.query.token,
          })

          if (
            error ||
            decoded.username !== result.username ||
            !isValidateHash(decoded.password, result.password)
          ) {
            throw invalidTokenError()
          }

          const isInvalidToken = result.invalidTokens.find(
            t => t === args.query.token
          )

          if (isInvalidToken) {
            throw invalidAuthenticationError()
          }

          const { password, meta, ...user } = result
          return { ...user, token: args.query.token }
        } catch (error) {
          errorLogger(error)

          return error.extensions
            ? error
            : new ApolloError(
                ...resolverQueryError({
                  error,
                  namespace,
                  resolverName,
                })
              )
        }
      },
    },
  }
}
