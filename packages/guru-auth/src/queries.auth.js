import { generateHash, isValidateHash, jwtSign, jwtVerify } from './token.auth'
import { loginError } from 'guru-utils'

import { types } from './types.auth'
import cuid from 'cuid'

export function getEngine(plugins) {
  const namespace = plugins.GURU_AUTH.config.engine
  const db = plugins[namespace]

  if (!namespace) {
    throw new Error('No engine provided for Guru Auth')
  }

  return { db, namespace }
}

async function userInsertOne({ db, context, typeDefs, namespace, data }) {
  try {
    const Mutation = db.resolvers({
      context,
      typeDefs,
      namespace,
    }).Mutation

    const response = await Mutation.userInsertOne(null, { data }, context)
    if (response.RESULTS.result === 'failed') {
      throw response
    }

    return response
  } catch (error) {
    console.error(error)
    return Promise.reject(error)
  }
}

async function userFindOne({ db, context, typeDefs, namespace, query }) {
  try {
    const Query = db.resolvers({
      context,
      typeDefs,
      namespace,
    }).Query

    return await Query.userFindOne(null, { query }, context)
  } catch (error) {
    console.log(error)
    return Promise.reject(error)
  }
}

async function userDeleteOne({
  db,
  context,
  typeDefs,
  namespace,
  query,
  data,
}) {
  try {
    const Mutation = db.resolvers({
      context,
      typeDefs,
      namespace,
    }).Mutation

    return await Mutation.userDeleteOne(null, { query, data }, context)
  } catch (error) {
    console.log(error)
    return Promise.reject(error)
  }
}

async function userUpdateOne({
  db,
  context,
  typeDefs,
  namespace,
  data,
  query,
}) {
  try {
    const Mutation = db.resolvers({
      context,
      typeDefs,
      namespace,
    }).Mutation

    const response = await Mutation.userUpdateOne(
      null,
      { data, query },
      context
    )

    if (response.RESULTS.result === 'failed') {
      throw response
    }

    return response
  } catch (error) {
    console.error(error)
    return Promise.reject(error)
  }
}

async function userProfileFindOne({ db, context, typeDefs, namespace, query }) {
  try {
    const Query = db.resolvers({
      context,
      typeDefs,
      namespace,
    }).Query

    return await Query.userProfileFindOne(null, { query }, context)
  } catch (error) {
    console.error(error)
    return Promise.reject(error)
  }
}

async function userProfileInsertOne({
  db,
  context,
  typeDefs,
  namespace,
  data,
}) {
  try {
    const Mutation = db.resolvers({
      context,
      typeDefs,
      namespace,
    }).Mutation

    const response = await Mutation.userProfileInsertOne(
      null,
      { data },
      context
    )

    if (response.RESULTS.result === 'failed') {
      throw response
    }

    return response
  } catch (error) {
    console.error(error)
    return Promise.reject(error)
  }
}

async function userProfileDeleteOne({
  db,
  context,
  typeDefs,
  namespace,
  query,
  data,
}) {
  try {
    const Mutation = db.resolvers({
      context,
      typeDefs,
      namespace,
    }).Mutation

    return await Mutation.userProfileDeleteOne(null, { query, data }, context)
  } catch (error) {
    console.log(error)
    return Promise.reject(error)
  }
}

export async function createLocalUser({ db, data = {}, namespace }) {
  const { username, password, email, name, roles } = data

  try {
    const notString = str => !typeof 'string' && str.trim() !== ''

    if (notString(username) || notString(password)) {
      const [errorMessage, errorCode] = loginError()
      throw {
        error: {
          code: errorCode,
          message: errorMessage,
        },
      }
    }

    const user = await findUser({
      db,
      namespace,
      query: { username },
    })

    if (user) {
      throw new Error('User already exists')
    }

    const context = { [namespace]: await db }
    const token = await jwtSign({
      payload: {
        username,
        password,
      },
    })

    const typeDefs = `${types(namespace)}`

    const userResponse = await userInsertOne({
      db,
      context,
      typeDefs,
      namespace,
      data: {
        password: await generateHash(password),
        roles,
        username,
        lastLoggedInWith: 'guru',
        lastLogin: new Date().toISOString(),
        invalidTokens: [],
      },
    })

    await userProfileInsertOne({
      db,
      context,
      typeDefs,
      namespace,
      data: {
        user: userResponse.RESULTS._id.toString(),
        displayName: name,
        username,
        emails: [email],
      },
    })

    return {
      _id: userResponse.RESULTS._id,
      displayName: data.name,
      roles: roles,
      token,
      username,
    }
  } catch (error) {
    return Promise.reject(error)
  }
}

export async function createOrUpdateOauthUser({
  db,
  namespace,
  profile,
  provider,
}) {
  try {
    const context = { [namespace]: await db }
    const providerId = { [`${provider}Id`]: profile.id }
    const typeDefs = `${types(namespace)}
    input InputAuthUser {
      username: String
    }
  `

    const user = await userFindOne({
      db,
      context,
      typeDefs,
      namespace,
      query: providerId,
    })

    const userData = {
      ...user,
      ...providerId,
      lastLoggedInWith: provider,
      lastLogin: new Date().toISOString(),
    }

    if (user) {
      // Update existing user
      const userId = user._id.toString()

      const userResponse = await userUpdateOne({
        query: { _id: userId },
        data: { ...userData },
        db,
        context,
        typeDefs,
        namespace,
      })

      const userProfileResponse = await userProfileFindOne({
        db,
        context,
        typeDefs,
        namespace,
        query: { user: userId },
      })

      return {
        _id: userId,
        displayName: userProfileResponse.displayName,
        roles: user.roles,
        username: user.username,
      }
    } else {
      // Create a new user with profile
      const username = profile.username || `oauth_${cuid()}`

      const userResponse = await userInsertOne({
        db,
        context,
        typeDefs,
        namespace,
        data: { ...userData, username, invalidTokens: [] },
      })

      const userProfileResponse = await userProfileInsertOne({
        data: {
          user: `${userResponse.RESULTS._id}`,
          displayName: profile.displayName,
          emails: profile.emails,
        },
        db,
        context,
        typeDefs,
        namespace,
      })
      return {
        _id: `${userResponse.RESULTS._id}`,
        displayName: profile.displayName,
        roles: userData.roles,
        username,
      }
    }
  } catch (error) {
    console.error(error)
    return Promise.reject(error)
  }
}

export async function findUser({ db, namespace, query }) {
  try {
    const context = { [namespace]: await db }
    const typeDefs = `${types(namespace)}
    input InputAuthUser {
      _id: String
      password: String
      username: String
    }
  `
    return await userFindOne({
      db,
      context,
      typeDefs,
      namespace,
      query: { username: query.username },
    })
  } catch (error) {
    console.error(error)
    return Promise.reject(error)
  }
}

export async function findAndLogout({
  db,
  namespace,
  data: { username, token },
}) {
  try {
    const context = { [namespace]: await db }
    const typeDefs = `${types(namespace)}
    input InputAuthUser {
      username: String!
    }
  `

    const user = await userFindOne({
      db,
      context,
      typeDefs,
      namespace,
      query: { username },
    })

    if (!user) {
      return Promise.reject('User does not exist')
    }

    const invalidTokens = user.invalidTokens || []

    const insertToken =
      invalidTokens.filter(t => t === token).length > 0 ? [] : [token]

    const { error } = await jwtVerify({ token })

    if (error) {
      throw invalidTokenError()
    }

    return await userUpdateOne({
      query: { username },
      data: { invalidTokens: [...invalidTokens, ...insertToken] },
      db,
      context,
      typeDefs,
      namespace,
    })
  } catch (error) {
    console.error(error)
    return Promise.reject(error)
  }
}

export async function updateUser({ db, namespace, data: { username } }) {
  try {
    const context = { [namespace]: await db }
    const typeDefs = `${types(namespace)}
    input InputAuthUser {
      username: String!
    }
  `

    return await userUpdateOne({
      query: { username },
      data: { invalidTokens: [] },
      db,
      context,
      typeDefs,
      namespace,
    })
  } catch (error) {
    console.error(error)
    return Promise.reject(error)
  }
}

export async function deleteUser({ db, namespace, query }) {
  try {
    const context = { [namespace]: await db }
    const typeDefs = `${types(namespace)}
    input InputAuthUser {
      password: String
      token: String
    }
  `

    const result = await findUser({
      db,
      context,
      typeDefs,
      namespace,
      query,
    })

    const userResponse = await userDeleteOne({
      db,
      context,
      typeDefs,
      namespace,
      query,
    })

    console.log()

    await userProfileDeleteOne({
      db,
      context,
      typeDefs,
      namespace,
      query: { user: result._id.toString() },
    })

    return userResponse
  } catch (error) {
    console.error(error)
    return Promise.reject(error)
  }
}
