import { Box, buildTypeDefs, getObjectDirectiveArguments, typeEngine } from 'guru-utils'

import camelCase from 'lodash/fp/camelCase'
import upperFirst from 'lodash/fp/upperFirst'

export function minimongoTypeDefs({ typeDefs, namespace, nonInputTypes }) {
  return buildTypeDefs(typeDefs, nonInputTypes, ({ current, input }) => {
    const { engine } = getObjectDirectiveArguments(current)
    const dbEngine = typeEngine({
      engine,
      namespace,
      type: 'typeDef',
      name: 'minimongo',
    })

    if (!dbEngine) return { inputs: '', types: '' }
    const nameCamelCase = camelCase(current.name.value)

    const name = Box(current.name.value)
      .map(str => camelCase(str))
      .map(str => upperFirst(str))
      .fold(str => str)

    // console.log(`
    //   ${nameCamelCase}Resolve(query: Input${name}, projection: Input${name}): ${name}

    //   `)
    return {
      inputs: `
        input Input${name} {
          ${input.fields}
        }
    
        input InputNested${name} {
          ${input.fields}
        }
      `,
      types: `
        type Query {
          ${nameCamelCase}Find(query: Input${name}, projection: Input${name}): [${name}]          
          ${nameCamelCase}FindById(query: Input${name}, projection: Input${name}): ${name}
          ${nameCamelCase}FindOne(query: Input${name}, projection: Input${name}): ${name}
          ${nameCamelCase}Resolve(query: Input${name}, projection: Input${name}): ${name}
        }

        type Mutation {
          ${nameCamelCase}Remove(query: Input${name}, data: Input${name}): ${name}
          ${nameCamelCase}Upsert(query: Input${name}, data: Input${name}): ${name}
        }
      `,
    }
  })
}
