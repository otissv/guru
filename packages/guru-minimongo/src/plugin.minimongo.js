import { MinimongoClient } from './client.minimongo'
import { minimongoResolvers } from './resolvers.minimongo'
import { minimongoTypeDefs } from './typeDefs.minimongo'
import pkg from '../package.json'

export class MinimongoPlugin {
  _minimongoClient
  namespace = 'minimongo'
  package = pkg.name
  type = 'MongodbPlugin'

  constructor({ namespace, seed } = {}) {
    this.namespace = namespace || this.namespace

    this._connect()
    this.seed(seed)
  }

  _connect = () => {
    this._minimongoClient = new MinimongoClient({
      namespace: this.namespace,
      options: this.options,
    })
  }

  seed = async collections => {
    try {
      if (collections) return

      await this._minimongoClient.client()
      await this._minimongoClient.seed(collections)
    } catch (error) {
      throw error
    }
  }

  client = () => {
    return this._minimongoClient.client()
  }

  resolvers() {
    return minimongoResolvers(...arguments)
  }

  typeDefs() {
    return minimongoTypeDefs(...arguments)
  }
}
