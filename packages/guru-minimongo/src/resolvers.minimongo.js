import { getObjectDirectiveArguments, notObjectTypeDef, typeEngine } from 'guru-utils'

import { upsertMutation } from './mutations'
import { findByIdQuery, findQuery, findOneQuery, resolveQuery } from './queries'

import camelCase from 'lodash/fp/camelCase'
import { parse } from 'graphql'

export function minimongoResolvers({ context, typeDefs, namespace }) {
  return parse(typeDefs).definitions.reduce(
    (previous, current) => {
      if (notObjectTypeDef(current)) return previous

      const { engine } = getObjectDirectiveArguments(current)
      const dbEngine = typeEngine({
        engine,
        namespace,
        type: 'resolver',
        name: 'MINIMONGO',
      })
      if (!dbEngine) return previous

      const collection = dbEngine.value.collection

      const name = camelCase(current.name.value)

      const args = {
        context,
        namespace,
        moduleName: name,
        collection: dbEngine.value.collection,
      }

      return {
        ...previous,

        Mutation: {
          ...previous.Mutation,
          // [`${name}AddCollection`]: (obj, args, context, info) => {},
          // [`${name}DropCollection`]: (obj, args, context, info) => {},
          // [`${name}DropManyCollection`]: (obj, args, context, info) => {},

          [`${name}Upsert`]: upsertMutation(args),

          [`${name}Remove`]: (obj, args, context, info) => {
            const minimongo = context[namespace]

            return minimongo
              .client()
              .then(db =>
                db[name].findOne(
                  args.input,
                  args.projection,
                  doc => resolve(doc),
                  error => reject(error)
                )
              )
          },
        },

        Query: {
          ...previous.Query,
          [`${name}Find`]: findQuery(args),
          [`${name}FindById`]: findByIdQuery(args),
          [`${name}FindOne`]: findOneQuery(args),
          [`${name}Resolve`]: resolveQuery(args),
        },
      }
    },
    { Query: {}, Mutation: {} }
  )
}
