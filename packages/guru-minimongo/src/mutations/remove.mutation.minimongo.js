import {
  collectionNameError,
  databaseConfigurationError,
  getResolverArgs,
  meta,
  noDataError,
  resolverMutationError,
} from 'guru-utils'
import cuid from 'cuid'

export function removeMutation(params) {
  const namespace = params.namespace
  const moduleName = params.moduleName
  const collection = params.collection
  const paramsContext = params.context
  const resolverName = `${moduleName}Upsert`

  return async function removeMutationResolver(
    parent,
    args,
    context = {},
    info
  ) {
    try {
      const dbInstance = paramsContext[namespace]
        ? await paramsContext[namespace]
        : context[namespace]
      // Error Checking
      if (!dbInstance) {
        // throw new ApolloError(
        //   ...databaseConfigurationError({ namespace, resolverName })
        // )
      }
      if (!collection) {
        // throw new ApolloError(...collectionNameError({ resolverName }))
      }

      // Arguments
      const obj = getResolverArgs(args, parent)
      const projection = obj.projection || {}
      const query = obj.query || {}
      const buildQuery = ({ id, ...query }) => ({
        ...(id ? { _id: id } : {}),
        ...query,
      })

      const { bases, unique } = options

      // Database query
      const db = await dbInstance.client()

      // Add collection if it doesn't exist
      !db[collection] && db.addCollection(collection)

      const response = await new Promise((resolve, reject) =>
        db[collection].remove(buildQuery(query), projection, resolve, reject)
      )

      if (response.error) {
        // throw new ApolloError(
        //   ...resolverMutationError({
        //     error: response.error,
        //     namespace,
        //     resolverName,
        //   })
        // )
      }

      return {
        RESULTS: {
          id: response._id,
          operation: resolverName,
          result: 'ok',
          n: 1,
        },
      }
    } catch (error) {
      console.log(error)
      return { error }
    }
  }
}
