import {
  collectionNameError,
  databaseConfigurationError,
  getResolverArgs,
  meta,
  noDataError,
  resolverMutationError,
} from 'guru-utils'
import cuid from 'cuid'

export function upsertMutation(params) {
  const namespace = params.namespace
  const moduleName = params.moduleName
  const collection = params.collection
  const paramsContext = params.context
  const resolverName = `${moduleName}Upsert`

  return async function upsertMutationResolver(
    parent,
    args,
    context = {},
    info
  ) {
    try {
      const dbInstance = paramsContext[namespace]
        ? await paramsContext[namespace]
        : context[namespace]
      // Error Checking
      if (!dbInstance) {
        // throw new ApolloError(
        //   ...databaseConfigurationError({ namespace, resolverName })
        // )
      }
      if (!collection) {
        // throw new ApolloError(...collectionNameError({ resolverName }))
      }

      if (!args.data) {
        // throw new ApolloError(...noDataError({ resolverName, args: args }))
      }

      // Arguments
      const obj = getResolverArgs(args, parent)
      const data = obj.data || {}
      const options = obj.options || {}
      const query = obj.query || {}

      const { bases, unique } = options

      // Database query
      const db = await dbInstance.client()

      // Add collection if it doesn't exist
      !db[collection] && db.addCollection(collection)

      const buildData = ({ id, ...data }) => ({
        _id: id || cuid(),
        ...data,
      })

      const mappedData = data =>
        Array.isArray(data)
          ? data.map(item => buildData(item))
          : buildData(data)

      const response = await new Promise((resolve, reject) =>
        db[collection].upsert(mappedData(data), bases, resolve, reject)
      )

      if (response.error) {
        // throw new ApolloError(
        //   ...resolverMutationError({
        //     error: response.error,
        //     namespace,
        //     resolverName,
        //   })
        // )
      }

      return {
        RESULTS: {
          id: response._id,
          operation: resolverName,
          result: 'ok',
          n: 1,
        },
      }
    } catch (error) {
      console.log(error)
      return { error }
    }
  }
}
