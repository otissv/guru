import minimongo from 'minimongo'

export class MinimongoClient {
  constructor({ context, namespace, typeDefs }) {
    this.namespace = namespace
    this.context = context
    this.typeDefs = typeDefs
  }

  client = () => {
    function localStorageTest() {
      let test = 'test'
      try {
        window.localStorage.setItem(test, test)
        window.localStorage.removeItem(test)
        return true
      } catch (e) {
        return false
      }
    }

    const localStorage = () =>
      new minimongo.LocalStorageDb({
        namespace: this.namespace,
      })

    const storageAutoselect = () =>
      new minimongo.utils.autoselectLocalDb(
        { namespace: this.namespace },
        db => db,
        error => {
          console.error(error)
          return error
        }
      )

    return localStorageTest()
      ? Promise.resolve(localStorage())
      : Promise.resolve(storageAutoselect())
  }

  resolvers = () => {
    return mongoDbResolvers({
      context: {
        ...this.context,
        context,
        [this.namespace]: this,
      },
      typeDefs: this.typeDefs,
      namespace: this.namespace,
    })
  }

  insertData = async ({ collection, data }) => {
    try {
      const db = await this.client()

      if (!db[collection]) {
        db.addCollection(collection)
      }

      return new Promise((resolve, reject) => {
        db[collection].seed(data, doc => resolve(doc), error => reject({ error }))
      })
    } catch (error) {
      console.log(error)
      return { error }
    }
  }

  seed = async collections => {
    if (!collections) return

    try {
      if (Array.isArray(collections)) {
        collections.forEach(async collection => {
          await this.insertData(collection)
        })
      } else {
        await this.insertData(collections)
      }
    } catch (error) {
      console.log(error)
      return { error }
    }
  }
}
