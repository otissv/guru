import {
  collectionNameError,
  databaseConfigurationError,
  getResolverArgs,
  meta,
  noDataError,
  resolverMutationError,
} from 'guru-utils'

export function findOneQuery(params) {
  const namespace = params.namespace
  const moduleName = params.moduleName
  const collection = params.collection
  const paramsContext = params.context
  const resolverName = `${moduleName}FindOne`

  return async function findOneQueryResolver(parent, args, context = {}, info) {
    try {
      const dbInstance = paramsContext[namespace]
        ? await paramsContext[namespace]
        : context[namespace]

      // Error Checking
      if (!dbInstance) {
        // throw new ApolloError(
        //   ...databaseConfigurationError({ namespace, resolverName })
        // )
      }
      if (!collection) {
        // throw new ApolloError(...collectionNameError({ resolverName }))
      }

      // Arguments
      const obj = getResolverArgs(args, parent)
      const projection = obj.projection || {}
      const query = obj.query || {}
      const buildQuery = ({ id, ...query }) => ({
        ...(id ? { _id: id } : {}),
        ...query,
      })

      // Database query
      const db = await dbInstance.client()

      // Add collection if it doesn't exist
      !db[collection] && db.addCollection(collection)

      const mapDoc = resolve => ({ _id, ...doc }) => resolve({ id: _id, ...doc })

      return await new Promise((resolve, reject) =>
        db[collection].findOne(buildQuery(query), projection).fetch(mapDoc(resolve), reject)
      )
    } catch (error) {
      console.log(error)
      return { error }
    }
  }
}
