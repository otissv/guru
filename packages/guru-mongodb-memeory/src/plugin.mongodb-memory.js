import MongoMemoryServer from 'mongodb-memory-server'
import { MongodbPlugin } from 'guru-mongodb'
import pkg from '../package.json'

export class MongodbMemoryPlugin extends MongodbPlugin {
  constructor({ namespace, options = {}, seed }) {
    const mongoServer = new MongoMemoryServer()
    const mongoUri = mongoServer.getConnectionString()
    const _options = {
      ...options,
      uri: options.uri || mongoUri,
    }
    const type = 'MongodbMemoryPlugin'
    super({
      namespace: namespace || 'mongodbMemory',
      options: _options,
      packageName: pkg.name,
      seed,
      type,
    })
  }
}
