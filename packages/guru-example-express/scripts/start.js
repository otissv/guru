const { exec, spawn } = require('child_process')
const nodemon = require('nodemon')
const chalk = require('chalk')

exec('mkdir -p $HOME/.gurutest', err => {
  if (err) {
    console.error(err)
    return
  }
})

const userHome = process.env[process.platform == 'win32' ? 'USERPROFILE' : 'HOME']

const pipe = spawn('mongod', [`--dbpath=${userHome}/.gurudata`])
pipe.stdout.on('data', function(data) {
  let verbose = false
  if (verbose) {
    console.log(data.toString('utf8'))
  } else {
    const str = data.toString('utf8')
    const port = str.match(/port=\w[0-9]*/g) ? str.match(/port=\w[0-9]*/g)[0] : null
    const shutdown = str.match(/shutting down.*/g) ? str.match(/shutting down.*/g) : null

    if (port) {
      console.log(`Started MongoDB server on port ${port.split('=')[1]}`)
    }

    if (shutdown) {
      if (data.toString('utf8').match(/shutting down with code:100/)) {
        console.log(`A mongo instance already running`)
      } else {
        console.log(`MongoDB ${shutdown}`)
      }
    }
  }
})

pipe.stderr.on('data', data => {
  console.log(data.toString('utf8'))
})

pipe.on('close', code => {
  console.log('Process exited with code: ' + code)
})

// Node Server
nodemon({
  script: './dist/index.js',
  ext: 'js json graphql',
  ignore: 'dist/__generated__/*',
})

nodemon
  .on('start', function() {})
  .on('quit', function() {
    process.exit()
  })
  .on('restart', function(files) {
    console.log(chalk.green('App restarted due to: ', files))
  })
