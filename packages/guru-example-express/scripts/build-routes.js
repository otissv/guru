const argv = require('minimist')(process.argv.splice(2))
const chalk = require('chalk')
const fs = require('fs')
const globby = require('globby')
const path = require('path')
const shell = require('shelljs')
const util = require('util')
const watch = require('watch')

const writeFile = util.promisify(fs.writeFile)
const lstat = util.promisify(fs.lstat)

const getConfig = (key, value, base) => {
  if (config && config[key]) {
    return key === 'base' ? config[key] : path.resolve(base, config[key])
  } else {
    return value
  }
}

const configPath = argv.c || argv.config || './view/fspages.config'
const watching = argv.w || argv.watching

const config = require(path.resolve(process.cwd(), configPath))
const BASE = getConfig('base', process.cwd())

const DEST = getConfig('output', path.resolve(BASE, './pages'), BASE)
const SRC = getConfig('src', path.resolve(BASE, './fspages'), BASE)
const ROUTES = `${DEST}/routes.json`

async function cleanPages() {
  try {
    shell.rm('-r', DEST)
    shell.mkdir(DEST)
    shell.rm('-r', ROUTES)
  } catch (error) {
    throw new Error(error)
  }
}

function upperFirst(str) {
  return `${str[0].toUpperCase()}${str.substr(1, str.length + 1)}`
}

function subPageCode({ page, indexPath }) {
  const pageName = page.match(/_([^._]+)\./)[1]
  const pagePath = page.replace(/_/g, '/')
  const component = upperFirst(pageName)

  return `import ${component} from '../fspages/${pagePath}'
import Index from '../fspages/${indexPath}'

export default props => <Index {...props}>
  <${component}/>
</Index>
`
}

function pageCode(page) {
  const pageSplit = page.split('.')[0].split('_')
  component = upperFirst(pageSplit[pageSplit.length - 1])

  return `import ${component} from '../fspages/${page.replace(/_/g, '/')}'

export default ${component}`
}

async function cratePage({ page, files }) {
  try {
    let data = ''

    const nestedIndex =
      page.lastIndexOf('_pages_') >= 0 && !page.match('_index') ? page.lastIndexOf('_pages_') : null

    if (nestedIndex) {
      const fsIndexPagePath = `${SRC}/${page
        .substr(0, page.lastIndexOf('_'))
        .replace(/_/g, '/')}/index`

      const indexPage = files.filter(file => file.match(fsIndexPagePath))[0]
      let indexPath

      if (indexPage) {
        indexPath = indexPage.replace(`${SRC}/`, '')
      } else {
        throw new Error(
          `
${page.replace(/_/g, '/')} does not have a root index in its pages directory
          `
        )
      }

      data = subPageCode({
        page,
        indexPath,
      })
    } else {
      data = pageCode(page)
    }

    await writeFile(`${DEST}/${page}.js`, data)
  } catch (error) {
    throw new Error(error)
  }
}

function getHref(href) {
  switch (true) {
    case href === '/index':
      // root page

      return '/'

    // index page
    case Boolean(href.match(/\/index/)):
      return href.replace(/\/index/, '')

    // edit page
    case Boolean(href.match(/\/edit$/)):
      return href.replace(/\/edit$/, '/:param/edit')

    // params page
    case Boolean(href.match(/\/param$/)):
      return href.replace(/\/param$/, '/:param')

    default:
      return href
  }
}

async function router() {
  try {
    const files = await globby(`${SRC}/**/*`)
    console.log(SRC, files)

    return files.reduce(async (previous, filePath) => {
      try {
        const previousArr = await previous
        const isDirectory = await lstat(filePath).then(stats => stats.isDirectory())

        if (isDirectory) return previousArr

        const file = filePath.replace(SRC, '')
        const fileName = file.substr(file.lastIndexOf('/') + 1)

        // Do not add routes for next files
        if (fileName.substr(0, 1) === '_') {
          shell.cp('-r', filePath, DEST)
          return previousArr
        }

        const href = file.split('.')[0]
        const page = file
          .substr(1, file.length - 1)
          .replace(/\//g, '_')
          .replace('.js', '')

        // Do not add root for nested pages
        if (href.match('/pages/index')) return previousArr
        await cratePage({ page, files })

        return [
          ...previousArr,
          {
            href: getHref(href).replace('/pages/', '/'),
            page: `/${page}`,
          },
        ]
      } catch (error) {
        throw new Error(error)
      }
    }, [])
  } catch (error) {
    throw new Error(error)
  }
}

async function createRoutesFile(routes) {
  try {
    return writeFile(ROUTES, JSON.stringify(routes, null, 2))
  } catch (error) {
    throw new Error(error)
  }
}

function buildRoutes() {
  Promise.resolve()
    .then(cleanPages)
    .then(router)
    .then(createRoutesFile)
    .then(() => console.log(`${chalk.white.bgGreen(' DONE ')} Built pages into ${BASE}`))
    .catch(error => console.error(error))
}

if (watching) {
  buildRoutes()
  process.stdout.write(
    `Watching fspages
`
  )
  watch.watchTree(SRC, (file, curr, prev) => {
    if (typeof file === 'object' && prev === null && curr === null) {
      // Finished walking the tree
    } else {
      buildRoutes()
    }
  })
} else {
  buildRoutes()
}
