export function typeDef({ app, config, plugins }) {
  const engine = plugins.GURU_AUTH.config.engine

  return `
  #union Transaction = CashSale | ClientCreditNote | ClientInvoice | SupplierCreditNote | SupplierInvoice

  enum PAYMENT_STATUS {
    PAID
    NOT_PAID
    OVER_DUE
    PART_PAID
  }

  type Account @engine(namespace: "${engine}", collection: "account") {
    _id: String
    account: String
    company: Company
    contacts: [Contact]
    name: String
    notes: [Note]
    status: PAYMENT_STATUS
    type: String
  }

  type AccountsPayable @engine(namespace: "${engine}", collection: "accounts_payable") {
    _id: String
  }

  type AccountsReceivable @engine(namespace: "${engine}", collection: "accounts_receivable") {
    _id: String
  }

  type ChartOfAccounts @engine (namespace: "${engine}", collection: "cash_sales") {
    _id: String
    account: LedgerAccount
    code: String
    description: String
    entity: Entity
    name: String
    parent: Entity
  }

  type CashSale @engine(namespace: "${engine}", collection: "cash_sales") {
    _id: String
    amount: Float
    client: Account
    discount: Int
    date: DateTime
    entity: Entity
    notes: [Note]
    paymentDate: DateTime
    quantity: Float
    status: PAYMENT_STATUS
    transactionType: TransactionType
    vat: Vat 
  }

  type ClientCreditNote  @engine(namespace: "${engine}", collection: "client_credit_notes") {
    _id: String
    billingAddress: Address
    client:  Account
    clientInvoices: [ClientInvoice]
    clientOrders: [ClientOrder]
    date: DateTime
    entity: Entity
    notes: [Note]
    shippingAddress: Address
    shippingMethod: String
    status: PAYMENT_STATUS
    transactionType: TransactionType
  }

  type ClientInvoice @engine(namespace: "${engine}", collection: "client_invoices") {
    _id: String
    billingAddress: Address
    client: Account
    clientCreditNotes: [ClientCreditNote]
    clientOrders: [ClientOrder]
    date: DateTime
    entity: Entity
    notes: [Note]
    paymentDate: DateTime
    shippingAddress: Address
    shippingMethod: String
    status: PAYMENT_STATUS
    transactionType: TransactionType
  }

  type ClientOrder @engine(namespace: "${engine}", collection: "client_orders") {
    _id: String
    billingAddress: Address
    amount: String
    client: Account
    clientCreditNotes: [ClientCreditNote]
    clientInvoices: [ClientInvoice]
    date: DateTime
    discount: String
    entity: Entity
    notes: [Note]
    quantity: String
    shippingAddress: Address
    shippingMethod: String
    transactionType: TransactionType
  }

  type Currency @engine(namespace: "${engine}", collection: "currency"){
    code: String
    entity: Entity
    name: String
    symbol: String
  }

  type LedgerAccount @engine(namespace: "${engine}", collection: "ledger_account") {
    _id: String
    description: String
    entity: Entity
    name: String
    notes: [Note]
    status: ACTIVE_STATUS
  }

  type GeneralLedger  @engine(namespace: "${engine}", collection: "posting") {
    _id: String
    category: String
    credit: Int
    account: LedgerAccount
    currency: String
    debit: Int
    description: String
    discount: Int
    entity: Entity
    reference: String
    #transaction: Transaction
    type: TransactionType
  }

  type SupplierCreditNote @engine(namespace: "${engine}", collection: "supplier_credit_notes") {
    _id: String
    billingAdders: Address
    amount: Int
    date: DateTime
    entity: Entity
    notes: [Note]
    posting: GeneralLedger
    shippingAddress: Address
    shippingMethod: String
    status: PAYMENT_STATUS
    supplier: Account
    supplierInvoices: [SupplierInvoice]
    supplierOrders: [SupplierOrder]
    transactionType: TransactionType
  }

  type SupplierInvoice @engine(namespace: "${engine}", collection: "supplier_invoices") {
    _id: String
    billingAddress: Address
    date: DateTime
    entity: Entity
    notes: [Note]
    orderId: String
    paymentDate: DateTime
    posting: GeneralLedger
    shippingAddress: String
    shippingMethod: String
    status: PAYMENT_STATUS
    supplier: Account
    supplierCreditNotes: [SupplierCreditNote]
    supplierOrders: [SupplierOrder]
    transactionType: TransactionType
  }

  type SupplierOrder @engine(namespace: "${engine}", collection: "supplier_orders") {
    _id: String
    amount: String
    billingAddress: Address
    date: DateTime
    discount: String
    entity: Entity
    notes: [Note]
    quantity: String
    shippingAddress: String
    shippingMethod: String
    status: PAYMENT_STATUS
    supplier: Account
    supplierCreditNotes: [SupplierCreditNote]
    supplierInvoices: [SupplierInvoice]
    transactionType: TransactionType
    paymentDate: DateTime
  }

  type TransactionType @engine(namespace: "${engine}", collection: "transaction_type") {
    _id: String
    code: String
    description: String
    entity: Entity
    ledgerAccount: LedgerAccount
    value: String
  }

  type Vat @engine(namespace: "${engine}", collection: "vat") {
    _id: String
    code: String
    description: String
    entity: Entity
    value: String
  }
`
}
