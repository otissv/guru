export function resolver({ app, config }) {
  return {
    Query: {
      postFindOne: (obj, args, context, info) => ({ id: '1', title: 'Post1' }),
      categoryFindOne: (obj, args, context, info) => ({ id: '1', name: 'Category1' }),
      commentFindOne: (obj, args, context, info) => ({ id: '1', content: 'Comment!' }),
    },
  }
}
