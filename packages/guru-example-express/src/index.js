import cors from 'cors'
import morgan from 'morgan'
import { ExtractJwt } from 'passport-jwt'

import { AuthPlugin, authenticate } from 'guru-auth'
import { ModulesPlugin } from 'guru-modules'
import { MongodbPlugin } from 'guru-mongodb'
import { middleware } from 'guru-middleware'
import { guruServer } from 'guru-server'
import { FakePlugin } from 'guru-fake'

import { seed } from './seed/mongodb.seed'

const typeDefs = `
  # This "Book" type can be used in other type declarations.
  type Book {
    title: String
    author: String
  }

  # The "Query" type is the root of all GraphQL queries.
  # (A "Mutation" type will be covered later on.)
  type Query {
    books: [Book]
  }

  type Project @engine(namespace: "mongodb", collection: "projects") {
    _id: String
    name: String
    endpoint: String
    params: String
    headers: String
  }
`

const resolvers = {
  // Transaction: {
  //   __resolveType(parent, context, info) {
  //     switch (parent.type) {
  //       case 'CASH_SALE':
  //         return 'CashSale'
  //       case 'CLIENT_CREDIT_NOTE':
  //         return
  //       case 'ClientCreditNote':
  //         return
  //       case 'CLIENT_INVOICE':
  //         return 'ClientInvoice'
  //       case 'SUPPLIER_CREDIT_NOTE':
  //         return 'SupplierCreditNote'
  //       case 'SUPPLIER_INVOICE':
  //         return 'SupplierInvoice'
  //       default:
  //         return null
  //     }
  //   },
  // },

  Query: {
    books: (parent, args, context, info) => {
      console.log(context)
      return [
        {
          id: 1,
          title: 'Harry Potter and the Chamber of Secrets',
          author: 'J.K. Rowling',
        },
        {
          id: 2,
          title: 'Jurassic Park',
          author: 'Michael Crichton',
        },
      ]
    },
  },

  // Hooks: {
  //   beforeAll: (parent, args, context, info) => {},
  //   afterAll: (parent, args, context, info) => {},
  //   beforeBooks: (parent, args, context, info) => {},
  //   afterBooks: (parent, args, context, info) => {},
  // },
}

const config = {
  graphql: [
    {
      route: '/graphql',
      config: {
        typeDefs: [typeDefs],
        resolvers: [resolvers],
        // context: args => ({}),
        options: {
          // debug: process.env.DEBUG,
          // tracing: process.env.DEBUG,
        },
      },
      middleware: [
        (req, res, next) => {
          // console.log(req)
          next()
        },
        // authenticate,
      ],
    },
  ],

  plugins: [
    new ModulesPlugin(),
    new AuthPlugin({
      engine: 'mongodb',
    }),
    new MongodbPlugin({
      namespace: 'mongodb',
      // seed,
      options: {
        uri: process.env.MONGO_URI,
        opts: {
          server: {
            socketOptions: { keepAlive: 1 },
          },
        },
      },
    }),
    new FakePlugin(),
  ],

  routes: ({ app }) => {
    app.get('/', (req, res) => res.json({ hello: 'me' }))
  },

  middleware: ({ app, config, plugins }) => middleware({ app, config, plugins }),

  // remember to include guru-middleware when using auth
  auth: {
    facebook: {
      clientID: process.env.FACEBOOK_APP_ID,
      clientSecret: process.env.FACEBOOK_APP_SECRET,
      callbackURL: process.env.FACEBOOK_CALLBACK_URL,
      options: {},
    },
    github: {
      clientID: process.env.GITHUB_CLIENT_ID,
      clientSecret: process.env.GITHUB_CLIENT_SECRET,
      callbackURL: process.env.GITHUB_CALLBACK_URL,
      options: {},
    },
    // twitter: {
    //   consumerKey: process.env.TWITTER_CONSUMER_KEY,
    //   consumerSecret: process.env.TWITTER_CONSUMER_SECRET,
    //   callbackUrl: process.env.TWITTER_CALLBACK_URL,
    // },
    local: true,
    jwt: {
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    },
  },
}

const server = guruServer(config)
server
  .then(({ start }) => start())
  .catch(error => {
    console.log(new Error(error))
    process.exit(0)
  })
