const faker = require('faker')
const times = require('lodash/fp/times')
const { ObjectId } = require('mongodb')

exports.users = {
  collection: 'users',
  data: times(i => ({
    _id: getId(i),
    lastLoggedInWith: 'facebook',
    lastLogin: null,
    username: `user_${i}`,
    password: `pass${i}`,
    token: `token_${i}`,
    username: `user_${i}`,
    roles: [1],
    auth0Id: null,
    auth0apiId: null,
    azuerId: null,
    bitbucketId: null,
    facebookId: null,
    githubId: null,
    gitlabId: null,
    googleId: null,
    instagramId: null,
    jwtId: null,
    twitterId: null,
    windowsliveId: null,
    age: `1${i}`,
  }))(100),
}

function getId(index) {
  const ids = [
    '5c9b99fa6a4cee62aff2fe58',

    '5c9b99fa6a4cee62aff2fe59',

    '5c9b99fa6a4cee62aff2fe5a',

    '5c9b99fa6a4cee62aff2fe5b',

    '5c9b99fa6a4cee62aff2fe5c',

    '5c9b99fa6a4cee62aff2fe5d',

    '5c9b99fa6a4cee62aff2fe5e',

    '5c9b99fa6a4cee62aff2fe5f',

    '5c9b99fa6a4cee62aff2fe60',

    '5c9b99fa6a4cee62aff2fe61',

    '5c9b99fa6a4cee62aff2fe62',

    '5c9b99fa6a4cee62aff2fe63',

    '5c9b99fa6a4cee62aff2fe64',

    '5c9b99fa6a4cee62aff2fe65',

    '5c9b99fa6a4cee62aff2fe66',

    '5c9b99fa6a4cee62aff2fe67',

    '5c9b99fa6a4cee62aff2fe68',

    '5c9b99fa6a4cee62aff2fe69',

    '5c9b99fa6a4cee62aff2fe6a',

    '5c9b99fa6a4cee62aff2fe6b',

    '5c9b99fa6a4cee62aff2fe6c',

    '5c9b99fa6a4cee62aff2fe6d',

    '5c9b99fa6a4cee62aff2fe6e',

    '5c9b99fa6a4cee62aff2fe6f',

    '5c9b99fa6a4cee62aff2fe70',

    '5c9b99fa6a4cee62aff2fe71',

    '5c9b99fa6a4cee62aff2fe72',

    '5c9b99fa6a4cee62aff2fe73',

    '5c9b99fa6a4cee62aff2fe74',

    '5c9b99fa6a4cee62aff2fe75',

    '5c9b99fa6a4cee62aff2fe76',

    '5c9b99fa6a4cee62aff2fe77',

    '5c9b99fa6a4cee62aff2fe78',

    '5c9b99fa6a4cee62aff2fe79',

    '5c9b99fa6a4cee62aff2fe7a',

    '5c9b99fa6a4cee62aff2fe7b',

    '5c9b99fa6a4cee62aff2fe7c',

    '5c9b99fa6a4cee62aff2fe7d',

    '5c9b99fa6a4cee62aff2fe7e',

    '5c9b99fa6a4cee62aff2fe7f',

    '5c9b99fa6a4cee62aff2fe80',

    '5c9b99fa6a4cee62aff2fe81',

    '5c9b99fa6a4cee62aff2fe82',

    '5c9b99fa6a4cee62aff2fe83',

    '5c9b99fa6a4cee62aff2fe84',

    '5c9b99fa6a4cee62aff2fe85',

    '5c9b99fa6a4cee62aff2fe86',

    '5c9b99fa6a4cee62aff2fe87',

    '5c9b99fa6a4cee62aff2fe88',

    '5c9b99fa6a4cee62aff2fe89',

    '5c9b99fa6a4cee62aff2fe8a',

    '5c9b99fa6a4cee62aff2fe8b',

    '5c9b99fa6a4cee62aff2fe8c',

    '5c9b99fa6a4cee62aff2fe8d',

    '5c9b99fa6a4cee62aff2fe8e',

    '5c9b99fa6a4cee62aff2fe8f',

    '5c9b99fa6a4cee62aff2fe90',

    '5c9b99fa6a4cee62aff2fe91',

    '5c9b99fa6a4cee62aff2fe92',

    '5c9b99fa6a4cee62aff2fe93',

    '5c9b99fa6a4cee62aff2fe94',

    '5c9b99fa6a4cee62aff2fe95',

    '5c9b99fa6a4cee62aff2fe96',

    '5c9b99fa6a4cee62aff2fe97',

    '5c9b99fa6a4cee62aff2fe98',

    '5c9b99fa6a4cee62aff2fe99',

    '5c9b99fa6a4cee62aff2fe9a',

    '5c9b99fa6a4cee62aff2fe9b',

    '5c9b99fa6a4cee62aff2fe9c',

    '5c9b99fa6a4cee62aff2fe9d',

    '5c9b99fa6a4cee62aff2fe9e',

    '5c9b99fa6a4cee62aff2fe9f',

    '5c9b99fa6a4cee62aff2fea0',

    '5c9b99fa6a4cee62aff2fea1',

    '5c9b99fa6a4cee62aff2fea2',

    '5c9b99fa6a4cee62aff2fea3',

    '5c9b99fa6a4cee62aff2fea4',

    '5c9b99fa6a4cee62aff2fea5',

    '5c9b99fa6a4cee62aff2fea6',

    '5c9b99fa6a4cee62aff2fea7',

    '5c9b99fa6a4cee62aff2fea8',

    '5c9b99fa6a4cee62aff2fea9',

    '5c9b99fa6a4cee62aff2feaa',

    '5c9b99fa6a4cee62aff2feab',

    '5c9b99fa6a4cee62aff2feac',

    '5c9b99fa6a4cee62aff2fead',

    '5c9b99fa6a4cee62aff2feae',

    '5c9b99fa6a4cee62aff2feaf',

    '5c9b99fa6a4cee62aff2feb0',

    '5c9b99fa6a4cee62aff2feb1',

    '5c9b99fa6a4cee62aff2feb2',

    '5c9b99fa6a4cee62aff2feb3',

    '5c9b99fa6a4cee62aff2feb4',

    '5c9b99fa6a4cee62aff2feb5',

    '5c9b99fa6a4cee62aff2feb6',

    '5c9b99fa6a4cee62aff2feb7',

    '5c9b99fa6a4cee62aff2feb8',

    '5c9b99fa6a4cee62aff2feb9',

    '5c9b99fa6a4cee62aff2feba',

    '5c9b99fa6a4cee62aff2febb',
  ]

  return ObjectId(ids[index])
}
