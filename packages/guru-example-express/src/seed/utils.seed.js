export function randomNumber({ min, max, interval = 1 }) {
  let r = Math.floor((Math.random() * (max - min + interval)) / interval)
  return r * interval + min
}
