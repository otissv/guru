const faker = require('faker')
const times = require('lodash/fp/times')
const { ObjectId } = require('mongodb')

const { addresses } = require('./addresses.mongodb.seed')
const { companies } = require('./companies.mongodb.seed')
const { emails } = require('./emails.mongodb.seed')
const { jobs } = require('./jobs.mongodb.seed')
const { person } = require('./person.mongodb.seed')
const { phones } = require('./phones.mongodb.seed')
const { roles } = require('./roles.mongodb.seed')
const { user_profiles } = require('./user_profiles.mongodb.seed')
const { users } = require('./users.mongodb.seed')

const { notes } = require('./notes.mongodb.seed')

exports.seed = [
  addresses,
  notes,
  companies,
  emails,
  jobs,
  person,
  phones,
  roles,
  user_profiles,
  users,
]
