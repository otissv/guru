import times from 'lodash/fp/times'

export const data = {
  users: times(i => ({
    id: `${i}`,
    username: `user_${i}`,
    firstName: `firstName_${i}`,
    lastName: `lastName_${i}`,
    age: i + 10,
    addresses: [`${i}`],
  }))(10),

  address: times(i => ({
    id: `${i}`,
    address_1: 'address_1',
    address_2: 'address_2',
    city: 'city',
    country: 'country',
  }))(10),
}
