import React from 'react'

import { GraphqlLogo } from './GraphqlLogo'
import { useGuru } from '../GuruStore'
import { SidebarToolbarLink } from '../Sidebar'
import { useRouter } from 'reusable'

export function SidebarButton({ onClick }) {
  const {
    store: {
      selected: { projects: project = {} },
    },
  } = useGuru()
  const { setRoute } = useRouter()
  const href = `/${project.name}/graphql`

  function handleClick(e) {
    e.preventDefault()
    onClick && onClick(e)

    setRoute(href)
  }

  return (
    <SidebarToolbarLink
      href={href}
      name="graphql"
      onClick={handleClick}
      Component={GraphqlLogo}
      title="Graphql"
    />
  )
}
