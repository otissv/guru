const webpack = require('webpack')
const path = require('path')
const nodeExternals = require('webpack-node-externals')
const WebpackPreBuildPlugin = require('pre-build-webpack')
const SimpleProgressWebpackPlugin = require('simple-progress-webpack-plugin')
const StartServerPlugin = require('start-server-webpack-plugin')

const pkg = require(path.join(process.cwd(), './package.json'))

let devtool
let plugins

const dependencies = [...Object.keys(pkg.dependencies), ...Object.keys(pkg.peerDependencies)]
const env = process.env.WEBPACK_ENV
const output = {
  path: path.join(process.cwd(), './dist'),
  libraryTarget: 'umd',
}
const pluginsShared = [
  new webpack.NamedModulesPlugin(),
  new webpack.NoEmitOnErrorsPlugin(),
  new webpack.DefinePlugin({
    'process.env': { BUILD_TARGET: JSON.stringify('index') },
  }),
]

if (env === 'build') {
  devtool = 'source-map'
  plugins = [...pluginsShared]
  output.filename = 'index.min.js'
} else {
  devtool = 'inline-source-map'
  plugins = [
    ...pluginsShared,
    new webpack.HotModuleReplacementPlugin(),
    new StartServerPlugin('index.js'),
  ]
  output.filename = 'index.js'
}

module.exports = {
  devtool,
  plugins,
  output,
  entry: ['webpack/hot/poll?1000', './src/index'],
  watch: true,
  target: 'node',
  mode: 'development',
  node: {
    __filename: true,
    __dirname: true,
  },
  externals: [nodeExternals({ whitelist: ['webpack/hot/poll?1000'] })],
  module: {
    rules: [
      {
        test: /\.js?$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              babelrc: true,
            },
          },
        ],
        exclude: /node_modules/,
      },
    ],
  },
}
