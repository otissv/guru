import '../helpers/dotenvTest'

import { MongodbClient } from 'guru-mongodb'
import request from 'request-promise'
import { seed } from '../helpers/fixtures/graphql.mongodb.fixtures'
import { userFragment } from '../helpers/fragments/user-fragment'

const GRAPHQL_URL = process.env.GRAPHQL_URL
const MONGODB = process.env.MONGODB

const post = query =>
  request.post({
    uri: GRAPHQL_URL,
    body: { query: query },
    json: true,
  })

describe('Mongodb Engines', () => {
  /* == Engine == */
  it('Engines return stats and collection', async () => {
    const query = `{
      engines {
        name
        stats {
          collections
          views
          objects
          numExtents
          indexes
          indexSize
          fsUsedSize
          fsTotalSize
          ok
        }
        collections {
          name
          indexes
          count
        }
      }
    }
    `
    const response = await post(query)
    const actual = response.data.engines[0].collections.sort((a, b) =>
      a.name > b.name ? 1 : b.name > a.name ? -1 : 0
    )

    const expected = [
      {
        name: 'addresses',
        indexes: [{ _id: 1 }],
        count: 100,
      },
      {
        name: 'companies',
        indexes: [{ _id: 1 }, { value: 1 }],
        count: 100,
      },
      {
        name: 'emails',
        indexes: [{ _id: 1 }],
        count: 100,
      },
      {
        name: 'jobs',
        indexes: [{ _id: 1 }],
        count: 100,
      },
      {
        name: 'person',
        indexes: [{ _id: 1 }],
        count: 100,
      },
      {
        name: 'phones',
        indexes: [{ _id: 1 }],
        count: 100,
      },
      {
        name: 'roles',
        indexes: [{ _id: 1 }],
        count: 5,
      },
      {
        name: 'user_profiles',
        indexes: [{ _id: 1 }],
        count: 100,
      },
      {
        name: 'users',
        indexes: [{ _id: 1 }, { username: 1 }],
        count: 100,
      },
    ]

    expect(actual).toEqual(expected)
  })

  it('Engines queries engine and collection', async () => {
    const query = `{
      engines (name : "mongodb_test") {
        name
        stats {
          collections
          views
          objects
          numExtents
          indexes
          indexSize
          fsUsedSize
          fsTotalSize
          ok
        }
        collections (name: "roles") {
          name
          indexes
          count
         data (projection: {id: 1})
        }
      }
    }
    `
    const response = await post(query)
    const actual = response.data.engines

    const expected = [
      {
        collections: [
          {
            count: 5,
            data: [
              { id: '5c9ba222139ae94b6c2ca2d0' },
              { id: '5c9ba222139ae94b6c2ca2d1' },
              { id: '5c9ba222139ae94b6c2ca2d2' },
              { id: '5c9ba222139ae94b6c2ca2d3' },
              { id: '5c9ba222139ae94b6c2ca2d4' },
            ],
            indexes: [{ _id: 1 }],
            name: 'roles',
          },
        ],
        name: 'mongodb_test',
        stats: {
          collections: 9,
          fsTotalSize: null,
          fsUsedSize: null,
          indexSize: 42005,
          indexes: 11,
          numExtents: 0,
          objects: 805,
          ok: 1,
          views: 0,
        },
      },
    ]

    expect(actual).toEqual(expected)
  })
})
