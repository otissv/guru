import '../helpers/dotenvTest'

import { URL } from 'url'
import fs from 'fs'
import path from 'path'
import request from 'request'
import { userFragment } from '../helpers/fixtures/accounts.fixture'
import util from 'util'

const requestGet = util.promisify(request.get)
const post = util.promisify(request.post)

const GRAPHQL_URL = process.env.GRAPHQL_URL

const graphqlOptions = {
  url: GRAPHQL_URL,
  form: {
    query: `
   query {
    userFindMany {
      ...userFields
    }
  }
  ${userFragment}
  `,
  },
}

// test('User - findMany returns documents', async () => {
//   const body = await post(graphqlOptions).then(response => response.body)
//   const actual = JSON.parse(body)
//   const expected = {
//     data: {
//       userFindMany: [
//         {
//           id: '5a2309733166ce40e052483f',
//         },
//         {
//           id: '5a285b308441296f1371ee83',
//         },
//       ],
//     },
//   }
//   expect(actual).toEqual(expected)
// })
