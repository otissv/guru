exports.userFragment = ` 
  fragment userFields on User {
    id: String
    username: String
    firstName: String
    lastName: String
    age: Int
    role: [String]
    COUNT: COUNT
  }
`
