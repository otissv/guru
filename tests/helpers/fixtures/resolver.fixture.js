import { ObjectId } from 'mongodb'

export const dataIds = [
  {
    // 0.
    id: '57509b5f350a10fb44e4c2b5',
  },
  {
    // 1.
    id: '57509b5f350a10fb44e4c2b7',
  },
  {
    // 2.
    id: '5758078ba2bb7ddb1444096e',
  },
  {
    // 3.
    id: '5758078ba2bb7ddb1444096f',
  },
  {
    // 4.
    id: '5758078ba2bb7ddb14440970',
  },
  {
    id: '5758078bf5048b981206a0e2',
  },
  {
    // 6.
    id: '5758078bf5048b981206a0e3',
  },
  {
    // 7.
    id: '57509b5f350a10fb44e4c2b3',
  },
  {
    // 8.
    id: '5758078ba2bb7ddb1444096a',
  },
  {
    // 9.
    id: '5758078ba2bb7ddb14440979',
  },
  {
    // 10.
    id: '5758078ba2bb7ddb14440971',
  },
  {
    // 11.
    id: '5758078ba2bb7ddb14440972',
  },
  {
    // 12.
    id: '5758078ba2bb7ddb14440973',
  },
  {
    // 13.
    id: '5758078ba2bb7ddb14440974',
  },
  {
    // 14.
    id: '5758078ba2bb7ddb14440975',
  },
  {
    // 15.
    id: '5758078ba2bb7ddb14440976',
  },
  {
    // 16.
    id: '5758078ba2bb7ddb14440977',
  },
  {
    // 17.
    id: '5758078ba2bb7ddb14440961',
  },
  {
    // 18.
    id: '5758078ba2bb7ddb14440962',
  },
  {
    // 19.
    id: '5758078ba2bb7ddb14440963',
  },
  {
    // 20.
    id: '5758078ba2bb7ddb14440964',
  },
  {
    // 21.
    id: '5758078ba2bb7ddb14440965',
  },
  {
    // 22.
    id: '5758078ba2bb7ddb14440966',
  },
]

export const data = dataIds.map((item, index) => ({
  _id: ObjectId(item.id),
  age: index + 1,
  username: `user_${index + 1}`,
  email: `user_${index + 1}@email.com`,
  roles: ['ADMIN'],
}))
