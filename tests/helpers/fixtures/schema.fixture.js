exports.schema = ({ collection, namespace }) => `

type User @engine(namespace: ${namespace ||
  'mongodb'}, collection: "${collection}") {
  id: String
  username: String
  firstName: String 
  lastName: String
  age: Int
  role: Role
  COUNT: COUNT
}
  
type Role @engine(namespace: ${namespace ||
  'mongodb'}, collection: "${collection}") {
  id: String
  name: String
  description: String
}
`
