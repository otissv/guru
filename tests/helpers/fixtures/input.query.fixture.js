import { ObjectId } from 'mongodb'

export const dataIds = [
  {
    // 0.
    id: '57509b5f350a10fb44e4c2b5',
    qty: 10,
  },
  {
    // 1.
    id: '57509b5f350a10fb44e4c2b7',
    qty: 20,
  },
  {
    // 2.
    id: '5758078ba2bb7ddb1444096e',
    qty: 25,
  },
  {
    // 3.
    id: '5758078ba2bb7ddb1444096f',
    qty: 30,
  },
  {
    // 4.
    id: '5758078ba2bb7ddb14440970',
    qty: 10,
  },
  {
    id: '5758078bf5048b981206a0e2',
    qty: 20,
  },
  {
    // 6.
    id: '5758078bf5048b981206a0e3',
    qty: 25,
  },
  {
    // 7.
    id: '57509b5f350a10fb44e4c2b3',
    qty: 30,
  },
  {
    // 8.
    id: '5758078ba2bb7ddb1444096a',
    qty: 10,
  },
  {
    // 9.
    id: '5758078ba2bb7ddb14440979',
    qty: 20,
  },
  {
    // 10.
    id: '5758078ba2bb7ddb14440971',
    qty: 25,
  },
  {
    // 11.
    id: '5758078ba2bb7ddb14440972',
    qty: 30,
  },
  {
    // 12.
    id: '5758078ba2bb7ddb14440973',
    qty: 10,
  },
  {
    // 13.
    id: '5758078ba2bb7ddb14440974',
    qty: 20,
  },
  {
    // 14.
    id: '5758078ba2bb7ddb14440975',
    qty: 25,
  },
  {
    // 15.
    id: '5758078ba2bb7ddb14440976',
    qty: 30,
  },
  {
    // 16.
    id: '5758078ba2bb7ddb14440977',
    qty: 10,
  },
  {
    // 17.
    id: '5758078ba2bb7ddb14440961',
    qty: 20,
  },
  {
    // 18.
    id: '5758078ba2bb7ddb14440962',
    qty: 25,
  },
  {
    // 19.
    id: '5758078ba2bb7ddb14440963',
    qty: 30,
  },
  {
    // 20.
    id: '5758078ba2bb7ddb14440964',
    qty: 10,
  },
  {
    // 21.
    id: '5758078ba2bb7ddb14440965',
    qty: 20,
  },
  {
    // 22.
    id: '5758078ba2bb7ddb14440966',
    qty: 25,
  },
  {
    // 23.
    id: '5758078ba2bb7ddb14440967',
    qty: 30,
  },
]

export const data = dataIds.map((item, index) => ({
  _id: ObjectId(item.id),
  age: index + 1,
  username: `user_${index + 1}`,
  email: `user_${index + 1}@email.com`,
  roles: ['ADMIN'],
}))
