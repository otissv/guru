import { ObjectId } from 'mongodb'
import times from 'lodash/fp/times'
export const data = [
  {
    // 0.
    id: '57509b5f350a10fb44e4c2b5',
  },
  {
    // 1.
    id: '57509b5f350a10fb44e4c2b7',
  },
  {
    // 2.
    id: '5758078ba2bb7ddb1444096e',
  },
  {
    // 3.
    id: '5758078ba2bb7ddb1444096f',
  },
  {
    // 4.
    id: '5758078ba2bb7ddb14440970',
  },
  {
    id: '5758078bf5048b981206a0e2',
  },
  {
    // 6.
    id: '5758078bf5048b981206a0e3',
  },
  {
    // 7.
    id: '57509b5f350a10fb44e4c2b3',
  },
  {
    // 8.
    id: '5758078ba2bb7ddb1444096a',
  },
  {
    // 9.
    id: '5758078ba2bb7ddb14440979',
  },
  {
    // 10.
    id: '5758078ba2bb7ddb14440971',
  },
  {
    // 11.
    id: '5758078ba2bb7ddb14440972',
  },
  {
    // 12.
    id: '5758078ba2bb7ddb14440973',
  },
  {
    // 13.
    id: '5758078ba2bb7ddb14440974',
  },
  {
    // 14.
    id: '5758078ba2bb7ddb14440975',
  },
  {
    // 15.
    id: '5758078ba2bb7ddb14440976',
  },
  {
    // 16.
    id: '5758078ba2bb7ddb14440977',
  },
  {
    // 17.
    id: '5758078ba2bb7ddb14440961',
  },
  {
    // 18.
    id: '5758078ba2bb7ddb14440962',
  },
  {
    // 19.
    id: '5758078ba2bb7ddb14440963',
  },
  {
    // 20.
    id: '5758078ba2bb7ddb14440964',
  },
  {
    // 21.
    id: '5758078ba2bb7ddb14440965',
  },
  {
    // 22.
    id: '5758078ba2bb7ddb14440966',
  },
]

export const seed = [
  {
    collection: 'accounts',
    indexs: { username: 1 },
    data: times(i => ({
      _id: ObjectId(data[i].id),
      lastLoggedInWith: 'facebook',
      lastLogin: null,
      password: `pass${i}`,
      roles: [1],
      token: `token_${i}`,
      username: `user_${i}`,
      auth0Id: null,
      auth0apiId: null,
      azuerId: null,
      bitbucketId: null,
      facebookId: null,
      githubId: null,
      gitlabId: null,
      googleId: null,
      instagramId: null,
      jwtId: null,
      twitterId: null,
      windowsliveId: null,
    }))(20),
  },

  {
    collection: 'addresses',
    silent: true,
    data: times(i => ({
      _id: ObjectId(data[i].id),
      address_1: 'address_1',
      address_2: 'address_2',
      city: 'city',
      country: 'country',
    }))(20),
  },

  {
    collection: 'users',
    indexs: { username: 1 },
    data: times(i => ({
      _id: ObjectId(data[i].id),
      username: `user_${i}`,
      firstName: `firstName_${i}`,
      lastName: `lastName_${i}`,
      age: i + 10,
      addresses: [`${i}`],
    }))(20),
  },
]
