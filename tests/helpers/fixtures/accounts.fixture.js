import { ObjectId } from 'mongodb'

export const dataIds = [
  {
    _id: ObjectId('57509b5f350a10fb44e4c2b5'),
  },
  {
    _id: ObjectId('57509b5f350a10fb44e4c2b7'),
  },
  {
    _id: ObjectId('5758078ba2bb7ddb1444096e'),
  },
  {
    _id: ObjectId('5758078ba2bb7ddb1444096f'),
  },
  {
    _id: ObjectId('5758078ba2bb7ddb14440970'),
  },
  {
    _id: ObjectId('5758078bf5048b981206a0e2'),
  },
]

export const data = [
  {
    _id: ObjectId('57509b5f350a10fb44e4c2b5'),
    age: 1,
    username: 'a',
    email: 'a@email.com',
    roles: ['ADMIN'],
  },
  {
    _id: ObjectId('57509b5f350a10fb44e4c2b7'),
    age: 2,
    username: 'b',
    email: 'v@email.com',
  },
  {
    _id: ObjectId('5758078ba2bb7ddb1444096e'),
    age: 3,
    username: 'c',
    email: 'c@email.com',
  },
  {
    _id: ObjectId('5758078ba2bb7ddb1444096f'),
    age: 4,
    username: 'd',
    email: 'd@email.com',
  },
  {
    _id: ObjectId('5758078ba2bb7ddb14440970'),
    age: 5,
    username: 'e',
    email: 'e@email.com',
  },
  {
    _id: ObjectId('5758078bf5048b981206a0e2'),
    age: 6,
    username: 'f',
    email: 'f@email.com',
  },
  {
    _id: ObjectId('5758078ba2bb7ddb14440961'),
    age: 7,
    username: 'g',
    email: 'g@email.com',
  },
  {
    _id: ObjectId('5758078ba2bb7ddb14440971'),
    age: 8,
    username: 'h',
    email: 'e@email.com',
  },
  {
    _id: ObjectId('5758078bf5048b981206a0e1'),
    age: 9,
    username: 'i',
    email: 'f@email.com',
  },
]
