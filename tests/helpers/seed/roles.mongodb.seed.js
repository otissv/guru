const faker = require('faker')
const times = require('lodash/fp/times')
const { ObjectId } = require('mongodb')

exports.roles = {
  collection: 'roles',
  data: times(i => ({
    _id: getId(i),
    name: `ROLE_${i}`,
    description: ` Role ${i}`,
  }))(5),
}

function getId(index) {
  const ids = [
    '5c9ba222139ae94b6c2ca2d0',
    '5c9ba222139ae94b6c2ca2d1',
    '5c9ba222139ae94b6c2ca2d2',
    '5c9ba222139ae94b6c2ca2d3',
    '5c9ba222139ae94b6c2ca2d4',
  ]

  return ObjectId(ids[index])
}
