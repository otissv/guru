const faker = require('faker')
const times = require('lodash/fp/times')
const { ObjectId } = require('mongodb')

exports.user_profiles = {
  collection: 'user_profiles',
  data: times(i => ({
    _id: getId(i),
    user: i,
    addresses: [i],
    avatar: faker.image.avatar(),
    bio: faker.lorem.sentence(),
    displayName: faker.name.firstName(),
    emails: [1],
    name: [i],
    photos: [
      faker.image.imageUrl(),
      faker.image.imageUrl(),
      faker.image.imageUrl(),
    ],
    website: faker.internet.url(),
    phoneNumbers: [i],
    organization: [i],
  }))(100),
}

function getId(index) {
  const ids = [
    '5c9ba223139ae94b6c2ca339',
    '5c9ba223139ae94b6c2ca33a',
    '5c9ba223139ae94b6c2ca33b',
    '5c9ba223139ae94b6c2ca33c',
    '5c9ba223139ae94b6c2ca33d',
    '5c9ba223139ae94b6c2ca33e',
    '5c9ba223139ae94b6c2ca33f',
    '5c9ba223139ae94b6c2ca340',
    '5c9ba223139ae94b6c2ca341',
    '5c9ba223139ae94b6c2ca342',
    '5c9ba223139ae94b6c2ca343',
    '5c9ba223139ae94b6c2ca344',
    '5c9ba223139ae94b6c2ca345',
    '5c9ba223139ae94b6c2ca346',
    '5c9ba223139ae94b6c2ca347',
    '5c9ba223139ae94b6c2ca348',
    '5c9ba223139ae94b6c2ca349',
    '5c9ba223139ae94b6c2ca34a',
    '5c9ba223139ae94b6c2ca34b',
    '5c9ba223139ae94b6c2ca34c',
    '5c9ba223139ae94b6c2ca34d',
    '5c9ba223139ae94b6c2ca34e',
    '5c9ba223139ae94b6c2ca34f',
    '5c9ba223139ae94b6c2ca350',
    '5c9ba223139ae94b6c2ca351',
    '5c9ba223139ae94b6c2ca352',
    '5c9ba223139ae94b6c2ca353',
    '5c9ba223139ae94b6c2ca354',
    '5c9ba223139ae94b6c2ca355',
    '5c9ba223139ae94b6c2ca356',
    '5c9ba223139ae94b6c2ca357',
    '5c9ba223139ae94b6c2ca358',
    '5c9ba223139ae94b6c2ca359',
    '5c9ba223139ae94b6c2ca35a',
    '5c9ba223139ae94b6c2ca35b',
    '5c9ba223139ae94b6c2ca35c',
    '5c9ba223139ae94b6c2ca35d',
    '5c9ba223139ae94b6c2ca35e',
    '5c9ba223139ae94b6c2ca35f',
    '5c9ba223139ae94b6c2ca360',
    '5c9ba223139ae94b6c2ca361',
    '5c9ba223139ae94b6c2ca362',
    '5c9ba223139ae94b6c2ca363',
    '5c9ba223139ae94b6c2ca364',
    '5c9ba223139ae94b6c2ca365',
    '5c9ba223139ae94b6c2ca366',
    '5c9ba223139ae94b6c2ca367',
    '5c9ba223139ae94b6c2ca368',
    '5c9ba223139ae94b6c2ca369',
    '5c9ba223139ae94b6c2ca36a',
    '5c9ba223139ae94b6c2ca36b',
    '5c9ba223139ae94b6c2ca36c',
    '5c9ba223139ae94b6c2ca36d',
    '5c9ba223139ae94b6c2ca36e',
    '5c9ba223139ae94b6c2ca36f',
    '5c9ba223139ae94b6c2ca370',
    '5c9ba223139ae94b6c2ca371',
    '5c9ba223139ae94b6c2ca372',
    '5c9ba223139ae94b6c2ca373',
    '5c9ba223139ae94b6c2ca374',
    '5c9ba223139ae94b6c2ca375',
    '5c9ba223139ae94b6c2ca376',
    '5c9ba223139ae94b6c2ca377',
    '5c9ba223139ae94b6c2ca378',
    '5c9ba223139ae94b6c2ca379',
    '5c9ba223139ae94b6c2ca37a',
    '5c9ba223139ae94b6c2ca37b',
    '5c9ba223139ae94b6c2ca37c',
    '5c9ba223139ae94b6c2ca37d',
    '5c9ba223139ae94b6c2ca37e',
    '5c9ba223139ae94b6c2ca37f',
    '5c9ba223139ae94b6c2ca380',
    '5c9ba223139ae94b6c2ca381',
    '5c9ba223139ae94b6c2ca382',
    '5c9ba223139ae94b6c2ca383',
    '5c9ba223139ae94b6c2ca384',
    '5c9ba223139ae94b6c2ca385',
    '5c9ba223139ae94b6c2ca386',
    '5c9ba223139ae94b6c2ca387',
    '5c9ba223139ae94b6c2ca388',
    '5c9ba223139ae94b6c2ca389',
    '5c9ba223139ae94b6c2ca38a',
    '5c9ba223139ae94b6c2ca38b',
    '5c9ba223139ae94b6c2ca38c',
    '5c9ba223139ae94b6c2ca38d',
    '5c9ba223139ae94b6c2ca38e',
    '5c9ba223139ae94b6c2ca38f',
    '5c9ba223139ae94b6c2ca390',
    '5c9ba223139ae94b6c2ca391',
    '5c9ba223139ae94b6c2ca392',
    '5c9ba223139ae94b6c2ca393',
    '5c9ba223139ae94b6c2ca394',
    '5c9ba223139ae94b6c2ca395',
    '5c9ba223139ae94b6c2ca396',
    '5c9ba223139ae94b6c2ca397',
    '5c9ba223139ae94b6c2ca398',
    '5c9ba223139ae94b6c2ca399',
    '5c9ba223139ae94b6c2ca39a',
    '5c9ba223139ae94b6c2ca39b',
    '5c9ba223139ae94b6c2ca39c',
  ]

  return ObjectId(ids[index])
}
