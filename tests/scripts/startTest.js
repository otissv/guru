require('../helpers/dotenvTest')
const { spawn } = require('child_process')

require('./serverTest')

spawn('jest --watchAll --notify --no-cache', {
  stdio: 'inherit',
  shell: true,
})
