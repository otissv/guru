const { exec, spawn } = require('child_process')

exec('mkdir -p $HOME/.gurutest', err => {
  if (err) {
    console.error(err)
  }
})

const userHome =
  process.env[process.platform == 'win32' ? 'USERPROFILE' : 'HOME']

const pipe = spawn('mongod', [`--dbpath=${userHome}/.gurudata`])

pipe.stdout.on('data', data => {
  let verbose = false
  if (verbose) {
    console.log(data.toString('utf8'))
  } else {
    const str = data.toString('utf8')
    const port = str.match(/port=\w[0-9]*/g)
      ? str.match(/port=\w[0-9]*/g)[0]
      : null
    const shutdown = str.match(/shutting down.*/g)
      ? str.match(/shutting down.*/g)
      : null

    if (port) {
      console.log(`Started MongoDB server on port ${port.split('=')[1]}`)
    }

    if (shutdown) {
      console.log(`MongoDB ${shutdown}`)
    }
  }
})

pipe.stderr.on('data', data => {
  console.log(data.toString('utf8'))
})

pipe.on('close', code => {
  console.log('Process exited with code: ' + code)
})
