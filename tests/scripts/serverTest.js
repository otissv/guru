const cors = require('cors')
const auth = require('guru-auth')
const morgan = require('morgan')
const { ModulesPlugin } = require('guru-modules')
const { MongodbPlugin } = require('guru-mongodb')
const { MongodbMemoryPlugin } = require('guru-mongodb-memory')
const { guruServer } = require('guru-server')
const { middleware } = require('guru-middleware')
const { parse } = require('graphql')
const { seed } = require('../helpers/seed/mongodb.seed')

const { AccountsPlugin, config: authConfig } = auth

const engine = 'mongodb_test'

const typeDefs = `
  # Comments in GraphQL are defined with the hash (#) symbol.

  # This "Book" type can be used in other type declarations.
  type Book {
    title: String
    author: String
  }

  # Address type
    type Address @engine(namespace: "${engine}", collection: "addresses") {
      # Address's unique identifier
      id: String
      address_1: String
      address_2: String
      city: String
      county: String
      country: String
      zipCode: String
    }

    # Company type
    type Company @engine(namespace: "${engine}", collection: "companies") {
      # Company's unique identifier
      id: String
      name: String
      addresses: [Address]
    }

    # Email type
    type Emails @engine(namespace: "${engine}", collection: "emails") {
      # Email's unique identifier
      id: String
      # Email address
      value: Email
      # Email type
      type: String
      # Meta Data
      META: META
      # Request results
      RESULTS: RESULTS
      # Count number of items
      COUNT: COUNT
    }

    # Job type
    type Job @engine(namespace: "${engine}", collection: "jobs") {
      # Job's unique identifier
      id: String
      # Job area
      area: String
      # Job description 
      description: String
      # Job title
      title: String
      # Job type
      type: String
      # Meta Data
      META: META
      # Request results
      RESULTS: RESULTS
      # Count number of items
      COUNT: COUNT
    }

    # Phone Type
    type Phones @engine(namespace: "${engine}", collection: "emails") {
      # Phone's unique identifier
      id: String
      # Email address
      value: String
      # Email type
      type: String
      # Meta Data
      META: META
      # Request results
      RESULTS: RESULTS
      # Count number of items
      COUNT: COUNT
    }

    # Create permission type
    type Permission @engine(namespace: "${engine}", collection: "permissions") {
      # Permission's unique identifier
      id: String
      # Resolver operation name.
      operation: String
      # Authorized roles for permission.
      roles: [Role]
      # Count number of items
      # Request results
      RESULTS: RESULTS
      # Count number of items
      COUNT: COUNT
      # Meta Data
      META: META
    }

    # Person Type
    type Person  @engine(namespace: "${engine}", collection: "person") {
      # Person's unique identifier
      id: String
      # Person's family name.
      familyName: String
      # Person's given name.
      givenName: String
      # Person's middle name.
      middleName: String
      # Meta Data
      META: META
      # Request results
      RESULTS: RESULTS
      # Count number of items
      COUNT: COUNT
    }

    # Role Type
    type Role @engine(namespace: "${engine}", collection: "roles") {
      # Role's unique identifier
      id: String
      # Authorized role.
      name: String
      # Roles description.
      description: String
      # Meta Data
      META: META
      # Request results
      RESULTS: RESULTS
      # Count number of items
      COUNT: COUNT
    }

    # User Type
    type User @engine(namespace: "${engine}", collection: "users") {
      # User's unique identifier
      id: String
      # Time and date of the last time a user logged in.
      lastLogin: String
      # The provider the last user used to log in.
      lastLoggedInWith: String
      # User's encrypted password.
      password: String
      # User's username
      username: String
      # Authenticated encrypted token.
      token: String
      # List of user's roles.
      roles: [Role]
      # Auth0 ID
      auth0Id: [String]
      # Azuer ID
      azuerId: String
      # Bitbucket ID
      bitbucketId: String
      # Auth0 API ID
      auth0apiId: String
      # Facebook ID
      facebookId: String
      # GitHub ID
      githubId: String
      # Gitlab ID
      gitlabId: String
      # Google ID
      googleId: String
      # Instagram ID
      instagramId: String
      # Jwt ID
      jwtId: String
      # windowslive ID
      windowsliveId: String
      # twitter ID
      twitterId: String
      # Meta Data
      META: META
      # Request results
      RESULTS: RESULTS
      # Count number of items
      COUNT: COUNT
    }

    # User Profile type
    type UserProfile @engine(namespace: "${engine}", collection: "user_profiles") {
      # User's profile unique identifier
      id: String
      # User addresses
      addresses: [Address]
      # User company
      company: Company
      # User Avatar
      avatar: String
      # User's ID
      user: User
      # User bio
      bio: String
      # User's display name
      displayName: String
      # User's email address
      emails: [Emails]
      # User's name
      name: Person
      # URL to user's photos image.
      photos: [String]
      # User's website.
      website: String
      # Users phone numbers
      phoneNumbers: [Phone]
      # User's organization
      organization: String
      # User's location
      location: String
      # Meta Data
      META: META
      # Request results
      RESULTS: RESULTS
      # Count number of items
      COUNT: COUNT
    }



  # The "Query" type is the root of all GraphQL queries.
  type Query {
    books: [Book]
    book: Book
  }

`

const resolvers = {
  Query: {
    books: (parent, args, context, info) => {
      return [
        {
          id: 1,
          title: 'Harry Potter and the Chamber of Secrets',
          author: 'J.K. Rowling',
        },
        {
          id: 2,
          title: 'Jurassic Park',
          author: 'Michael Crichton',
        },
      ]
    },
    book: (parent, args, context, info) => {
      console.log(args)
      return {
        id: 1,
        title: 'Harry Potter and the Chamber of Secrets',
        author: 'J.K. Rowling',
      }
    },
  },

  // Hooks: {
  //   beforeAll: (parent, args, context, info) => {},
  //   afterAll: (parent, args, context, info) => {},
  //   beforeBooks: (parent, args, context, info) => {},
  //   afterBooks: (parent, args, context, info) => {},
  // },
}

const config = {
  routes: [
    {
      route: '/graphql',
      auth: {
        facebook: {
          clientID: process.env.FACEBOOK_APP_ID,
          clientSecret: process.env.FACEBOOK_APP_SECRET,
          callbackURL: process.env.FACEBOOK_CALLBACK_URL,
          options: {},
        },
        github: {
          clientID: process.env.GITHUB_CLIENT_ID,
          clientSecret: process.env.GITHUB_CLIENT_SECRET,
          callbackURL: process.env.GITHUB_CALLBACK_URL,
          options: {},
        },
        twitter: {
          consumerKey: process.env.TWITTER_CONSUMER_KEY,
          consumerSecret: process.env.TWITTER_CONSUMER_SECRET,
          callbackUrl: process.env.TWITTER_CALLBACK_URL,
        },
      },
      graphql: {
        typeDefs: [typeDefs],
        resolvers: [resolvers],
        // context: args => ({}),
        options: {
          debug: process.env.DEBUG,
          tracing: process.env.DEBUG,
          introspection: true,
        },
      },
    },
  ],

  plugins: [
    // new ModulesPlugin(),
    // new AccountsPlugin({ engine: 'mongodb', config: authConfig }),
    new MongodbMemoryPlugin({
      namespace: engine,
      seed,
    }),
    // new MongodbPlugin({
    //   namespace: 'mongo_test',
    //   options: {
    //     uri: process.env.MONGODB,
    //     opts: {
    //       server: {
    //         socketOptions: { keepAlive: 1 },
    //       },
    //     },
    //   },
    //   seed,
    // }),
  ],

  middleware: ({ app }) => {
    app.use(cors())
    app.use(morgan('tiny'))
  },
}

const server = guruServer(config)
server
  .then(({ start }) => start())
  .catch(error => {
    console.log(new Error(error))
    process.exit(0)
  })
