import '../helpers/dotenvTest'

import { MongodbClient } from 'guru-mongodb'
import request from 'request-promise'
import { seed } from '../helpers/fixtures/graphql.mongodb.fixtures'
import { userFragment } from '../helpers/fragments/user-fragment'

const GRAPHQL_URL = process.env.GRAPHQL_URL
const MONGODB = process.env.MONGODB

const post = query =>
  request.post({
    uri: GRAPHQL_URL,
    body: { query: query },
    json: true,
  })

describe('Mongodb query', () => {
  /* == Count == */
  it('Count returns number of documents', async () => {
    const query = `{
      userCount {
        COUNT {
          n
        }
      }
    }
    `

    const response = await post(query)
    const actual = response.data.userCount

    expect(actual.COUNT.n).toBe(100)
  })

  /* == Find == */
  it('Find returns all documents in collection', async () => {
    const query = `{
      userFind {
        id
      }
    }
    `

    const response = await post(query)
    const actual = response.data.userFind

    expect(actual.length).toBe(100)
  })

  /* == FindById == */
  it('FindById with no query returns null', async () => {
    const query = `{
      userFindById  {
        id
        username
      }
    }
    `

    const response = await post(query)
    const actual = response.data.userFindById

    expect(actual).toEqual(null)
  })

  it('FindById returns documents', async () => {
    const query = `{
      userFindById(query: { id: ["5c9b99fa6a4cee62aff2fe5a", "5c9b99fa6a4cee62aff2fe59", "5c9b99fa6a4cee62aff2fe58"] }) {
        id
        username
      }
    }
    `

    const response = await post(query)
    const actual = response.data.userFindById

    const expected = [
      {
        id: '5c9b99fa6a4cee62aff2fe58',
        username: 'user_0',
      },
      {
        id: '5c9b99fa6a4cee62aff2fe59',
        username: 'user_1',
      },
      {
        id: '5c9b99fa6a4cee62aff2fe5a',
        username: 'user_2',
      },
    ]

    expect(actual).toEqual(expected)
  })

  /* == FindOne == */
  it('FindOne returns first document', async () => {
    const query = `{
      userFindOne {
        id
        username
      }
    }
      `

    const response = await post(query)
    const actual = response.data.userFindOne

    const expected = {
      id: '5c9b99fa6a4cee62aff2fe58',
      username: 'user_0',
    }

    expect(actual).toEqual(expected)
  })

  it('FindOne returns first matching document', async () => {
    const query = `{
      userFindOne (query: { username: "user_2" }) {
        id
        username
      }
    }
    `

    const response = await post(query)
    const actual = response.data.userFindOne

    const expected = {
      id: '5c9b99fa6a4cee62aff2fe5a',
      username: 'user_2',
    }

    expect(actual).toEqual(expected)
  })

  /* == FindOneById == */
  it('FindOneById returns a document', async () => {
    const query = `{
      userFindOneById(query: { id: "5c9b99fa6a4cee62aff2fe5a" }) {
        id
        username
      }
    }
    `

    const response = await post(query)
    const actual = response.data.userFindOneById

    const expected = {
      id: '5c9b99fa6a4cee62aff2fe5a',
      username: 'user_2',
    }

    expect(actual).toEqual(expected)
  })
})
