import '../helpers/dotenvTest'

import { MongodbClient, mongoDbResolvers } from 'guru-mongodb'

import { data } from '../helpers/fixtures/resolver.fixture'
import { schema } from '../helpers/fixtures/schema.fixture'

let Query
let collectionName = 'test_resolver_queries'
let context
let db
let namespace = 'mongodb'
let resolver
let typeDefs = schema({ collection: collectionName, namespace })

beforeAll(async () => {
  db = new MongodbClient({
    namespace: 'mongodb',
    typeDefs,
    options: {
      uri: process.env.MONGODB,
      opts: {
        server: {
          socketOptions: { keepAlive: 1 },
        },
      },
    },
  })

  await db.client()
  await db.seed({
    collection: `${collectionName}`,
    data,
    indexs: { age: -1 },
    silent: true,
  })

  context = { [namespace]: await db }

  Query = mongoDbResolvers({ context, typeDefs, namespace }).Query
})

test('Mongodb query - Count returns number of documents', async () => {
  const actual = await Query.userCount(null, {}, context)

  expect(actual.COUNT.n).toBe(23)
})

test('Mongodb query resolvers - Standalone Count returns number of documents', async () => {
  const actual = await Query.userCount(null, {}, {})

  expect(actual.COUNT.n).toBe(23)
})

/* == Find == */
test('Mongodb query - Find returns all documents in collection', async () => {
  const actual = await Query.userFind(null, {}, context)

  expect(actual.length).toBe(23)
})

/* == FindById == */
test('Mongodb query - FindById returns a document', async () => {
  const args = {
    query: { id: ['57509b5f350a10fb44e4c2b7'] },
  }
  const actual = await Query.userFindById(null, args, context)

  const expected = [
    {
      id: '57509b5f350a10fb44e4c2b7',
      age: 2,
      email: 'user_2@email.com',
      roles: ['ADMIN'],
      username: 'user_2',
    },
  ]

  expect(actual).toEqual(expected)
})

test('Mongodb query - FindById returns a document', async () => {
  const args = {
    query: { id: ['57509b5f350a10fb44e4c2b7'] },
  }

  const actual = await Query.userFindById(null, args, context)

  const expected = [
    {
      age: 2,
      email: 'user_2@email.com',
      id: '57509b5f350a10fb44e4c2b7',
      roles: ['ADMIN'],
      username: 'user_2',
    },
  ]

  expect(actual).toEqual(expected)
})

/* == FindOne == */
test('Mongodb query - FindOne returns a document', async () => {
  const actual = await Query.userFindOne(null, {}, context)

  const expected = {
    age: 1,
    email: 'user_1@email.com',
    id: '57509b5f350a10fb44e4c2b5',
    roles: ['ADMIN'],
    username: 'user_1',
  }

  expect(actual).toEqual(expected)
})

test('Mongodb query resolvers - FindOne returns a document', async () => {
  const actual = await Query.userFindOne(null, {}, context)

  const expected = {
    age: 1,
    email: 'user_1@email.com',
    id: '57509b5f350a10fb44e4c2b5',
    roles: ['ADMIN'],
    username: 'user_1',
  }

  // expect(actual).toEqual(expected)
})

/* == FindOneById == */
test('Mongodb query - FindOneById returns a document', async () => {
  const args = {
    query: { id: '57509b5f350a10fb44e4c2b5' },
  }
  const actual = await Query.userFindOneById(null, args, context)

  const expected = {
    age: 1,
    email: 'user_1@email.com',
    id: '57509b5f350a10fb44e4c2b5',
    roles: ['ADMIN'],
    username: 'user_1',
  }

  expect(actual).toEqual(expected)
})
