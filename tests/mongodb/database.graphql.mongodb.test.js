import '../helpers/dotenvTest'

import { MongodbClient } from 'guru-mongodb'
import request from 'request-promise'
import { seed } from '../helpers/fixtures/graphql.mongodb.fixtures'
import { userFragment } from '../helpers/fragments/user-fragment'

const GRAPHQL_URL = process.env.GRAPHQL_URL
const MONGODB = process.env.MONGODB

const post = query =>
  request.post({
    uri: GRAPHQL_URL,
    body: { query: query },
    json: true,
  })

describe('Mongodb Collections', () => {
  /* == Find == */
  it('CollectionFind finds all collections', async () => {
    const query = `{
      mongodbTestCollectionFind{
        count
      }
      
    }
    `
    const response = await post(query)
    const actual = response.data.mongodbTestCollectionFind

    expect(actual.length).toBe(9)
  })

  it('CollectionFind finds all collections by name', async () => {
    const query = `{
      mongodbTestCollectionFind (name: "roles") {
        engine
        name
        indexes
        data
        count
      }
    }
    `

    const response = await post(query)
    const actual = response.data.mongodbTestCollectionFind
    const expected = [
      {
        count: 5,
        data: [
          {
            description: ' Role 0',
            id: '5c9ba222139ae94b6c2ca2d0',
            name: 'ROLE_0',
          },
          {
            description: ' Role 1',
            id: '5c9ba222139ae94b6c2ca2d1',
            name: 'ROLE_1',
          },
          {
            description: ' Role 2',
            id: '5c9ba222139ae94b6c2ca2d2',
            name: 'ROLE_2',
          },
          {
            description: ' Role 3',
            id: '5c9ba222139ae94b6c2ca2d3',
            name: 'ROLE_3',
          },
          {
            description: ' Role 4',
            id: '5c9ba222139ae94b6c2ca2d4',
            name: 'ROLE_4',
          },
        ],
        engine: 'mongodb_test',
        indexes: [{ _id: 1 }],
        name: 'roles',
      },
    ]

    expect(actual).toEqual(expected)
  })

  it('CollectionFind projects and queries data id', async () => {
    const query = `{
      mongodbTestCollectionFind (name: "roles") {
        name
        data (query: { name: "ROLE_0" }, projection: { id: 1 })
      }
    }
    `
    const response = await post(query)
    const actual = response.data.mongodbTestCollectionFind
    const expected = [
      {
        name: 'roles',
        data: [
          {
            id: '5c9ba222139ae94b6c2ca2d0',
          },
        ],
      },
    ]

    expect(actual).toEqual(expected)
  })

  /* == Find Documents == */
  it('CollectionFindDocuments finds all documents', async () => {
    const query = `{
      mongodbTestCollectionFindDocuments (collection: "roles") {
        engine
        collection
        data
      }
    }
    `
    const response = await post(query)
    const actual = response.data.mongodbTestCollectionFindDocuments
    const expected = {
      engine: 'mongodb_test',
      collection: 'roles',
      data: [
        {
          _id: '5c9ba222139ae94b6c2ca2d0',
          name: 'ROLE_0',
          description: ' Role 0',
        },
        {
          _id: '5c9ba222139ae94b6c2ca2d1',
          name: 'ROLE_1',
          description: ' Role 1',
        },
        {
          _id: '5c9ba222139ae94b6c2ca2d2',
          name: 'ROLE_2',
          description: ' Role 2',
        },
        {
          _id: '5c9ba222139ae94b6c2ca2d3',
          name: 'ROLE_3',
          description: ' Role 3',
        },
        {
          _id: '5c9ba222139ae94b6c2ca2d4',
          name: 'ROLE_4',
          description: ' Role 4',
        },
      ],
    }

    expect(actual).toEqual(expected)
  })

  it('CollectionFindDocuments finds all documents with query and projection', async () => {
    const query = `{
      mongodbTestCollectionFindDocuments (collection: "roles") {
        collection
        data (query: {id: "5c9ba222139ae94b6c2ca2d1"}, projection:{id: 1})
      }
    }
    `

    const response = await post(query)
    const actual = response.data.mongodbTestCollectionFindDocuments
    const expected = {
      collection: 'roles',
      data: [
        {
          _id: '5c9ba222139ae94b6c2ca2d1',
        },
      ],
    }

    expect(actual).toEqual(expected)
  })

  /* == Stats == */
  it('CollectionStats returns stats', async () => {
    const query = `{
      mongodbTestCollectionStats (name:"roles") {
        size
        count
        avgObjSize
        storageSize
        capped
      }
    }
    `
    const response = await post(query)
    const actual = response.data.mongodbTestCollectionStats
    const expected = {
      size: 320,
      count: 5,
      avgObjSize: 64,
      storageSize: 440,
      capped: false,
    }

    expect(actual).toEqual(expected)
  })
})
