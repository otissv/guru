import '../helpers/dotenvTest'

import { MongodbClient } from 'guru-mongodb'

let client
const MONGODB = process.env.MONGODB

beforeAll(async () => {
  const mongodb = new MongodbClient({
    namespace: 'mongodb',
    options: {
      uri: MONGODB,
      opts: {
        server: {
          socketOptions: { keepAlive: 1 },
        },
      },
    },
  })
  client = await mongodb.client()
})

test('Can connect to mongodb instance', async () => {
  expect(client.s.databaseName).toBe('test')
})
