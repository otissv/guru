import '../helpers/dotenvTest'

import { MongodbClient, queryObjectId } from 'guru-mongodb'

import { ObjectId } from 'mongodb'
import { data } from '../helpers/fixtures/accounts.fixture'

const collectionName = 'test_query_utils'
const MONGODB = process.env.MONGODB

beforeAll(async () => {
  const mongodb = new MongodbClient({
    namespace: 'mongodb',
    options: {
      uri: MONGODB,
      opts: {
        server: {
          socketOptions: { keepAlive: 1 },
        },
      },
    },
  })
  await mongodb.client()
  await mongodb.seed({
    collection: `${collectionName}`,
    data,
    indexs: { age: -1 },
    silent: true,
  })
})

/*
 * Mongodb queryObjectId tests
 */
test('Mongodb queryObjectId returns empty object if args is empty.', async () => {
  const actual = queryObjectId({})
  expect(actual).toEqual({})
})

test('Mongodb queryObjectId returns empty object if no args.', async () => {
  const actual = queryObjectId()
  expect(actual).toEqual({})
})

test('Mongodb queryObjectId returns args with id', async () => {
  const id = '54495ad94c934721ede76d90'
  const actual = queryObjectId({ id, hello: 'world' })

  expect(actual).toEqual({
    _id: ObjectId(id),
    hello: 'world',
  })
})

test('Mongodb queryObjectId returns args with no id', async () => {
  const actual = queryObjectId({ hello: 'world' })

  expect(actual).toEqual({
    hello: 'world',
  })
})

test('Mongodb queryObjectId returns args with array', async () => {
  const id_0 = '57509b5f350a10fb44e4c2b5'
  const id_1 = '57509b5f350a10fb44e4c2b7'
  const actual = queryObjectId({ $in: [{ id: id_0 }, { id: id_1 }] })

  expect(actual).toEqual({
    $in: [{ _id: ObjectId(id_0) }, { _id: ObjectId(id_1) }],
  })
})

test('Mongodb queryObjectId ignores args with that start an  withunder score', async () => {
  const id_0 = '57509b5f350a10fb44e4c2b5'
  const id_1 = '57509b5f350a10fb44e4c2b7'

  const actual = queryObjectId({
    _id: { $in: [ObjectId(id_0), ObjectId(id_1)] },
  })

  expect(actual).toEqual({
    _id: { $in: [ObjectId(id_0), ObjectId(id_1)] },
  })
})

test('Mongodb queryObjectId retrurs $in args with ObjectID', async () => {
  const id_0 = '57509b5f350a10fb44e4c2b5'
  const id_1 = '57509b5f350a10fb44e4c2b7'

  const actual = queryObjectId({ id: { $in: [id_0, id_1] } })

  expect(actual).toEqual({
    _id: { $in: [ObjectId(id_0), ObjectId(id_1)] },
  })
})

test('Mongodb queryObjectId returns may ids', async () => {
  const id_0 = '57509b5f350a10fb44e4c2b5'
  const id_1 = '57509b5f350a10fb44e4c2b7'
  const actual = queryObjectId({ id: [id_0, id_1] })

  expect(actual).toEqual({ _id: { $in: [ObjectId(id_0), ObjectId(id_1)] } })
})
