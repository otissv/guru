import '../helpers/dotenvTest'

import { MongodbClient, mongoDbResolvers } from 'guru-mongodb'

import { data } from '../helpers/fixtures/resolver.fixture'
import { schema } from '../helpers/fixtures/schema.fixture'

let Mutation
let collectionName = 'test_resolver_mutations'
let context
let db
let namespace = 'mongodb'
let resolver
let typeDefs = schema({ collection: collectionName, namespace })

beforeAll(async () => {
  db = new MongodbClient({
    namespace: 'mongodb',
    typeDefs,
    options: {
      uri: process.env.MONGODB,
      opts: {
        server: {
          socketOptions: { keepAlive: 1 },
        },
      },
    },
  })
  await db.client()
  await db.seed({
    collection: `${collectionName}`,
    data,
    indexs: { age: -1 },
    silent: true,
  })
  context = { [namespace]: await db }
  Mutation = mongoDbResolvers({ context, typeDefs, namespace }).Mutation
})

/* == InsertOne == */
test('Mongodb mutation - InsertOne does not insert document if there is no data', async () => {
  const actual = await Mutation.userInsertOne(null, {}, context)

  expect(actual.extensions.code).toBe('RESOLVER_MUTATION_DATA_ERROR')
})

test('Mongodb mutation resolvers - InsertOne does not insert document if there is no data', async () => {
  const actual = await Mutation.userInsertOne(null, {}, {})

  expect(actual.extensions.code).toBe('RESOLVER_MUTATION_DATA_ERROR')
})

test('Mongodb mutation - InsertOne inserts a document', async () => {
  const args = {
    data: {
      age: 1000,
      username: 'user_1000',
      email: 'user_1000@email.com',
      roles: ['ADMIN'],
    },
  }
  const actual = await Mutation.userInsertOne(null, args, context)
  const expected = {
    RESULTS: {
      id: '5b49f0b11b16183659fc7f2d',
      n: 1,
      operation: 'userInsertOne',
      result: 'ok',
    },
  }

  expect(actual.RESULTS.result).toBe('ok')
  expect(actual.RESULTS.n).toBe(1)
})

/* == InsertMany == */
test('Mongodb mutation - InsertMany does not create document with empty item', async () => {
  const args = {
    data: [{ username: 'user_2000' }, {}],
  }

  const actual = await Mutation.userInsertMany(null, args, context)

  expect(actual.extensions.code).toBe('RESOLVER_MUTATION_DATA_ERROR')
})

test('Mongodb mutation - InsertMany does not create documents with no data', async () => {
  const actual = await Mutation.userInsertMany(null, {}, context)

  expect(actual.extensions.code).toBe('RESOLVER_MUTATION_DATA_ERROR')
})
test('Mongodb mutation - InsertMany creates many documents', async () => {
  const args = {
    data: [
      {
        age: 2000,
        username: 'user_2000',
        email: 'user_2000email.com',
        roles: ['ADMIN'],
      },
      {
        age: 3000,
        username: 'user_3000',
        email: 'user_3000@email.com',
        roles: ['ADMIN'],
      },
    ],
  }

  const actual = await Mutation.userInsertMany(null, args, context)

  const expected = {
    RESULTS: {
      id: '5b49f0b11b16183659fc7f2d',
      n: 2,
      operation: 'userInsertMany',
      result: 'ok',
    },
  }

  expect(actual.RESULTS.result).toBe('ok')
  expect(actual.RESULTS.n).toBe(2)
})

/* == Delete One == */
test('Mongodb mutation - deleteOne does not delete documentation with input', async () => {
  const actual = await Mutation.userDeleteOne(null, {}, context)

  expect(actual.extensions.code).toBe('RESOLVER_INPUT_ERROR')
})

test('Mongodb mutation - Delete a document', async () => {
  const args = {
    query: {
      age: { EQ: 23 },
    },
  }
  const actual = await Mutation.userDeleteOne(null, args, context)
  const expected = {
    RESULTS: { n: 1, operation: 'userDeleteOneMutation', result: 'ok' },
  }

  expect(actual).toEqual(expected)
})

test('Mongodb mutation - DeleteOne deletes matching documentations', async () => {
  const args = {
    query: { AND: [{ age: { GT: 3 } }, { age: { LT: 6 } }] },
  }

  const actual = await Mutation.userDeleteOne(null, args, context)
  const expected = {
    RESULTS: { n: 2, operation: 'userDeleteOneMutation', result: 'ok' },
  }
  expect(actual).toEqual(expected)
})

/* == DeleteMany == */
test('Mongodb mutation - Delete matching documentations', async () => {
  const args = {
    query: { AND: [{ age: { GT: 10 } }, { age: { LT: 13 } }] },
  }

  const actual = await Mutation.userDeleteMany(null, args, {})
  const expected = {
    RESULTS: { n: 2, operation: 'userDeleteManyMutation', result: 'ok' },
  }
  expect(actual).toEqual(expected)
})

/* == UpdateOne == */
test('Mongodb mutation - UpdateOne does not update document if there is no data', async () => {
  const actual = await Mutation.userUpdateOne(null, {}, context)

  expect(actual.extensions.code).toBe('RESOLVER_MUTATION_DATA_ERROR')
})

test('Mongodb mutation - Update updates a document', async () => {
  const args = {
    query: { _id: '5758078ba2bb7ddb14440965' },
    data: { username: 'user_guru' },
  }
  const actual = await Mutation.userUpdateOne(null, args, context)
  const expected = {
    RESULTS: {
      id: '5758078ba2bb7ddb14440965',
      n: 1,
      operation: 'userUpdateOneMutation',
      result: 'ok',
    },
  }

  expect(actual).toEqual(expected)
})

/* == UpdateMany == */
test('Mongodb mutation - UpdateMany does not update any documents if there is no data', async () => {
  const actual = await Mutation.userUpdateMany(null, {}, context)

  expect(actual.extensions.code).toBe('RESOLVER_MUTATION_DATA_ERROR')
})

test('Mongodb mutation - UpdateMany updates multiple documents', async () => {
  const args = {
    query: { age: { LT: 3 } },
    data: { username: 'user_guru2' },
  }
  const actual = await Mutation.userUpdateMany(null, args, context)
  const expected = {
    RESULTS: { n: 2, operation: 'userUpdateManyMutation', result: 'ok' },
  }

  expect(actual).toEqual(expected)
})

/* == UpdateManyById == */
test('Mongodb mutation - UpdateManyById does not update any documents if there is no data', async () => {
  const actual = await Mutation.userUpdateManyById(null, {}, context)

  expect(actual.extensions.code).toBe('RESOLVER_MUTATION_DATA_ERROR')
})

test('Mongodb mutation - UpdateManyById multiple documents', async () => {
  const args = {
    query: { _id: ['5758078ba2bb7ddb14440963', '5758078ba2bb7ddb14440964'] },
    data: { username: 'user_guru3' },
  }
  const actual = await Mutation.userUpdateManyById(null, args, context)
  const expected = {
    RESULTS: { n: 2, operation: 'userUpdateManyByIdMutation', result: 'ok' },
  }

  expect(actual).toEqual(expected)
})

test('Mongodb mutation resolver - UpdateManyById multiple documents', async () => {
  const args = {
    query: { _id: ['5758078ba2bb7ddb14440963', '5758078ba2bb7ddb14440964'] },
    data: { username: 'user_guru3' },
  }
  const actual = await Mutation.userUpdateManyById(null, args, {})
  const expected = {
    RESULTS: { n: 2, operation: 'userUpdateManyByIdMutation', result: 'ok' },
  }

  expect(actual).toEqual(expected)
})
