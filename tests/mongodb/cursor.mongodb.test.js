import '../helpers/dotenvTest'

import { MongodbClient, execute } from 'guru-mongodb'

import { data } from '../helpers/fixtures/accounts.fixture'

let client
const collectionName = 'test_query_utils'
const MONGODB = process.env.MONGODB

beforeAll(async () => {
  const mongodb = new MongodbClient({
    namespace: 'mongodb',
    options: {
      uri: MONGODB,
      opts: {
        server: {
          socketOptions: { keepAlive: 1 },
        },
      },
    },
  })
  client = await mongodb.client()
  await mongodb.seed({
    collection: `${collectionName}`,
    data,
    indexs: { age: -1 },
    silent: true,
  })
})

test.skip('Projection - Cursor return projections.', async () => {
  const doc = await client.then(db => {
    return execute({
      col: collectionName,
      cursor: null,
      db,
      method: 'findOne',
      args: {},
      projection: {
        _id: 0,
        username: 1,
        email: 1,
      },
    })
  })

  expect(doc).toEqual({ email: 'a@email.com', username: 'a' })
})

test.skip('Cursor - Returns a execute.', async () => {
  const actual = await client.then(db => {
    return execute({
      col: collectionName,
      db,
      method: 'findOne',
    })
  })

  expect(actual._id).toBeTruthy()
})

test.skip('Batch -Cursor returns batch size.', async () => {
  const actual = await new Promise((resolve, reject) => {
    client.then(db => {
      return execute({
        col: collectionName,
        db,
        method: 'find',
        cursor: { limit: 3, batchSize: 1 },
        projection: {
          _id: 0,
          username: 1,
        },
      }).toArray((error, docs) => {
        if (error) console.error(error)
        resolve(docs)
      })
    })
  })

  expect(actual).toEqual([
    { username: 'a' },
    { username: 'b' },
    { username: 'c' },
  ])
})

// test.skip('Collation -Cursor returns Collation.', async () => {
//   const actual = await new Promise((resolve, reject) => {
//     // client.adminCommand({ setFeatureCompatibilityVersion: '3.6' });
//     client.then(db => {
//       return execute({
//         col: collectionName,
//         db,
//         method: 'find',
//         cursor: { collation: { locale: 'en_US', strength: 1 } },
//         projection: {
//           _id: 0,
//           username: 1
//         }
//       }).toArray((error, docs) => {
//         if (error) console.error(error);
//         resolve(docs);
//       });
//     });
//   });

//   expect(actual).toEqual([]);
// });

test.skip('Comment - Associates a comment string with the find operation.', async () => {
  const actual = await new Promise((resolve, reject) => {
    client.then(db => {
      return execute({
        col: collectionName,
        db,
        method: 'find',
        cursor: { limit: 3, comment: 'Hi, there!' },
        projection: {
          _id: 0,
          username: 1,
        },
      }).toArray((error, docs) => {
        if (error) console.error(error)
        resolve(docs)
      })
    })
  })

  expect(actual).toEqual([
    { username: 'a' },
    { username: 'b' },
    { username: 'c' },
  ])
})

test.skip('Count - Counts the number of documents referenced by a execute.', async () => {
  const actual = await client.then(db => {
    return execute({
      col: collectionName,
      db,
      method: 'find',
      cursor: { count: true },
      projection: {
        _id: 0,
        username: 1,
      },
    })
  })

  expect(actual).toBe(9)
})

test.skip('Filter - Filters object used for the execute.', async () => {
  const actual = await new Promise((resolve, reject) => {
    client.then(db => {
      return execute({
        col: collectionName,
        db,
        method: 'find',
        cursor: { filter: { roles: { ELEM_MATCH: { EQ: 'ADMIN' } } } },
        projection: {
          _id: 0,
          username: 1,
        },
      }).toArray((error, docs) => {
        if (error) console.error(error)
        resolve(docs)
      })
    })
  })
  expect(actual).toEqual([{ username: 'a' }])
})

test.skip('First - Cursor returns first 3 items.', async () => {
  const actual = await new Promise((resolve, reject) => {
    client.then(db => {
      return execute({
        col: collectionName,
        db,
        method: 'find',
        cursor: { limit: 3 },
        projection: {
          _id: 0,
          username: 1,
        },
      }).toArray((error, docs) => {
        if (error) console.error(error)
        resolve(docs)
      })
    })
  })

  expect(actual).toEqual([
    { username: 'a' },
    { username: 'b' },
    { username: 'c' },
  ])
})

test.skip('ForEach - Iterates the execute to apply a JavaScript function to each document from the execute.', async () => {
  const actual = await new Promise((resolve, reject) => {
    client.then(db => {
      return execute({
        col: collectionName,
        db,
        method: 'find',
        cursor: { forEach: u => {} },
        projection: {
          _id: 0,
          username: 1,
        },
      }).toArray((error, docs) => {
        if (error) console.error(error)
        resolve(docs)
      })
    })
  })

  expect(actual).toEqual(undefined)
})

test.skip('Explain - Provides information on the execute plan.', async () => {
  const actual = await client.then(db => {
    return execute({
      col: collectionName,
      db,
      method: 'find',
      cursor: { limit: 3, explain: 'queryPlanner' },
      projection: {
        _id: 0,
        username: 1,
      },
    })
  })

  expect(actual.executionStats.executionStages.limitAmount).toEqual(3)
})

test.skip('Has Next - Query can iterate further to return more documents.', async () => {
  const actual = await client.then(db => {
    return execute({
      col: collectionName,
      db,
      method: 'find',
      cursor: { limit: 3, hasNext: true },
      projection: {
        _id: 0,
        username: 1,
      },
    })
  })

  expect(actual).toBe(true)
})

test.skip('Last - Cursor returns last 3 items with natural sort.', async () => {
  const actual = await new Promise((resolve, reject) => {
    client.then(db => {
      return execute({
        col: collectionName,
        db,
        method: 'find',
        cursor: { last: { username: 3 } },
        projection: {
          _id: 0,
          username: 1,
        },
      }).toArray((error, docs) => {
        if (error) console.error(error)
        resolve(docs)
      })
    })
  })

  expect(actual).toEqual([
    { username: 'i' },
    { username: 'h' },
    { username: 'g' },
  ])
})

test.skip('Last - Cursor returns last 3 items with defined sort.', async () => {
  const actual = await new Promise((resolve, reject) => {
    client.then(db => {
      return execute({
        col: collectionName,
        db,
        method: 'find',
        cursor: { last: 3 },
        projection: {
          _id: 0,
          username: 1,
        },
      }).toArray((error, docs) => {
        if (error) console.error(error)
        resolve(docs)
      })
    })
  })

  expect(actual).toEqual([
    { username: 'i' },
    { username: 'h' },
    { username: 'g' },
  ])
})

test.skip('Limit - Cursor returns limit 3 items.', async () => {
  const actual = await new Promise((resolve, reject) => {
    client.then(db => {
      return execute({
        col: collectionName,
        db,
        method: 'find',
        cursor: { limit: 3 },
        projection: {
          _id: 0,
          username: 1,
        },
      }).toArray((error, docs) => {
        if (error) console.error(error)
        resolve(docs)
      })
    })
  })

  expect(actual.length).toBe(3)
})

test.skip('Map - Applies function to each document visited by the execute and collects the return values from successive application into an array.', async () => {
  const actual = await new Promise((resolve, reject) => {
    client.then(db => {
      return execute({
        col: collectionName,
        db,
        method: 'find',
        cursor: { limit: 3, map: (u, i) => u.username },
        projection: {
          _id: 0,
          username: 1,
        },
      }).toArray((error, docs) => {
        if (error) console.error(error)
        resolve(docs)
      })
    })
  })

  expect(actual).toEqual(['a', 'b', 'c'])
})

test.skip('Max - Specifies the exclusive upper bound for a specific index in order to constrain the results.', async () => {
  const actual = await new Promise((resolve, reject) => {
    client.then(db => {
      return execute({
        col: collectionName,
        db,
        method: 'find',
        cursor: { max: { age: 7 } },
        projection: {
          _id: 0,
          age: 1,
        },
      }).toArray((error, docs) => {
        if (error) console.error(error)
        resolve(docs)
      })
    })
  })

  expect(actual).toEqual([{ age: 9 }, { age: 8 }])
})

test.skip('Min - Specifies the inclusive lower bound for a specific index in order to constrain the results.', async () => {
  const actual = await new Promise((resolve, reject) => {
    client.then(db => {
      return execute({
        col: collectionName,
        db,
        method: 'find',
        cursor: { min: { age: 2 } },
        projection: {
          _id: 0,
          age: 1,
        },
      }).toArray((error, docs) => {
        if (error) console.error(error)
        resolve(docs)
      })
    })
  })

  expect(actual).toEqual([{ age: 2 }, { age: 1 }])
})

test.skip('Sort - Cursor returns items ascending order.', async () => {
  const actual = await new Promise((resolve, reject) => {
    client.then(db => {
      return execute({
        col: collectionName,
        db,
        method: 'find',
        cursor: { limit: 3, sort: { username: 1 } },
        projection: {
          _id: 0,
          username: 1,
        },
      }).toArray((error, docs) => {
        if (error) console.error(error)
        resolve(docs)
      })
    })
  })

  expect(actual).toEqual([
    { username: 'a' },
    { username: 'b' },
    { username: 'c' },
  ])
})

test.skip('Sort - Cursor returns items descending order.', async () => {
  const actual = await new Promise((resolve, reject) => {
    client.then(db => {
      return execute({
        col: collectionName,
        db,
        method: 'find',
        cursor: { limit: 3, sort: { username: -1 } },
        projection: {
          _id: 0,
          username: 1,
        },
      }).toArray((error, docs) => {
        if (error) console.error(error)
        resolve(docs)
      })
    })
  })

  expect(actual).toEqual([
    { username: 'i' },
    { username: 'h' },
    { username: 'g' },
  ])
})

test.skip('Skip -Cursor returns skip 3 items.', async () => {
  const actual = await new Promise((resolve, reject) => {
    client.then(db => {
      return execute({
        col: collectionName,
        db,
        method: 'find',
        cursor: { limit: 3, skip: 3, sort: { username: 1 } },
        projection: {
          _id: 0,
          username: 1,
        },
      }).toArray((error, docs) => {
        if (error) console.error(error)
        resolve(docs)
      })
    })
  })

  expect(actual).toEqual([
    { username: 'd' },
    { username: 'e' },
    { username: 'f' },
  ])
})
