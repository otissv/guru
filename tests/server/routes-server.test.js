import '../helpers/dotenvTest'

import request from 'request'
import util from 'util'

const get = util.promisify(request.get)
const post = util.promisify(request.post)

const BASE_URL = process.env.BASE_URL
const GRAPHQL_URL = process.env.GRAPHQL_URL

// test('Server has index routes', async () => {
//   const actual = await get({
//     url: BASE_URL,
//   })

//   expect(actual.statusCode).toBe(200)
// })

test('Get - Empty graphql endpoint is bad request', async () => {
  const actual = await get({
    url: GRAPHQL_URL,
  })
  expect(actual.statusCode).toBe(400)
})

test('Post - Empty body is internal server error', async () => {
  const actual = await post({
    url: BASE_URL,
  })
  expect(actual.statusCode).toBe(404)
})
