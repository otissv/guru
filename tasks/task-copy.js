'use strict'

const shell = require('shelljs')
const argv = require('minimist')(process.argv)
const glob = require('glob-promise')
const watch = require('watch')
const silent = argv.s || argv.silent
const watching = argv.w || argv.watch
const fs = require('fs')

const src = `${process.cwd()}/src`
const dest = `${process.cwd()}/lib`

const pipe = sequence => {
  return sequence.reduce((previous, current) => current(), null)
}

function cleanFolder() {
  shell.config.silent = silent
  shell.rm('-r', dest)
  shell.config.silent = false

  if (!silent && !shell.error()) {
    process.stdout.write(`Cleaned ${dest}
`)
  }
}

function copyFolder() {
  const move = [].concat(`${src}/**/*.graphql`, `${src}/**/*.json`)

  move.forEach(pattern => {
    glob(pattern)
      .then(paths =>
        paths.map(p => {
          const pathSlit = p.split('src')[1]
          const fileName = p.substr(p.lastIndexOf('/') + 1)
          const output = pathSlit.split(fileName)[0]
          return {
            path: p,
            dest: output,
            fileName: fileName,
          }
        })
      )
      .then(pathsObj => {
        pathsObj.forEach(obj => {
          const output = `${dest}${obj.dest}`

          shell.ls(output)
          if (shell.error()) {
            Promise.resolve(shell.mkdir('-p', output))
          }
          shell.cp('-R', obj.path, output + obj.fileName)
          shell.config.silent = false

          if (!silent) {
            process.stdout.write(`Copied ${obj.path} -> ${output + obj.fileName}
`)
          }
        })
      })
      .catch(error => {
        process.stdout.write('copy: ', error)
        shell.config.silent = false
      })
  })
}

function buildBabel() {
  shell.config.silent = silent
  shell.exec(`babel -d ${dest} ${src} -s --ignore node_modules,lib/public
`)
  shell.config.silent = false

  if (!silent) {
    process.stdout.write(`Babel ${src} -> ${dest}

  `)
  }
}

function watchFolders() {
  function processFile(file) {
    const ext = file.split('.')[1]
    const pathSlit = file.split('src')[1]

    if (ext === 'js') {
      shell.exec(`babel ${file} --out-file ${dest}/${pathSlit}`)
      process.stdout.write(
        `date & Babel ${file} -> ${dest}${pathSlit}
`
      )
    } else {
      shell.cp('-R', file, `${dest}${pathSlit}`)
      process.stdout.write(
        `Copied ${file} -> ${dest}${pathSlit}
`
      )
    }
  }

  watch.watchTree(`${src}`, (file, curr, prev) => {
    if (typeof file === 'object' && prev === null && curr === null) {
      // Finished walking the tree
    } else if (prev === null) {
      // file is a new
      const pathSlit = file.split('src')[1]
      const isDir = fs.lstatSync(file).isDirectory()

      if (isDir) {
        shell.mkdir('-p', `${dest}${pathSlit}`)
      } else {
        processFile(file)
      }

      process.stdout.write(`Created ${dest}${pathSlit}
`)
    } else if (curr.nlink === 0) {
      // file was removed
      const pathSlit = file.split('src')[1]
      shell.rm('-r', `${dest}${pathSlit}`)
      process.stdout.write(`Removed ${dest}${pathSlit}
`)
    } else {
      // file was changed
      const pathSlit = file.split('src')[1]
      processFile(file)

      process.stdout.write(`${file} -> ${dest}${pathSlit}
`)
    }
  })
}

if (watching) {
  pipe([cleanFolder, copyFolder, buildBabel])

  watchFolders()
} else {
  pipe([cleanFolder, copyFolder, buildBabel])
}

// TODO: add error log
