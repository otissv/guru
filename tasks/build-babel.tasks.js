const shell = require('shelljs')

exports.buildBabel = function buildBabel({ dest, silent, src }) {
  return function() {
    shell.config.silent = silent
    shell.exec(`node ../../node_modules/@babel/cli/bin/babel.js -d ${dest} ${src} -s --config-file ${process.cwd()}/babel.config.js --ignore node_modules,dist,public
`)
    shell.config.silent = false

    if (!silent) {
      process.stdout.write(`${src} -> ${dest}

  `)
    }
  }
}
