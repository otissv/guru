const shell = require('shelljs')

exports.cleanFolders = function cleanFolders({ dest, silent, src }) {
  return function() {
    shell.config.silent = silent
    shell.rm('-r', dest)
    shell.config.silent = false

    if (!silent && !shell.error()) {
      process.stdout.write(`Cleaned ${dest}
`)
    }
  }
}
