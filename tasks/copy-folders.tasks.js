const shell = require('shelljs')
const glob = require('glob-promise')

exports.copyFolders = function copyFolders({ dest, silent, src }) {
  return function() {
    const move = [].concat(`${src}/**/*.graphql`, `${src}/**/*.json`)

    move.forEach(pattern => {
      glob(pattern)
        .then(paths =>
          paths.map(p => {
            const pathSlit = p.split('src')[1]
            const fileName = p.substr(p.lastIndexOf('/') + 1)
            const output = pathSlit.split(fileName)[0]
            return {
              path: p,
              dest: output,
              fileName: fileName,
            }
          })
        )
        .then(pathsObj => {
          pathsObj.forEach(obj => {
            const output = `${dest}${obj.dest}`

            shell.ls(output)
            if (shell.error()) {
              Promise.resolve(shell.mkdir('-p', output))
            }
            shell.cp('-R', obj.path, output + obj.fileName)
            shell.config.silent = false

            if (!silent) {
              process.stdout.write(`Copied ${obj.path} -> ${output +
                obj.fileName}
`)
            }
          })
        })
        .catch(error => {
          process.stdout.write('copy: ', error)
          shell.config.silent = false
        })
    })
  }
}
