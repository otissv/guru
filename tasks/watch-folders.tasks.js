const shell = require('shelljs')
const watch = require('watch')
const fs = require('fs')

exports.watchFolders = function watchFolders({ dest, silent, src, watching }) {
  function processFile(file) {
    const idx = file.lastIndexOf('.')
    const ext = file.substr(idx + 1, file.length - 1)
    const pathSlit = file.split('/src/')[1]

    if (ext === 'js') {
      shell.exec(`babel ${file} --out-file ${dest}/${pathSlit}`)
      process.stdout.write(
        `date & Babel ${file} -> ${dest}/${pathSlit}
  `
      )
    } else {
      shell.cp('-R', file, `${dest}/${pathSlit}`)
      process.stdout.write(
        `Copied ${file} -> ${dest}/${pathSlit}
  `
      )
    }
  }

  watch.watchTree(`${src}`, (file, curr, prev) => {
    if (typeof file === 'object' && prev === null && curr === null) {
      // Finished walking the tree
    } else if (prev === null) {
      // file is a new
      const pathSlit = file.split('/src/')[1]
      const isDir = fs.lstatSync(file).isDirectory()

      if (isDir) {
        shell.mkdir('-p', `${dest}/${pathSlit}`)
      } else {
        processFile(file)
      }

      process.stdout.write(`Created ${dest}${pathSlit}
  `)
    } else if (curr.nlink === 0) {
      // file was removed
      const pathSlit = file.split('src')[1]
      shell.rm('-r', `${dest}${pathSlit}`)
      process.stdout.write(`Removed ${dest}${pathSlit}
  `)
    } else {
      // file was changed
      const pathSlit = file.split('src')[1]
      processFile(file)

      process.stdout.write(`${file} -> ${dest}${pathSlit}
  `)
    }
  })
}
